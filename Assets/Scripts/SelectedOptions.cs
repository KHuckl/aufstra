﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class SelectedOptions
{
    public InputHandler.InputType inputType;
    public float steeringWheelSensitivity;
    public bool moveInSteps;
    public float soundVol;
    public bool vrMode;

    protected static readonly string PPKEY_INPUTTYPE = "inputType";
    protected static readonly string PPKEY_STEERINGWHEELSENSITIVITY = "swSensitivity";
    protected static readonly string PPKEY_MOVEINSTEPS = "moveInSteps";
    protected static readonly string PPKEY_SOUNDVOLUME = "soundVol";
    protected static readonly string PPKEY_VR = "vr";

    static SelectedOptions instance;
    public static SelectedOptions getInstance()
    {
        if (instance == null) {
            instance = new SelectedOptions();
            instance.Init();
        }

        return instance;
    }

    //Werte auslesen und setzen
    protected void Init()
    {
        if (PlayerPrefs.HasKey(PPKEY_INPUTTYPE))
        {
            setInputType(PlayerPrefs.GetInt(PPKEY_INPUTTYPE));
        }
        else
        {
            setInputType(0);
        }

        if (PlayerPrefs.HasKey(PPKEY_STEERINGWHEELSENSITIVITY))
        {
            setSteeringWheelSensitivity(PlayerPrefs.GetFloat(PPKEY_STEERINGWHEELSENSITIVITY));
        }
        else
        {
            setSteeringWheelSensitivity(1f);
        }

        if (PlayerPrefs.HasKey(PPKEY_MOVEINSTEPS))
        {
            setMoveInSteps(PlayerPrefs.GetInt(PPKEY_MOVEINSTEPS) == 1);
        }
        else
        {
            setMoveInSteps(true);
        }

        if (PlayerPrefs.HasKey(PPKEY_SOUNDVOLUME))
        {
            setSoundVolume(PlayerPrefs.GetFloat(PPKEY_SOUNDVOLUME));
        }
        else
        {
            setSoundVolume(1f);
        }

        if (PlayerPrefs.HasKey(PPKEY_VR))
        {
            setVRMode(PlayerPrefs.GetInt(PPKEY_VR) == 1);
        }
        else
        {
            setVRMode(false);
        }
    }

    public void setInputType(int v)
    {
        inputType = v == 0 ? InputHandler.InputType.Keyboard : InputHandler.InputType.SteeringWheel;
        PlayerPrefs.SetInt(PPKEY_INPUTTYPE, v);
    }

    public void setSteeringWheelSensitivity(float v)
    {
        steeringWheelSensitivity = v;
        PlayerPrefs.SetFloat(PPKEY_STEERINGWHEELSENSITIVITY, v);
    }

    public void setMoveInSteps(bool v)
    {
        moveInSteps = v;
        PlayerPrefs.SetInt(PPKEY_MOVEINSTEPS, moveInSteps ? 1 : 0);
    }

    public void setSoundVolume(float v)
    {
        soundVol = v;
        PlayerPrefs.SetFloat(PPKEY_SOUNDVOLUME, v);
    }

    public void setVRMode(bool v)
    {
        vrMode = v;
        PlayerPrefs.SetInt(PPKEY_VR, vrMode ? 1 : 0);

        if (vrMode)
        {
            EnableVR();
        }
        else
        {
            DisableVR();
        }


    }

    //---
    IEnumerator LoadDevice(string newDevice, bool enable)
    {
        XRSettings.LoadDeviceByName(newDevice);
        yield return null;
        XRSettings.enabled = enable;
    }

    void EnableVR()
    {
        CoroutineStarter.StartCoroutine(LoadDevice("OpenVR", true));
    }

    void DisableVR()
    {
        CoroutineStarter.StartCoroutine(LoadDevice("", false));
    }
    //---

}
