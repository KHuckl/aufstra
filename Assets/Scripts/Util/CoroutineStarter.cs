﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CoroutineStarter
{
    static string coroutineStarterPrefabName= "CoroutineStarter";

    static CoroutineStarterScr css;

    public static void StartCoroutine(IEnumerator routine)
    {
        if(css == null)
        {
            GameObject instancedObj = GameObject.Instantiate(Resources.Load<GameObject>(coroutineStarterPrefabName)) as GameObject;
            css = instancedObj.GetComponent<CoroutineStarterScr>();
        }

        css.StartCoroutine(routine);
    }
}
