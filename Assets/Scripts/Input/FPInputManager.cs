﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPInputManager : MonoBehaviour
{
    public float sensitivityX = 2f;
    public float sensitivityY = 2f;

    private float rotationY = 0f;

    protected CharacterController charCon;


    protected float fp_v;
    protected float fp_h;

    InputHandler ih;

    void Awake()
    {
        charCon = GetComponent<CharacterController>();

        ih = GameObject.FindGameObjectWithTag("CoreObjects").transform.Find("InputHandler").GetComponent<InputHandler>();

        ih.EventFPVertical += setFPVertical;
        ih.EventFPHorizontal += setFPHorizontal;
    }

    private void setFPVertical(float val)
    {
        fp_v = val;
    }

    private void setFPHorizontal(float val)
    {
        fp_h = val;
    }

    void Update()
    {
        if (!ih.fp_steps) {
            

            // Move forward / backward
            Vector3 forward = transform.TransformDirection(Vector3.forward);
            float curSpeed = fp_v * sensitivityX;
            charCon.SimpleMove(forward * curSpeed);

            // Rotate around y - axis
            transform.Rotate(0, fp_h * sensitivityY, 0);
        }
        else
        {
            // Move forward / backward
            Vector3 forward = transform.TransformDirection(Vector3.forward);
            float curSpeed = fp_v;
            charCon.SimpleMove(forward * curSpeed);

            // Rotate around y - axis
            transform.Rotate(0, fp_h, 0);
        }


    }
}
