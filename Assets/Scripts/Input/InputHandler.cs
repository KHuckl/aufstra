﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    public enum InputType
    {
        Keyboard,
        SteeringWheel
    }

    //Events werden nur geworfen, wenn sich die Werte aendern
    protected float lastAccelerate = 0f;
    protected float lastBrake = 0f;
    protected float lastFp_vertical = 0f;
    protected float lastFp_horizontal = 0f;

    //Wird genutzt um zu sehen ob die H-Schaltung in Neutral gestellt wurde
    protected int lastShiftedGear = 1;

    //Wenn sich in Schritten bewegt werden soll, muss sich gemerkt werden, ob die Achse auch zwischendurch losgelassen wurde
    protected bool fp_v_axis_reseted;
    protected bool fp_h_axis_reseted;

    protected bool fp_v_axis_changeVehicle_reseted;


    protected static readonly string STEERINGWHEEL_ACCELERATE = "WheelAccelerate";
    protected static readonly string STEERINGWHEEL_BRAKE = "WheelBrake";
    protected static readonly string STEERINGWHEEL_CLUTCH = "WheelClutch";
    protected static readonly string STEERINGWHEEL_STEER = "WheelSteering";
    protected static readonly string STEERINGWHEEL_GEAR_REVERSE = "WheelGearReverse";
    protected static readonly string STEERINGWHEEL_GEAR_1 = "WheelGear1";
    protected static readonly string STEERINGWHEEL_GEAR_2 = "WheelGear2";
    protected static readonly string STEERINGWHEEL_GEAR_3 = "WheelGear3";
    protected static readonly string STEERINGWHEEL_GEAR_4 = "WheelGear4";
    protected static readonly string STEERINGWHEEL_GEAR_5 = "WheelGear5";
    protected static readonly string STEERINGWHEEL_GEAR_6 = "WheelGear6";
    protected static readonly string STEERINGWHEEL_SHIFTUP = "WheelShiftUp";
    protected static readonly string STEERINGWHEEL_SHIFTDOWN = "WheelShiftDown";
    protected static readonly string STEERINGWHEEL_TRAILER = "WheelTrailerAttachDetach";

    protected static readonly string FP_VERTICAL = "FPVertical";
    protected static readonly string FP_HORIZONTAL = "FPHorizontal";
    protected static readonly string FP_VERTICAL_AXIS_CHANGEVEHICLE = "FPVerticalAXIS-ChangeVehicle";
    protected static readonly string FP_HORIZONTAL_AXIS = "FPHorizontalAXIS";

    protected static readonly string CHANGEVEHICLE = "ChangeVehicle";
    protected static readonly string CHANGECAMERA = "ChangeCamera";

    protected static readonly string ENGINESTARTSTOP = "EngineStartStop";

    protected static readonly string BLINKERLEFT = "LeftBlinker";
    protected static readonly string BLINKERRIGHT = "RightBlinker";
    protected static readonly string HAZARDLIGHTS = "HazardLights";
    protected static readonly string LIGHTS = "Lights";
    protected static readonly string LIGHTSFULLBEAM = "LightsFullBeam";

    protected static readonly string HANDBRAKE = "Handbrake";
    protected static readonly string HORN = "Horn";



    //Wegen VR: Es koennen große Schritte (statt gleichmaessiges bewegen) getaetigt werden
    public bool fp_steps = true;
    //Vor/Zureuck Schrittgroesse
    public float fp_verticalStep = 1f;
    //Links/Rechts Dreh-Schrittgroesse
    public float fp_horizontalStep = 1f;

    //Gas geben
    public delegate void DelAccelerate(float value);
    public event DelAccelerate EventAccelerate;

    //Bremsen
    public delegate void DelBrake(float value);
    public event DelBrake EventBrake;

    //Lenken
    public delegate void DelSteer(float value);
    public event DelSteer EventSteer;

    //Schalten in bestimmten Gang
    public delegate void DelShift(int value);
    public event DelShift EventShift;
    //Schalten +1
    public delegate void DelShiftUp();
    public event DelShiftUp EventShiftUp;
    //Schalten -1
    public delegate void DelShiftDown();
    public event DelShiftDown EventShiftDown;

    //Anhaenger an/abkuppeln
    public delegate void DelTrailer();
    public event DelTrailer EventTrailer;

    //First Person Vor/Zurueck
    public delegate void DelFPVertical(float value);
    public event DelFPVertical EventFPVertical;

    //First Person rechts/links drehen
    public delegate void DelFPHorizontal(float value);
    public event DelFPHorizontal EventFPHorizontal;

    //Ein/AUssteigen aus Fahrzeug
    public delegate void DelChangeVehicle();
    public event DelChangeVehicle EventChangeVehicle;

    //Kamera aendern
    public delegate void DelChangeCamera();
    public event DelChangeCamera EventChangeCamera;

    //Kupplung
    public delegate void DelClutch(float value);
    public event DelClutch EventClutch;

    //Motor an/aus
    public delegate void DelEngine();
    public event DelEngine EventEngine;

    //Blinker
    public delegate void DelBlinkerLeft();
    public event DelBlinkerLeft EventBlinkerLeft;
    public delegate void DelBlinkerRight();
    public event DelBlinkerRight EventBlinkerRight;
    //Warnblinklicht
    public delegate void DelHazardLights();
    public event DelHazardLights EventHazardLights;
    //Licht
    public delegate void DelLights();
    public event DelLights EventLights;
    //Fernlicht
    public delegate void DelLightsFullBeam();
    public event DelLightsFullBeam EventLightsFullBeam;

    //Handbremse
    //Hier koennte man (wie bei Horn) die Handbremse nur aktiv schalten, solange der Button gedrueckt bleibt.
    //Macht bei einem LKW-Simulator aber keinen grossen Sinn, daher bleibt Handbremse aktiv bis Button wieder gedrueckt wird
    public delegate void DelHandbrake();
    public event DelHandbrake EventHandbrake;

    //Hupe
    public delegate void DelHorn(bool doHorn);
    public event DelHorn EventHorn;

    //Pedal liefern immer 0 zurueck bevor eines das erstemal gedrueck wurde
    //Dh. Gas liefert 0 (halb gedrueck) zurueck, obwohl es bei -1(nicht gedrueckt) sein sollte
    //Hiermit wird geprueft ob ein Pedal bereits gedrueckt wurde, wenn nein, werden 0 Werte ignoriert (siehe getter)
    protected bool pedalsInitialized = false;
    
    void Start()
    {
        fp_steps = SelectedOptions.getInstance().moveInSteps;
    }

    // Update is called once per frame
    void Update()
    {

        //DEBUG output


        //for (int joystick = 1; joystick < 5; joystick++)
        //{
        //    for (int button = 0; button < 20; button++)
        //    {
        //        if (Input.GetKeyDown("joystick " + joystick + " button " + button))
        //        {

        //            Debug.Log("joystick = " + joystick + "  button = " + button);
        //        }



        //    }
        //}
        /*
        float inV = Input.GetAxisRaw(FP_VERTICAL_AXIS);
        float inH = Input.GetAxisRaw(FP_HORIZONTAL_AXIS);
        if (inV > 0 || inV < 0)
        {
            Debug.Log(FP_VERTICAL_AXIS +": "+ inV);
        }
        if (inH > 0 || inH < 0)
        {
            Debug.Log(FP_HORIZONTAL_AXIS + ": " + inH);
        }
        */
        // /DEBUG output




        Debug.Log(SelectedOptions.getInstance().inputType.ToString());
        //switch (SelectedOptions.getInstance().inputType)
        //{

        //    case InputType.Keyboard:
        //        doDebugInput();
        //        break;
        //    case InputType.SteeringWheel:
        //        doSteeringWheelInput();
        //        break;
        //}
        doSteeringWheelInput();
    }

    private void ThrowEventAccelerate(float value)
    {
        if (lastAccelerate == value)
            return;

        if (EventAccelerate != null)
        {
            EventAccelerate(value);
        }

        
        lastAccelerate = value;
    }

    private void ThrowEventBrake(float value)
    {
        if (lastBrake == value)
            return;

        if (EventBrake != null)
        {
            EventBrake(value);
        }

        lastBrake = value;
    }

    private void ThrowEventSteer(float value)
    {
        if (EventSteer != null)
        {
            EventSteer(value);
        }
    }

    private void ThrowEventShift(int value)
    {
        lastShiftedGear = value;

        if (EventShift != null)
        {
            EventShift(value);
        }
    }

    private void ThrowEventShiftUp()
    {
        if (EventShiftUp != null)
        {
            EventShiftUp();
        }
    }

    private void ThrowEventShiftDown()
    {
        if (EventShiftDown != null)
        {
            EventShiftDown();
        }
    }

    private void ThrowEventTrailer()
    {
        if (EventTrailer != null)
        {
            EventTrailer();
        }
    }

    private void ThrowFPSVertical(float value)
    {
        if (lastFp_vertical == value)
            return;

        if (EventFPVertical != null)
        {
            EventFPVertical(value);
        }

        lastFp_vertical = value;
    }

    private void ThrowFPSHorizontal(float value)
    {
        if (lastFp_horizontal == value)
            return;

        if (EventFPHorizontal != null)
        {
            EventFPHorizontal(value);
        }

        lastFp_horizontal = value;
    }

    private void ThrowEventChangeVehicle()
    {
        if (EventChangeVehicle != null)
        {
            EventChangeVehicle();
        }
    }

    private void ThrowEventChangeCamera()
    {
        if (EventChangeCamera != null)
        {
            EventChangeCamera();
        }
    }

    private void ThrowEventClutch(float value)
    {
        if (EventClutch != null)
        {
            EventClutch(value);
        }
    }

    private void ThrowEventEngine()
    {
        if (EventEngine != null)
        {
            EventEngine();
        }
    }

    private void ThrowEventBlinkerLeft()
    {
        if (EventBlinkerLeft != null)
        {
            EventBlinkerLeft();
        }
    }

    private void ThrowEventBlinkerRight()
    {
        if (EventBlinkerRight != null)
        {
            EventBlinkerRight();
        }
    }

    private void ThrowEventHazardLights()
    {
        if (EventHazardLights != null)
        {
            EventHazardLights();
        }
    }

    private void ThrowEventLights()
    {
        if (EventLights != null)
        {
            EventLights();
        }
    }

    private void ThrowEventLightsFullBeam()
    {
        if (EventLightsFullBeam != null)
        {
            EventLightsFullBeam();
        }
    }

    private void ThrowEventHandbrake()
    {
        if (EventHandbrake != null)
        {
            EventHandbrake();
        }
    }

    private void ThrowEventHorn(bool doHorn)
    {
        if (EventHorn != null)
        {
            EventHorn(doHorn);
        }
    }

    //----Keyboard
    private void doDebugInput()
    {
        if (Input.GetKey(KeyCode.W))
        {
            ThrowEventAccelerate(1);
            ThrowFPSVertical(1);
        }
        else
        {
            if (!Input.GetKey(KeyCode.S)) {
                ThrowEventAccelerate(0);
                ThrowFPSVertical(0);
            }
        }

        if (Input.GetKey(KeyCode.S))
        {
            ThrowEventBrake(1);
            ThrowFPSVertical(-1);
        }
        else
        {
            if (!Input.GetKey(KeyCode.W))
            {
                ThrowEventBrake(0);
                ThrowFPSVertical(0);
            }
        }

        if (Input.GetKey(KeyCode.A))
        {
            ThrowEventSteer(-1);
            ThrowFPSHorizontal(-1);
        }
        else
        {
            if (!Input.GetKey(KeyCode.D)) {
                ThrowEventSteer(0);
                ThrowFPSHorizontal(0);
            }
        }

        if (Input.GetKey(KeyCode.D))
        {
            ThrowEventSteer(1);
            ThrowFPSHorizontal(1);
        }
        else
        {
            if (!Input.GetKey(KeyCode.A)){
                ThrowEventSteer(0);
                ThrowFPSHorizontal(0);
            }
        }

        //Schalten
        //Rueckwaertsgang
        if (Input.GetKeyDown(KeyCode.KeypadMultiply))
        {
            ThrowEventShift(0);
        }
        //Neutral
        if (Input.GetKeyDown(KeyCode.Keypad0))
        {
            ThrowEventShift(1);
        }
        //1. Gang usw
        if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            ThrowEventShift(2);
        }
        if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            ThrowEventShift(3);
        }
        if (Input.GetKeyDown(KeyCode.Keypad3))
        {
            ThrowEventShift(4);
        }
        if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            ThrowEventShift(5);
        }
        if (Input.GetKeyDown(KeyCode.Keypad5))
        {
            ThrowEventShift(6);
        }
        if (Input.GetKeyDown(KeyCode.Keypad6))
        {
            ThrowEventShift(7);
        }
        if (Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            ThrowEventShiftUp();
        }
        if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            ThrowEventShiftDown();
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            ThrowEventTrailer();
        }

        if (Input.GetKeyDown(KeyCode.V))
        {
            ThrowEventChangeVehicle();
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            ThrowEventChangeCamera();
        }

        if (Input.GetKeyDown(KeyCode.KeypadDivide))
        {
            ThrowEventClutch(1);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            ThrowEventEngine();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            ThrowEventBlinkerLeft();
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            ThrowEventBlinkerRight();
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            ThrowEventHazardLights();
        }

        if (Input.GetKeyDown(KeyCode.U))
        {
            ThrowEventLights();
        }

        if (Input.GetKeyDown(KeyCode.I))
        {
            ThrowEventLightsFullBeam();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            ThrowEventHandbrake();
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            ThrowEventHorn(true);
        }else if (Input.GetKeyUp(KeyCode.H))
        {
            ThrowEventHorn(false);
        }

    }
    //----/Keyboard

    //----Steering Wheel
    protected float getSteeringWheelAcceleration()
    {
        //Gaspedal geht von -1 bis 1 (-1: nicht gedrueckt; 1: voll durchgedrueckt)
        float value = (Input.GetAxis(STEERINGWHEEL_ACCELERATE) + 1) / 2;

        if (!pedalsInitialized && value == 0.5f)
        {
            return 0f;
        }
        pedalsInitialized = true;

        return value;
    }

    protected float getSteeringWheelBrake()
    {
        //Bremspedal geht von -1 bis 1 (-1: voll durchgedrueckt; 1: nicht gedrueckt)
        float value = (Input.GetAxis(STEERINGWHEEL_BRAKE) - 1) * -1 / 2;

        if (!pedalsInitialized && value == 0.5f)
        {
            return 0f;
        }
        pedalsInitialized = true;

        return value;
    }

    protected float getSteeringWheelClutch()
    {
        //Kupplung geht von 1 bis -1 (-1: voll durchgedrueckt; 1: nicht gedrueckt)
        float value = (Input.GetAxis(STEERINGWHEEL_CLUTCH) - 1) * -1 / 2;

        if (!pedalsInitialized && value == 0.5f)
        {
            return 0f;
        }
        pedalsInitialized = true;

        return value;
    }
    

    void OnApplicationFocus(bool val)
    {
        //pedalsInitialized muss zurueck gesetzt werden, da beim verlieren des Fokuses wieder falsche Werte uebergeben werden (siehe pedalsInitialized)
        if (!val)
            pedalsInitialized = false;
    }

    private void doSteeringWheelInput()
    {
        Debug.Log("getSteeringWheelAcceleration: " + getSteeringWheelAcceleration() + "\tgetSteeringWheelBrake: " + getSteeringWheelBrake ()+ "\tWheelSteering: "+ Input.GetAxis("WheelSteering"));
        Debug.Log("getSteeringWheelAcceleration: " + getSteeringWheelAcceleration() + "\tgetSteeringWheelBrake: " + getSteeringWheelBrake() + "\tgetSteeringWheelClutch: " + getSteeringWheelClutch());


        if (getSteeringWheelAcceleration() >= 0)
        {
            ThrowEventAccelerate(getSteeringWheelAcceleration());
        }


        if (getSteeringWheelBrake() >= 0)
        {
            ThrowEventBrake(getSteeringWheelBrake());
        }

        if (getSteeringWheelClutch() >= 0)
        {
            ThrowEventClutch(getSteeringWheelClutch());
        }

        ThrowEventSteer(Input.GetAxis(STEERINGWHEEL_STEER) * SelectedOptions.getInstance().steeringWheelSensitivity);


        //Schalten
        //Rueckwaertsgang
        if (Input.GetButtonDown(STEERINGWHEEL_GEAR_REVERSE))
        {
            ThrowEventShift(0);
        }
        //Neutral
        /*if (Input.GetKeyDown(KeyCode.Keypad0))
        {
            ThrowEventShift(1);
        }*/
        //1. Gang usw
        if (Input.GetButtonDown(STEERINGWHEEL_GEAR_1))
        {
            ThrowEventShift(2);
        }
        if (Input.GetButtonDown(STEERINGWHEEL_GEAR_2))
        {
            ThrowEventShift(3);
        }
        if (Input.GetButtonDown(STEERINGWHEEL_GEAR_3))
        {
            ThrowEventShift(4);
        }
        if (Input.GetButtonDown(STEERINGWHEEL_GEAR_4))
        {
            ThrowEventShift(5);
        }
        if (Input.GetButtonDown(STEERINGWHEEL_GEAR_5))
        {
            ThrowEventShift(6);
        }
        if (Input.GetButtonDown(STEERINGWHEEL_GEAR_6))
        {
            ThrowEventShift(7);
        }
        if (Input.GetButtonDown(STEERINGWHEEL_SHIFTUP))
        {
            ThrowEventShiftUp();
        }
        if (Input.GetButtonDown(STEERINGWHEEL_SHIFTDOWN))
        {
            ThrowEventShiftDown();
        }


        //TODO: prufen ob lastShiftedGear immernoch gedrueckt ist (also H-Schaltung nicht in Neutral gesetzt wurde)
        checkLastShiftedGear();


        if (Input.GetButtonDown(STEERINGWHEEL_TRAILER))
        {
            ThrowEventTrailer();
        }

        if (!fp_steps)
        {
            //Gleichmaessiges bewegen
            if (Input.GetButtonDown(FP_VERTICAL)) { 
                ThrowFPSVertical(Input.GetAxisRaw(FP_VERTICAL));
            }
            else if(Input.GetAxisRaw(FP_VERTICAL_AXIS_CHANGEVEHICLE) > 0) {
                ThrowFPSVertical(Input.GetAxisRaw(FP_VERTICAL_AXIS_CHANGEVEHICLE));
            }
            else
            {
                ThrowFPSVertical(0);
            }

        }
        else{
            //In Schritten bewegen
            if (Input.GetButtonDown(FP_VERTICAL))
            {
                ThrowFPSVertical(Input.GetAxisRaw(FP_VERTICAL) * fp_verticalStep);
            }else if (Input.GetAxisRaw(FP_VERTICAL_AXIS_CHANGEVEHICLE) > 0)
            {
                if (fp_v_axis_reseted)
                {
                    ThrowFPSVertical(Input.GetAxisRaw(FP_VERTICAL_AXIS_CHANGEVEHICLE) * fp_verticalStep);
                    fp_v_axis_reseted = false;
                }else
                {
                    ThrowFPSVertical(0);
                }
            }
            else
            {
                fp_v_axis_reseted = true;
                ThrowFPSVertical(0);
            }
        }

        if (!fp_steps)
        {
            //Gleichmaessiges drehen
            if (Input.GetButtonDown(FP_HORIZONTAL))
            {
                ThrowFPSHorizontal(Input.GetAxisRaw(FP_HORIZONTAL));
            }else if (Input.GetAxisRaw(FP_HORIZONTAL_AXIS) != 0)
            {
                ThrowFPSHorizontal(Input.GetAxisRaw(FP_HORIZONTAL_AXIS));
            }
            else
            {
                ThrowFPSHorizontal(0);
            }
        }
        else{
            //In Schritten drehen
            if (Input.GetButtonDown(FP_HORIZONTAL))
            {
                 ThrowFPSHorizontal(Input.GetAxisRaw(FP_HORIZONTAL) * fp_horizontalStep);
            }
            else if (Input.GetAxisRaw(FP_HORIZONTAL_AXIS) != 0)
            {
                if (fp_h_axis_reseted)
                {
                    ThrowFPSHorizontal(Input.GetAxisRaw(FP_HORIZONTAL_AXIS) * fp_horizontalStep);
                    fp_h_axis_reseted = false;
                }
                else
                {
                    ThrowFPSHorizontal(0);
                }
            }else
            {
                fp_h_axis_reseted = true;
                ThrowFPSHorizontal(0);
            }
        }

        // VehicleChange
        //Einmal normaler Button und einmal negativ Seite der Bewegungsachse zum Vor/Zurueck gehen
        //Achse zusaetzlich, da kein Button mehr auf Lenkrad frei war
        if (Input.GetButtonDown(CHANGEVEHICLE))
        {
            ThrowEventChangeVehicle();
        }else if(Input.GetAxisRaw(FP_VERTICAL_AXIS_CHANGEVEHICLE) < 0)
        {
            if (fp_v_axis_changeVehicle_reseted) { 
                fp_v_axis_changeVehicle_reseted = false;
                ThrowEventChangeVehicle();
            }
        }
        else
        {
            fp_v_axis_changeVehicle_reseted = true;
        }
        // /VehicleChange

        if (Input.GetButtonDown(CHANGECAMERA))
        {
            ThrowEventChangeCamera();
        }

        if (Input.GetButtonDown(ENGINESTARTSTOP))
        {
            ThrowEventEngine();
        }

        if (Input.GetButtonDown(BLINKERLEFT))
        {
            ThrowEventBlinkerLeft();
        }

        if (Input.GetButtonDown(BLINKERRIGHT))
        {
            ThrowEventBlinkerRight();
        }

        if (Input.GetButtonDown(HAZARDLIGHTS))
        {
            ThrowEventHazardLights();
        }

        if (Input.GetButtonDown(LIGHTS))
        {
            ThrowEventLights();
        }

        if (Input.GetButtonDown(LIGHTSFULLBEAM))
        {
            ThrowEventLightsFullBeam();
        }

        if (Input.GetButtonDown(HANDBRAKE))
        {
            ThrowEventHandbrake();
        }

        if (Input.GetButtonDown(HORN))
        {
            ThrowEventHorn(true);
        }else if (Input.GetButtonUp(HORN))
        {
            ThrowEventHorn(false);
        }
    }

    private void checkLastShiftedGear()
    {
        //Debug.Log("checkLastShiftedGear: "+ lastShiftedGear);

        if (lastShiftedGear == 1)
            return;

        if (lastShiftedGear == 0 && !Input.GetButton(STEERINGWHEEL_GEAR_REVERSE))
        {
            ThrowEventShift(1);
        }
        if (lastShiftedGear == 2 && !Input.GetButton(STEERINGWHEEL_GEAR_1))
        {
            ThrowEventShift(1);
        }
        if (lastShiftedGear == 3 && !Input.GetButton(STEERINGWHEEL_GEAR_2))
        {
            ThrowEventShift(1);
        }
        if (lastShiftedGear == 4 && !Input.GetButton(STEERINGWHEEL_GEAR_3))
        {
            ThrowEventShift(1);
        }
        if (lastShiftedGear == 5 && !Input.GetButton(STEERINGWHEEL_GEAR_4))
        {
            ThrowEventShift(1);
        }
        if (lastShiftedGear == 6 && !Input.GetButton(STEERINGWHEEL_GEAR_5))
        {
            ThrowEventShift(1);
        }
        if (lastShiftedGear == 7 && !Input.GetButton(STEERINGWHEEL_GEAR_6))
        {
            ThrowEventShift(1);
        }


    }
    //----/Steering Wheel


}
