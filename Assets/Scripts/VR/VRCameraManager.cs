﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class VRCameraManager : MonoBehaviour
{
    bool doVR;

    public GameObject normalCam;
    public GameObject vrCam;

    //Muss in Update sein, da sobald man ein Fahrzeug das zweite mal betritt, immer beide Cameras aktiv sind (wahrscheinlich irgendein NWH Skript)
    void Update()
    {
        checkVR();
    }
    
    void checkVR()
    {
        doVR = SelectedOptions.getInstance().vrMode;

        if (doVR)
        {
            EnableVR();
        }
        else
        {
            DisableVR();
        }
    }

    void EnableVR()
    {
        normalCam.SetActive(false);
        vrCam.SetActive(true);
    }

    void DisableVR()
    {
        normalCam.SetActive(true);
        vrCam.SetActive(false);
    }

}
