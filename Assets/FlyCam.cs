﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyCam : MonoBehaviour {
    public bool center = true;
    public KeyCode boostKey = KeyCode.LeftShift;
    public float boost=2;
    public float speed=5;
    public float sensitivity = 60;
    float s = 0;

    void Update () {
        s = speed;
        if (Input.GetKey(boostKey))
        {
            speed *= boost;
        }
        transform.eulerAngles = new Vector3(transform.eulerAngles.x + Input.GetAxis("Mouse Y") * -sensitivity * Time.deltaTime, transform.eulerAngles.y + Input.GetAxis("Mouse X")*sensitivity*Time.deltaTime, 0);
        transform.position += transform.forward * Input.GetAxis("Vertical") * speed * Time.deltaTime;
        transform.position += transform.right * Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        speed = s;
    }

    void FixedUpdate ()
    {
        if(center)
        Cursor.lockState = CursorLockMode.Locked;
    }
}
