﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if PHOTON_MULTIPLAYER
public class PhotonManager : MonoBehaviour
{
    public Text networkStatusText;

    public Camera lobbyCamera;
    public GameObject playerPrefab;
    public Transform spawnPoint;


    private void Start()
    {
        PhotonNetwork.ConnectUsingSettings("NVP Multiplayer v1");
        PhotonNetwork.sendRate = 20;
    }


    public virtual void OnConnectedToMaster()
    {
        Debug.Log("Connected to master.");
    }


    public virtual void OnJoinedLobby()
    {
        Debug.Log("Jointed lobby.");
        lobbyCamera.gameObject.SetActive(false);

        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 20;
        roomOptions.IsOpen = true;
        roomOptions.IsVisible = true;

        PhotonNetwork.JoinOrCreateRoom("NVP Multiplayer Room", roomOptions, null);
    }


    public virtual void OnJoinedRoom()
    {
        PhotonNetwork.Instantiate(playerPrefab.name, spawnPoint.position + new Vector3(Random.value * 15f, 0, Random.value * 15f), spawnPoint.rotation, 0);
    }


    private void Update()
    {
        networkStatusText.text = PhotonNetwork.connectionStateDetailed.ToString();
    }
}
#endif