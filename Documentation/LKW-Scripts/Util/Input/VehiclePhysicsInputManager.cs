﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NWH.VehiclePhysics;
using System;

public class VehiclePhysicsInputManager : MonoBehaviour
{
    /// <summary>
    /// Set to null (none) if you want to use your own vehicle controller. If this is set to other than null current active vehicle according 
    /// to the assigned vehicle changer will be used instead of the assigned vehicle controller.
    /// </summary>
    [Tooltip("Set to null (none) if you want to use your own vehicle controller. If this is set to other than null current active vehicle according " +
        "to the assigned vehicle changer will be used instead of the assigned vehicle controller.")]
    public VehicleChanger vehicleChanger;

    /// <summary>
    /// If you want to use this script with a single vehicle or want to set your own vehicle controller from script set vehicle changer field to null / none.
    /// </summary>
    [Tooltip("If you want to use this script with a single vehicle or want to set your own vehicle controller from script set vehicle changer field to null / none.")]
    public VehicleController vehicleController;

    protected float currAcc = 0f;
    protected float currBrake = 0f;

    void Start()
    {
        InputHandler ih = GameObject.FindGameObjectWithTag("CoreObjects").transform.Find("InputHandler").GetComponent<InputHandler>();
        ih.EventAccelerate += SetAccelerate;
        ih.EventBrake += SetBrake;
        ih.EventSteer += SetSteer;
        ih.EventClutch += SetClutch;

        ih.EventShift += setNewGear;
        ih.EventShiftUp += shiftUp;
        ih.EventShiftDown += shiftDown;
        ih.EventTrailer += trailer;
        ih.EventEngine += engine;

        ih.EventBlinkerLeft += blinkerLeft;
        ih.EventBlinkerRight += blinkerRight;
        ih.EventHazardLights += hazardLights;
        ih.EventLights += lights;
        ih.EventLightsFullBeam += lightsFullBeam;
        ih.EventHandbrake += handbrake;
        ih.EventHorn += horn;

    }

    void Update()
    {
        if (vehicleChanger != null)
        {
            vehicleController = vehicleChanger.ActiveVehicleController;
        }

        SetVertical();
    }

    //Muss extra berechnet werden, da vehicleController nur .Vertical kombiniert zum beschleunigen/bremsen besitzt. Dh. keine separate Bremse
    private void SetVertical()
    {
        if (!checkRequirements())
            return;

        vehicleController.input.Vertical = currAcc - currBrake;
    }

    private bool checkRequirements()
    {
        if (vehicleController == null)
            return false;

        return true;
    }
    
    private void SetAccelerate(float value)
    {
        if (!checkRequirements())
            return;

        currAcc = value;
    }

    private void SetBrake(float value)
    {
        if (!checkRequirements())
            return;

        currBrake = value;
    }

    private void SetSteer(float value)
    {
        if (!checkRequirements())
            return;

        vehicleController.input.Horizontal = value;
    }

    private void setNewGear(int newGear)
    {
        if (!checkRequirements())
            return;

        vehicleController.transmission.ShiftInto(newGear - 1);
    }

    private void shiftUp()
    {
        if (!checkRequirements())
            return;

        vehicleController.input.ShiftUp = true;
    }

    private void shiftDown()
    {
        if (!checkRequirements())
            return;

        vehicleController.input.ShiftDown = true;
    }

    private void trailer()
    {
        if (!checkRequirements())
            return;

        vehicleController.input.trailerAttachDetach = true;
    }

    private void SetClutch(float value)
    {
        if (!checkRequirements())
            return;

        vehicleController.input.Clutch = value;
    }

    private void engine()
    {
        if (!checkRequirements())
            return;

        vehicleController.engine.Toggle();
    }

    private void blinkerLeft()
    {
        if (!checkRequirements())
            return;

        vehicleController.input.leftBlinker = !vehicleController.input.leftBlinker;
        if (vehicleController.input.leftBlinker)
            vehicleController.input.rightBlinker = false;
    } 
    

    private void blinkerRight()
    {
        if (!checkRequirements())
            return;

        vehicleController.input.rightBlinker = !vehicleController.input.rightBlinker;
        if (vehicleController.input.rightBlinker)
            vehicleController.input.leftBlinker = false;
    }

    private void hazardLights()
    {
        if (!checkRequirements())
            return;

        vehicleController.input.hazardLights = !vehicleController.input.hazardLights;
        vehicleController.input.leftBlinker = false;
        vehicleController.input.rightBlinker = false;
    }

    private void lights()
    {
        if (!checkRequirements())
            return;

        vehicleController.input.lowBeamLights = !vehicleController.input.lowBeamLights;
        if (!vehicleController.input.lowBeamLights)
            vehicleController.input.fullBeamLights = false;
    }

    private void lightsFullBeam()
    {
        if (!checkRequirements())
            return;

        vehicleController.input.fullBeamLights = !vehicleController.input.fullBeamLights;
        if (vehicleController.input.fullBeamLights)
            vehicleController.input.lowBeamLights = false;
    }

    private void handbrake()
    {
        if (!checkRequirements())
            return;

        vehicleController.input.Handbrake = vehicleController.input.Handbrake == 0 ? 1f : 0f;
    }

    private void horn(bool doHorn)
    {
        if (!checkRequirements())
            return;

        vehicleController.input.horn = doHorn;
    }
}
