﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BezierUtil
{
    //TODO: evtl. jedem waypoint zwei positionen zuweißen, die hier als arr[1] arr[2] "errechnet" werden
    public static Vector3[] getBezierVecArr(Transform start, Transform end)
    {
        Vector3[] arr = new Vector3[4];

        /*
        arr[0] = start.position;
        arr[1] = arr[0] + start.forward * 2;
        arr[3] = end.position;
        arr[2] = arr[3] - end.forward * 2;
        */

        //TODO: .GetComponent<Waypoint>() nur zum testen -> umbauen
        Waypoint startWP = start.GetComponent<Waypoint>();

        
        arr[0] = start.transform.position;
        if(startWP == null) {
            //Debug.Log("getBezierVecArr startWP.GetComp<Waypoint>() == null ");
            arr[1] = arr[0] + start.forward;
        }
        else { 
            arr[1] = arr[0] + (startWP.getBezierDirectionalForward() - startWP.transform.position);
        }
        arr[3] = end.transform.position;
        arr[2] = arr[3] + (end.GetComponent<Waypoint>().getBezierDirectionalBackward() - end.transform.position);
        


        return arr;
    }

    // parameter t ranges from 0f to 1f
    public static Vector3 getBezierPosition(Transform start, Transform end, float t)
    {
        /*
        Vector3 p0 = start.position;
        Vector3 p1 = p0 + start.forward;
        Vector3 p3 = end.position;
        Vector3 p2 = p3 - end.forward;
        */

        //TODO: .GetComponent<Waypoint>() nur zum testen -> in Waypoint start, Waypoint end,.. umbauen
        Vector3[] bezierArr = getBezierVecArr(start, end);
        Vector3 p0 = bezierArr[0];
        Vector3 p1 = bezierArr[1];
        Vector3 p3 = bezierArr[3];
        Vector3 p2 = bezierArr[2];


        // here is where the magic happens!
        return Mathf.Pow(1f - t, 3f) * p0 + 3f * Mathf.Pow(1f - t, 2f) * t * p1 + 3f * (1f - t) * Mathf.Pow(t, 2f) * p2 + Mathf.Pow(t, 3f) * p3;
    }



    public static float getApproximatelyNearestBezierT(Transform start, Transform end, Vector3 position, int cycles)
    {
        //Vector3[] bezierArr = getBezierVecArr(start, end);

        float res = getApproximatelyNearestBezierT(start, end, position, cycles, 0f, 1f);

        //Debug.Log("res: "+res);

        return res;
    }

    public static float getApproximatelyNearestBezierT(Transform start, Transform end, Vector3 position, int cyclesLeft, float tMin, float tMax)
    {
        if (tMin < 0 || tMin > 1 || tMax < 0 || tMax > 1)
            throw new System.ArgumentException("0 <= tMin/tMax <= 1 - tMin: " + tMin + "\ttMax: " + tMax);

        //Debug.Log("getApproximatelyNearestBezierT tMin: "+ tMin+ "\ttMax: "+ tMax+"\tcyclesLeft: " + cyclesLeft);


        float tToTest = (tMin + tMax) / 2;
        float thisRes = Vector3.Distance(position, getBezierPosition(start, end, tToTest));


        if (cyclesLeft - 1 > 0)
        {
            float tChange = ((tMax - tMin) / 2);
            float newResPlus = Vector3.Distance(position, getBezierPosition(start, end, tToTest + tChange));
            float newResMinus = Vector3.Distance(position, getBezierPosition(start, end, tToTest - tChange));

            //Debug.Log("newResPlus: "+ newResPlus+ "\tnewResMinus: "+ newResMinus);

            if (newResPlus < newResMinus)
                return getApproximatelyNearestBezierT(start, end, position, cyclesLeft - 1, tToTest, tToTest + tChange);
            else
                return getApproximatelyNearestBezierT(start, end, position, cyclesLeft - 1, tToTest - tChange, tToTest);
        }

        return tToTest;
    }
}
