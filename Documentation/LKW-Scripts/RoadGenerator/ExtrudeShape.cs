﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtrudeShape
{
    Mesh mesh;

    public Vector2[] vert2Ds { get; internal set; }
    public Vector2[] normals { get; internal set; }
    public float[] us { get; internal set; }
    public int[] lines { get; internal set; }

    //Wegen den verschiedenen Auslegunden von Programmen der Achsen
    public bool switchYZ;

    public ExtrudeShape(Mesh mesh, bool switchYZ)
    {
        this.mesh = mesh;
        this.switchYZ = switchYZ;
        Init();
    }

    public void Init()
    {
        Vector3[] meshVerts = mesh.vertices;
        vert2Ds = new Vector2[meshVerts.Length];
        int mvI = 0;
        foreach(Vector3 mv in meshVerts)
        {
            if (switchYZ)
            {
                vert2Ds[mvI] = new Vector2(mv.x, mv.z);
            }
            else
            {
                vert2Ds[mvI] = new Vector2(mv.x, mv.y);
            }

            mvI++;
        }

        Vector3[] meshNormals = mesh.normals;
        normals = new Vector2[meshNormals.Length];
        int mni = 0;
        foreach (Vector3 mn in meshNormals)
        {
            if (switchYZ)
            {
                normals[mni] = new Vector2(mn.x, mn.z);
            }
            else
            {
                normals[mni] = new Vector2(mn.x, mn.y);
            }

            mni++;
        }

        Vector2[] meshUVs = mesh.uv;
        us = new float[meshUVs.Length];
        int mUi = 0;
        foreach (Vector2 uv in meshUVs)
        {
            us[mUi] = uv.x;

            mUi++;
        }


        List<int> linesList = new List<int>();
        Vector2 v;
        //Muss vert2Ds dafuer entsprechend sortiert sein?
        for (int v2i = 0; v2i < vert2Ds.Length-1; v2i++)
        {
            v = vert2Ds[v2i];

            //if (v2i+1 < vert2Ds.Length && v.Equals(vert2Ds[v2i + 1]))
            if (v.Equals(vert2Ds[v2i + 1]))
            {
                continue;
            }

            linesList.Add(v2i);
            linesList.Add(v2i + 1);

        }
        

    }
}
