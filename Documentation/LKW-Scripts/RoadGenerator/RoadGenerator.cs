﻿//#define compile
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Basierend auf https://www.youtube.com/watch?v=o9RK6O2kOKo

public class RoadGenerator : MonoBehaviour
{
#if compile



    public Mesh baseMesh2D;

    
    public void Extrude(Mesh mesh, ExtrudeShape shape, OrientedPoint[] path)
    {


        int vertsInShape = shape.vert2Ds.Length;
        int segments = path.Length - 1;
        int edgeLoops = path.Length;
        int vertCount = vertsInShape * edgeLoops;
        int triCount = shape.lines.Length * segments;
        int triIndexCount = triCount * 3;

        int[] triangleIndeices = new int[triIndexCount];
        Vector3[] verticies = new Vector3[vertCount];
        Vector3[] normals = new Vector3[vertCount];
        Vector2[] uvs = new Vector2[vertCount];







        for(int i = 0; i < path.Length; i++)
        {
            int offset = i * vertsInShape;
            for(int j = 0; j < vertsInShape; j++)
            {
                int id = offset + j;
                verticies[id] = path[i].LocalToWorld(shape.vert2Ds[j].point);
                normals[id] = path[i].LocalToWorldDirection(shape.vert2Ds[j].normal);
                uvs[id] = new Vector2(vert2Ds[j].uCoord, i / ((float)edgeLoops));
            }
        }


        int ti = 0;
        for(int i = 0; i < segments; i++)
        {
            int offset = i * vertsInShape;
            for(int l = 0; l < lines.Length; l += 2)
            {
                int a = offset + lines[l] + vertsInShape;
                int b = offset + lines[l];
                int c = offset + lines[l + 1];
                int d = offset + lines[l + 1] + vertsInShape;

                triangleIndeices[ti] = a;
                ti++;
                triangleIndeices[ti] = b;
                ti++;
                triangleIndeices[ti] = c;
                ti++;
                triangleIndeices[ti] = c;
                ti++;
                triangleIndeices[ti] = d;
                ti++;
                triangleIndeices[ti] = a;
                ti++;



            }
        }







        mesh.Clear();
        mesh.vertices = verticies;
        mesh.triangles = triangleIndeices;
        mesh.normals = normals;
        mesh.uv = uvs;

    }

#endif
}
