﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SplineMesh;
using System;

public class SplineMeshTilingGaps : SplineMeshTilingBase
{
    [System.Serializable]
    public class SplineGap
    {
        public int curveNodeIndexStart;
        public int curveNodeIndexEnd;
    }

    public List<SplineGap> splineGaps;

    protected override void doCurveSpace(List<GameObject> used)
    {
        checkSplineGaps();

        int i = 0;
        foreach (CubicBezierCurve curve in spline.curves)
        {
            if (!isGap(i))
            {
                GameObject go = FindOrCreate("segment " + i + " mesh");

                go.GetComponent<MeshBender>().SetInterval(curve);
            
                used.Add(go);
                tagObject(go);
            }

            i++;
        }
    }

    protected override void doNonCurveSpace(List<GameObject> used)
    {
        if (splineGaps.Count == 0)
        {
            GameObject go = FindOrCreate("segment 1 mesh");
            go.GetComponent<MeshBender>().SetInterval(spline, 0);
            used.Add(go);
            tagObject(go);
            return;
        }

        checkSplineGaps();

        bool needToDoEnd = false;
        int start = 0;
        int end = 0;
        float currentLengthCounter = 0;
        float lengthStart = 0;
        float lengthEnd = 0;
        CubicBezierCurve curve;
        int createdElements=0;
        for(int i= 0; i < spline.curves.Count; i++ )
        {
            
            curve = spline.curves[i];

            if (isGap(i))
            {
                if (start < end) {
                    GameObject go = FindOrCreate("segment " + createdElements++ + " mesh");

                    go.GetComponent<MeshBender>().SetInterval(spline, lengthStart, lengthEnd);

                    used.Add(go);
                    tagObject(go);
                    needToDoEnd = false;
                }

                start = i+1;
                lengthStart = currentLengthCounter + curve.Length;
            }
            else
            {
                end = i+1;
                lengthEnd = currentLengthCounter + curve.Length;
                needToDoEnd = true;
            }

            currentLengthCounter += curve.Length;

            //Debug.Log("start: " + start + "(" + lengthStart + ")\tend: " + end + "(" + lengthEnd + ")\tisGap(" + i + "): " + isGap(i));
        }

        if (needToDoEnd)
        {
            GameObject go = FindOrCreate("segment " + createdElements++ + " mesh");

            go.GetComponent<MeshBender>().SetInterval(spline, lengthStart, lengthEnd);

            used.Add(go);
            tagObject(go);
            needToDoEnd = false;
        }

    }

    private void checkSplineGaps()
    {

        int lastEndNode = 0;
        foreach(SplineGap g in splineGaps)
        {
            if (g.curveNodeIndexStart >= g.curveNodeIndexEnd)
                Debug.LogError("curveNodeIndexStart is lower (or equal) than curveNodeIndexEnd");
            if (g.curveNodeIndexStart < 0)
                Debug.LogError("curveNodeIndexStart is lower than 0");
            if (g.curveNodeIndexEnd > spline.curves.Count)
                Debug.LogError("curveNodeIndexEnd is higher than the last node");
            if(g.curveNodeIndexEnd < lastEndNode)
                Debug.LogError("curveNodeIndexEnd is lower than the lastEndNode");

            lastEndNode = g.curveNodeIndexEnd;
        }
    }

    private bool isGap(int curveIndex)
    {
        

        foreach (SplineGap g in splineGaps)
        {
            if (curveIndex >= g.curveNodeIndexStart && curveIndex < g.curveNodeIndexEnd)
                return true;
        }

        return false;
    }
}
