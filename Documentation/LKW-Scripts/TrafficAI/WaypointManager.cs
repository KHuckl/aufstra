﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointManager : MonoBehaviour
{
    List<GameObject> waypoints;

    bool isInited = false;

    // Start is called before the first frame update
    void Awake()
    {
        waypoints = new List<GameObject>(GameObject.FindGameObjectsWithTag("Waypoint"));
    }

    //Waypoint der am naechsten ist, aber nicht das uebergebene GO ist
    public GameObject getNearestWaypoint(GameObject go)
    {
        float currNearestDist = -1f;
        GameObject currNearest = null;

        float dist;
        foreach (GameObject wp in waypoints)
        {
            if (wp.Equals(go))
                continue;

            dist = Vector3.Distance(go.transform.position, wp.transform.position);
            if (dist < currNearestDist || currNearest == null)
            {
                currNearestDist = dist;
                currNearest = wp;
            }
        }

        return currNearest;
    }

    //Naechster Waypoint von uebergebener Position
    public GameObject getNearestWaypoint(Vector3 pos)
    {
        float currNearestDist = -1f;
        GameObject currNearest = null;

        float dist;
        foreach(GameObject wp in waypoints)
        {
            dist = Vector3.Distance(pos, wp.transform.position);
            if (dist < currNearestDist || currNearest == null)
            {
                currNearestDist = dist;
                currNearest = wp;
            }
        }

        return currNearest;
    }
}
