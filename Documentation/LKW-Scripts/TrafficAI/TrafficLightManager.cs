﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLightManager : MonoBehaviour
{
    //Nur eine Gruppe(innere Liste) hat gruenes Licht, alle anderen waehrend dessen rot
    public List<TrafficLightGroup> tlGroups;
    int currGreenGroup=0;

    //Wie lange ist die Gruene Phase
    public float greenTime=15;
    protected float greenTimePast;

    public float yellowTime = 5;

    protected bool isYellowTime;

    private void Start()
    {
        greenTimePast = greenTime;
    }

    // Update is called once per frame
    void Update()
    {
        greenTimePast += Time.deltaTime;

        if(greenTimePast >= greenTime)
        {
            greenPhaseEnded();
        }


    }

    void greenPhaseEnded()
    {
        if (isYellowTime)
            return;

        int i = 0;
        foreach (TrafficLightGroup tlg in tlGroups)
        {
            if(i == currGreenGroup)
            {
                setGroup(tlg, TS_TrafficLight.Light.Yellow);
            }
            else
            {
                setGroup(tlg, TS_TrafficLight.Light.Red);
            }


            i++;
        }

        StartCoroutine(startGreenPhase());
    }

    IEnumerator startGreenPhase()
    {
        isYellowTime = true;
        yield return new WaitForSeconds(yellowTime);
        

        setNextGreenGroup();

        int i = 0;
        foreach (TrafficLightGroup tlg in tlGroups)
        {
            if (i == currGreenGroup)
            {
                setGroup(tlg, TS_TrafficLight.Light.Green);
            }
            else
            {
                setGroup(tlg, TS_TrafficLight.Light.Red);
            }
            


            i++;
        }

        greenTimePast = 0f;
        isYellowTime = false;
    }

    void setNextGreenGroup()
    {
        if(currGreenGroup+1 >= tlGroups.Count)
        {
            currGreenGroup = 0;
        }
        else
        {
            currGreenGroup++;
        }
    }

    void setGroup(TrafficLightGroup tlGroup, TS_TrafficLight.Light light)
    {
        foreach(TS_TrafficLight tl in tlGroup.tlGroupMembers)
        {
            tl.setLight(light);
        }
    }

    [System.Serializable]
    public class TrafficLightGroup
    {
        public List<TS_TrafficLight> tlGroupMembers;
    }
}
