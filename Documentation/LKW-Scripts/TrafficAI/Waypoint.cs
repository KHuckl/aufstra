﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Waypoint : MonoBehaviour
{
    public enum Direction
    {
        Straight,
        Left,
        Right
    }
    public Direction direction;

    [SerializeField]
    protected Transform bezierDirectionalForward;
    [SerializeField]
    protected Transform bezierDirectionalBackward;

    public GameObject nextWaypoint;
    protected Waypoint nextWaypointScr;

    public List<Waypoint> optionalNextWaypoints;

    //TODO: previousWaypoint ist nicht vernuenftig implementiert (muss in WaypointDirection noch in "connectWaypoints" gesetzt werden
    //wird auch ggf. nicht benoetigt
    //public GameObject previousWaypoint;

    float countAsNextPreviousWaypointDistance = 0.2f;

    public float recommendedSpeedKMH=5f;

    public float overrideRecommendedSpeedKMH = -1f;

    // Start is called before the first frame update
    void Awake()
    {
        Init();
    }

    public void Init()
    {
        //Marker verstecken
        transform.GetChild(0).gameObject.SetActive(false);
        bezierDirectionalForward.gameObject.SetActive(false);
        bezierDirectionalBackward.gameObject.SetActive(false);

        //Nahe Waypoints automatisch setzten
        //Dann muessen in Prefabs nur die waypoints des eigenen Bereichs gesetzt werden und die des naechsten Prefabs werden automatisch erkannt
        GameObject nearestWP = getWPM().getNearestWaypoint(gameObject);

        bool autoConnected = false;

        if (nextWaypoint == null && Vector3.Distance(transform.position, nearestWP.transform.position) <= countAsNextPreviousWaypointDistance)
        {
            nextWaypoint = nearestWP;
            autoConnected = true;
        }
        if (nextWaypoint != null) {
            nextWaypointScr = nextWaypoint.GetComponent<Waypoint>();


            
            if (autoConnected) {


                //Sollte der Konnektor eine niedrigere Geschwindigkeit haben, diese uebernehmen
                if (recommendedSpeedKMH > nextWaypointScr.recommendedSpeedKMH)
                {
                    //Dieser Waypoint hat eine zu hohe Geschwindigkeit
                    overrideRecommendedSpeedKMH = nextWaypointScr.recommendedSpeedKMH;
                }
                else if(recommendedSpeedKMH < nextWaypointScr.recommendedSpeedKMH)
                {
                    //Der naechste Waypoint hat eine zu hohe Geschwindigkeit
                    nextWaypointScr.overrideRecommendedSpeedKMH = recommendedSpeedKMH;
                }



                //Die bezierDirectionalForward/Backward position abgleichen
                nextWaypointScr.bezierDirectionalBackward.position = bezierDirectionalBackward.position;
                bezierDirectionalForward.position = nextWaypointScr.bezierDirectionalForward.position;

            }



        }

        /*if (previousWaypoint == null && Vector3.Distance(transform.position, nearestWP.transform.position) <= countAsNextPreviousWaypointDistance)
        {
            previousWaypoint = nearestWP;
        }*/

    }

    public Waypoint getNextWaypoint()
    {
        if (nextWaypointScr == null && optionalNextWaypoints.Count > 0)
            return getRandomNextWaypoint();

        return nextWaypointScr;
    }

    public Waypoint getRandomNextWaypoint()
    {
        if (optionalNextWaypoints.Count == 0)
            return getNextWaypoint();

        int r = UnityEngine.Random.Range(0, optionalNextWaypoints.Count+1);

        if (r == optionalNextWaypoints.Count)
            return getNextWaypoint();

        return optionalNextWaypoints[r];
    }


    public float getCountAsNextPreviousWaypointDistance()
    {
        return countAsNextPreviousWaypointDistance;
    }

    public float getRecommendedSpeedKMH()
    {
        if (overrideRecommendedSpeedKMH != -1)
            return overrideRecommendedSpeedKMH;

        return recommendedSpeedKMH;
    }

    static WaypointManager wpm;
    static WaypointManager getWPM()
    {
        if (wpm == null)
            wpm = GameObject.FindGameObjectWithTag("CoreObjects").transform.Find("WaypointManager").GetComponent<WaypointManager>();

        return wpm;
    }

    public Vector3 getBezierDirectionalForward()
    {
        if (bezierDirectionalForward == null)
            return transform.forward;

        return bezierDirectionalForward.position;
    }

    public Vector3 getBezierDirectionalBackward()
    {
        if (bezierDirectionalBackward == null)
            return transform.forward * -1;

        return bezierDirectionalBackward.position;
    }



#if UNITY_EDITOR

    public void setbezierDirectionalForwardPosition(Vector3 pos)
    {
        bezierDirectionalForward.transform.position = pos;
    }
    public void setbezierDirectionalBackwardPosition(Vector3 pos)
    {
        bezierDirectionalBackward.transform.position = pos;
    }

    public void resetbezierDirectionalForwardPosition()
    {
        bezierDirectionalForward.transform.localPosition = new Vector3(0, 0, 2);
    }
    public void resetbezierDirectionalBackwardPosition()
    {
        bezierDirectionalBackward.transform.localPosition = new Vector3(0, 0, -2);
    }



    //void OnDrawGizmosSelected()
    void OnDrawGizmos()
    {

        if (nextWaypoint != null && Vector3.Distance(nextWaypoint.transform.position, transform.position) > countAsNextPreviousWaypointDistance) {
            //DrawArrowToPosition(transform.position, nextWaypoint.transform.position);
            DrawPathToWaypoint(nextWaypoint.transform);

            Handles.Label(transform.position + new Vector3(0, 1f, 0), "Speed: " + recommendedSpeedKMH);
        }
        else if(nextWaypoint != null)
        {
            Handles.Label(transform.position + new Vector3(0, 1f, 0), "Speed: " + recommendedSpeedKMH + " -> " + nextWaypointScr.recommendedSpeedKMH);
        }
        else
        {
            Handles.Label(transform.position + new Vector3(0, 1f, 0), "Speed: " + recommendedSpeedKMH);
        }

        
        foreach (Waypoint wp in optionalNextWaypoints)
        {
            DrawPathToWaypoint(wp.transform);
        }
    }

    private void DrawPathToWaypoint(Transform wp)
    {
        if (wp == null || Vector3.Distance(wp.transform.position, transform.position) <= countAsNextPreviousWaypointDistance)
            return;

        //Pfeil direkt zum naechsten WP
        /*
        DrawArrowToPosition(transform.position, wp.transform.position);
        */

        
        //Pfeile bilden Bezierkurve nach
        float bezierDrawIncrement = 0.1f;
        Vector3 bezierPos;
        Vector3? oldPos = null;
        for (float i = bezierDrawIncrement; i <= 1; i += bezierDrawIncrement)
        {
            bezierPos = BezierUtil.getBezierPosition(transform, wp.transform, i);
            DrawArrowToPosition(oldPos == null ? transform.position : oldPos.Value, bezierPos);
            oldPos = bezierPos;
        }
        DrawArrowToPosition(oldPos == null ? transform.position : oldPos.Value, BezierUtil.getBezierPosition(transform, wp.transform, 1));
        
        //TODO: TEST
        //DrawArrowToPosition(transform.position, BezierUtil.getBezierPosition(transform, wp.transform, 0.5f));
    }

    void DrawArrowToPosition(Vector3 start, Vector3 end)
    {
        Vector3 dir = end - start;
        DrawArrow.ForGizmo(start + new Vector3(0, 0.2f, 0), dir, Color.red);
    }
#endif
}
