﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NWH.VehiclePhysics;

public abstract class TrafficSign : MonoBehaviour
{
    protected bool okToDrive;

    public List<Waypoint> affectedWaypoints;

    // Start is called before the first frame update
    void Start()
    {
        doStart();
    }

    // Update is called once per frame
    void Update()
    {
        doUpdate();
    }

    protected virtual void doStart()
    {
    }

    protected virtual void doUpdate()
    {
    }

    public virtual bool getOkToDrive(VehicleController vCon)
    {
        return okToDrive;
    }

    private void OnDrawGizmosSelected()
    {
        foreach (Waypoint wp in affectedWaypoints)
        {
            if(wp != null)
                DrawArrowToWaypoint(wp.transform);
        }
    }

    void DrawArrowToWaypoint(Transform wp)
    {
        Vector3 dir = wp.transform.position - transform.position;
        DrawArrow.ForGizmo(transform.position + new Vector3(0, 0.2f, 0), dir, Color.red);
    }
}
