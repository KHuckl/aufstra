﻿using NWH.VehiclePhysics;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TS_Stop : TrafficSign
{
    //public List<TriggerDetector> watchZones;
    public TriggerDetector watchZone;

    //Fahrzeuge, auf die gewartet werden muss
    List<VehicleController> carsInWatchZone = new List<VehicleController>();

    //Fahrzeuge, die gerade am stehen bleiben sind; Nach ablauf bestimmter Zeit werden sie in "carsStopped" ueberfuehrt
    List<VehicleController> carsAreStopping = new List<VehicleController>();
    //Alle Fahrzeuge, die gestoppt haben und sobald frei ist, weiter fahren duerfen
    List<VehicleController> carsStopped = new List<VehicleController>();

    protected override void doStart()
    {
        watchZone.EventOnTriggerEnter += WatchZoneCarEnter;
        watchZone.EventOnTriggerExit += WatchZoneCarExit;
    }

    private void OnDestroy()
    {
        watchZone.EventOnTriggerEnter += WatchZoneCarEnter;
        watchZone.EventOnTriggerExit += WatchZoneCarExit;
    }

    public override bool getOkToDrive(VehicleController vCon)
    {
        okToDrive = false;

        if (vCon.Speed < 0.01f && !carsStopped.Contains(vCon))
        {
            //2 Sekunden warten
            StartCoroutine(AddCarToStoppedList(vCon, 2));
            //Nach 10 Sekunden wieder entfernen
            StartCoroutine(RemoveCarFromStoppedList(vCon, 10));
        }

        if (carsStopped.Contains(vCon)) { 
            if (carsInWatchZone.Count > 1)
                okToDrive = false;
            else if (carsInWatchZone.Count == 0)
                okToDrive = true;
            else if (carsInWatchZone[0].Equals(vCon))
                okToDrive = true;
            else
                okToDrive = false;
        }

        return okToDrive;

    }

    //Fahrzeuge, die gewartet haben muessen sich gemerkt werden, da sie sonst immer kurz anfahren und wieder stehen bleiben usw.
    private IEnumerator AddCarToStoppedList(VehicleController vCon, int v)
    {
        if (carsAreStopping.Contains(vCon))
            yield break;

        carsAreStopping.Add(vCon);

        yield return new WaitForSeconds(v);

        carsAreStopping.Remove(vCon);
        carsStopped.Add(vCon);
        
    }

    //Fahrzeuge die wartet haben, muessen auch irgendwann wieder aus der Liste entfernt werden, da bei neuem anfahren an das selbe Schild, sonst nicht mehr gewartet werden muesste
    private IEnumerator RemoveCarFromStoppedList(VehicleController vCon, int v)
    {
        yield return new WaitForSeconds(v);

        carsStopped.Remove(vCon);
    }

    protected void WatchZoneCarEnter(Collider other)
    {
        if (other.isTrigger)
            return;

        if (other.attachedRigidbody == null)
            return;

        if (other.attachedRigidbody.tag.Equals("Vehicle"))
        {
            VehicleController vCon = other.attachedRigidbody.GetComponent<VehicleController>();
            if (!carsInWatchZone.Contains(vCon))
            {
                carsInWatchZone.Add(vCon);
                okToDrive = false;
            }

        }
    }

    protected void WatchZoneCarExit(Collider other)
    {
        if (other.isTrigger)
            return;

        if (other.attachedRigidbody == null)
            return;

        if (other.attachedRigidbody.tag.Equals("Vehicle"))
        {
            VehicleController vCon = other.attachedRigidbody.GetComponent<VehicleController>();
            if (carsInWatchZone.Contains(vCon))
            {
                carsInWatchZone.Remove(vCon);
            }
        }
    }


}
