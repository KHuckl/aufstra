﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TS_TrafficLight : TrafficSign
{
    public enum Light
    {
        Red,
        Yellow,
        Green
    }

    protected Light currLight;

    public GameObject lightRed;
    public GameObject lightYellow;
    public GameObject lightGreen;

    protected override void doStart()
    {
        setLight(Light.Red);
    }

    public void setLight(Light newLight)
    {
        currLight = newLight;

        switch (currLight)
        {
            case Light.Red:
                okToDrive = false;
                lightRed.SetActive(true);
                lightYellow.SetActive(false);
                lightGreen.SetActive(false);
                break;
            case Light.Yellow:
                okToDrive = false;
                lightRed.SetActive(false);
                lightYellow.SetActive(true);
                lightGreen.SetActive(false);
                break;
            case Light.Green:
                okToDrive = true;
                lightRed.SetActive(false);
                lightYellow.SetActive(false);
                lightGreen.SetActive(true);
                break;
        }
    }
}
