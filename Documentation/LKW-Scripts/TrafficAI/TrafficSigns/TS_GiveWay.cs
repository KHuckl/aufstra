﻿using NWH.VehiclePhysics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TS_GiveWay : TrafficSign
{
    public TriggerDetector watchZone;

    List<VehicleController> carsInWatchZone = new List<VehicleController>();

    List<AICar> hasStopped = new List<AICar>();

    protected override void doStart()
    {
        watchZone.EventOnTriggerEnter += WatchZoneCarEnter;
        watchZone.EventOnTriggerExit += WatchZoneCarExit;
    }

    private void OnDestroy()
    {
        watchZone.EventOnTriggerEnter += WatchZoneCarEnter;
        watchZone.EventOnTriggerExit += WatchZoneCarExit;
    }

    public override bool getOkToDrive(VehicleController vCon)
    {
        
        if (carsInWatchZone.Count > 1)
            okToDrive = false;
        else if (carsInWatchZone.Count == 0)
            okToDrive = true;
        else if (carsInWatchZone[0].Equals(vCon))
            okToDrive = true;
        else
            okToDrive = false;
        
        return okToDrive;
    }

    protected void WatchZoneCarEnter(Collider other)
    {
        if (other.isTrigger)
            return;

        if (other.attachedRigidbody == null)
            return;

        if (other.attachedRigidbody.tag.Equals("Vehicle"))
        {
            VehicleController vCon = other.attachedRigidbody.GetComponent<VehicleController>();
            if (!carsInWatchZone.Contains(vCon))
            {
                carsInWatchZone.Add(vCon);
                okToDrive = false;
            }

        }
    }

    protected void WatchZoneCarExit(Collider other)
    {
        if (other.isTrigger)
            return;

        if (other.attachedRigidbody == null)
            return;

        if (other.attachedRigidbody.tag.Equals("Vehicle"))
        {
            VehicleController vCon = other.attachedRigidbody.GetComponent<VehicleController>();
            if (carsInWatchZone.Contains(vCon))
            {
                carsInWatchZone.Remove(vCon);
            }
        }
    }


}
