﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionManagerForward : MonoBehaviour
{
    public enum Speed
    {
        Slow,
        Normal
    }

    public enum Direction
    {
        Left,
        Straight,
        Right
    }


    public TriggerDetectorCollider tDetForwardSubLeft;
    public TriggerDetectorCollider tDetForwardSubCenter;
    public TriggerDetectorCollider tDetForwardSubRight;
    public TriggerDetectorCollider tDetForwardSubLeft_Slow;
    public TriggerDetectorCollider tDetForwardSubCenter_Slow;
    public TriggerDetectorCollider tDetForwardSubRight_Slow;

    public void setDetectorCollider(TriggerDetector td, Speed speed, Direction direction)
    {
        if (Speed.Normal.Equals(speed))
        {
            switch (direction)
            {
                case Direction.Left:
                    td.SetSubColliderActive(tDetForwardSubLeft, true);
                    td.SetSubColliderActive(tDetForwardSubCenter, true);
                    td.SetSubColliderActive(tDetForwardSubRight, false);
                    td.SetSubColliderActive(tDetForwardSubLeft_Slow, false);
                    td.SetSubColliderActive(tDetForwardSubCenter_Slow, false);
                    td.SetSubColliderActive(tDetForwardSubRight_Slow, false);
                    break;
                case Direction.Straight:
                    td.SetSubColliderActive(tDetForwardSubLeft, true);
                    td.SetSubColliderActive(tDetForwardSubCenter, true);
                    td.SetSubColliderActive(tDetForwardSubRight, true);
                    td.SetSubColliderActive(tDetForwardSubLeft_Slow, false);
                    td.SetSubColliderActive(tDetForwardSubCenter_Slow, false);
                    td.SetSubColliderActive(tDetForwardSubRight_Slow, false);
                    break;
                case Direction.Right:
                    td.SetSubColliderActive(tDetForwardSubLeft, false);
                    td.SetSubColliderActive(tDetForwardSubCenter, true);
                    td.SetSubColliderActive(tDetForwardSubRight, true);
                    td.SetSubColliderActive(tDetForwardSubLeft_Slow, false);
                    td.SetSubColliderActive(tDetForwardSubCenter_Slow, false);
                    td.SetSubColliderActive(tDetForwardSubRight_Slow, false);
                    break;
            }
        }
        else
        {
            switch (direction)
            {
                case Direction.Left:
                    td.SetSubColliderActive(tDetForwardSubLeft, false);
                    td.SetSubColliderActive(tDetForwardSubCenter, false);
                    td.SetSubColliderActive(tDetForwardSubRight, false);
                    td.SetSubColliderActive(tDetForwardSubLeft_Slow, true);
                    td.SetSubColliderActive(tDetForwardSubCenter_Slow, true);
                    td.SetSubColliderActive(tDetForwardSubRight_Slow, false);
                    break;
                case Direction.Straight:
                    td.SetSubColliderActive(tDetForwardSubLeft, false);
                    td.SetSubColliderActive(tDetForwardSubCenter, false);
                    td.SetSubColliderActive(tDetForwardSubRight, false);
                    td.SetSubColliderActive(tDetForwardSubLeft_Slow, true);
                    td.SetSubColliderActive(tDetForwardSubCenter_Slow, true);
                    td.SetSubColliderActive(tDetForwardSubRight_Slow, true);
                    break;
                case Direction.Right:
                    td.SetSubColliderActive(tDetForwardSubLeft, false);
                    td.SetSubColliderActive(tDetForwardSubCenter, false);
                    td.SetSubColliderActive(tDetForwardSubRight, false);
                    td.SetSubColliderActive(tDetForwardSubLeft_Slow, false);
                    td.SetSubColliderActive(tDetForwardSubCenter_Slow, true);
                    td.SetSubColliderActive(tDetForwardSubRight_Slow, true);
                    break;
            }

        }
    }
}
