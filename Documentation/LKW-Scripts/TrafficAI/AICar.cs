﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NWH.VehiclePhysics;
using System;
using UnityEditor;

public class AICar : MonoBehaviour
{
    public GameObject carGO;
    VehicleChanger vCan;
    VehicleController vCon;

    GameObject positionGO;

    CollisionDetector aiColDet;
    public TriggerDetector tDetForward;
    List<Collider> obstaclesForward = new List<Collider>();
    public TriggerDetector tDetBackward;
    protected DetectionManagerForward dmf;
    List<Collider> obstaclesBackward = new List<Collider>();
    public float speedCountAsSlow = 10f;
    public float steerAngleDetectorThreshold = 30f;
    public TriggerDetector tDetTrafficSigns;
    List<Collider> trafficSigns = new List<Collider>();
    public TriggerDetector tDetTrafficSignsStop;
    List<Collider> trafficSignsStop = new List<Collider>();


    public bool DEBUGSETACTIVEAI;

    public bool startAIOnScenarioBegin;
    bool startedAIOnScenarioBegin = false;
    bool isActiveAI;

    //bool standard_FreezeWhenStill;
    bool standard_FreezeWhenInactive;
    Transmission.TransmissionType standard_TransmissionType;


    public Transform debugTargetPosTransform;

    public bool ConnectTrailerOnStart;

    //Wie oft rekursiv nach der naechsten Stelle gesucht werden soll
    protected int bezierApprCycles = 80;
    //Multiplikator wie weit vorausgeschaut wird
    public float bezierForwardMulti = 0.1f;

    //AI
    WaypointManager wpm;
    float waypointNearEnough = 2.5f;

    Waypoint nextWaypoint;
    Waypoint activeWaypoint;
    Waypoint oldWaypoint;

    bool hadCollision = false;

    float collisionThreshold = 5f;//2f;

    public float distanceToCheckForObstacle;

    //Ab welchem Wert das Fahrzeug als rutschend gilt
    public float wheelSlipThreshold = 0.1f;
    //Wenn Fahrzeug langsamer als die angegebene Geschwindigkeit faehrt, wird trotz rutschen gas gegeben
    public float wheelSlipAboveSpeed = 20f;

    //Wenn gewaehlt faehrt das Fahrzeug nur 2/3 der angegebenen Geschwindigkeit (Fuer schwere Fahrzeuge, zb. Bus)
    public bool drivesSlow;
    float carTargetSpeed;
    //




    // Start is called before the first frame update
    void Start()
    {
        if (carGO == null)
            carGO = transform.parent.gameObject;

        positionGO = transform.Find("PositionGameObject").gameObject;
        dmf = transform.Find("DetectionManagerForward").GetComponent<DetectionManagerForward>();

        isActiveAI = false;
        aiColDet = carGO.AddComponent<CollisionDetector>();
        aiColDet.EventOnCollisionEnter += AICarCollition;

        tDetForward.EventOnTriggerEnter += obstacleForwardEnter;
        tDetForward.EventOnTriggerExit += obstacleForwardExit;
        tDetBackward.EventOnTriggerEnter += obstacleBackwardEnter;
        tDetBackward.EventOnTriggerExit += obstacleBackwardExit;
        tDetTrafficSigns.EventOnTriggerEnter += trafficSignEnter;
        tDetTrafficSigns.EventOnTriggerExit += trafficSignExit;
        tDetTrafficSignsStop.EventOnTriggerEnter += trafficSignStopEnter;
        tDetTrafficSignsStop.EventOnTriggerExit += trafficSignStopExit; 

        vCon = carGO.GetComponent<VehicleController>();
        //standard_FreezeWhenStill = vCon.freezeWhenStill;
        standard_FreezeWhenInactive = vCon.freezeWhenInactive;
        standard_TransmissionType = vCon.transmission.transmissionType;

        GameObject core = GameObject.FindGameObjectWithTag("CoreObjects");
        vCan = core.transform.Find("_VehicleManager").GetComponent<VehicleChanger>();
        wpm = core.transform.Find("WaypointManager").GetComponent<WaypointManager>();

        DEBUGSETACTIVEAI = isActiveAI;
    }

    // Update is called once per frame
    void Update()
    {
        //testFollowTriggerDetectors();

        if (isActiveAI && vCon.Equals(vCan.ActiveVehicleController))
        {
            SetAIActive(false);
        }
        else if (startAIOnScenarioBegin && !startedAIOnScenarioBegin)
        {
            SetAIActive(true);
            startedAIOnScenarioBegin = true;
            DEBUGSETACTIVEAI = true;
        }
        else
        {
            if (DEBUGSETACTIVEAI != isActiveAI)
            {
                SetAIActive(DEBUGSETACTIVEAI);
            }
        }

        if (isActiveAI) {
            doAI();
        }
    }
    
    public bool getIsActiveAI()
    {
        return isActiveAI;
    }

    public void SetAIActive(bool active)
    {
        isActiveAI = active;

        //Kollision zurueckssetzen
        hadCollision = false;

        //vCon.freezeWhenStill = active ? false : standard_FreezeWhenStill;
        vCon.freezeWhenInactive = active ? false : standard_FreezeWhenInactive;
        vCon.transmission.transmissionType = active ? Transmission.TransmissionType.Automatic : standard_TransmissionType;

        if (active && ConnectTrailerOnStart)
            connectTrailer();

        vCon.Active = active;

    }

    protected GameObject getPositionGO()
    {
        //return carGO;
        return positionGO;
    }

    protected void doAI()
    {

        setWaypoint();
        
        if (activeWaypoint == null)
            return;

        setSteering();

        setAccelBrake();

        setHorn();

        setIndicators();
    }

    protected void setHorn()
    {
        if(hadCollision)
            vCon.input.horn = true;
        else
            vCon.input.horn = false;
    }

    private void setIndicators()
    {
        if (hadCollision)
        {
            SetIndicator(0, true);

            return;
        }
        else
        {
            SetIndicator(0, false);
        }
        
        if (Waypoint.Direction.Left.Equals(activeWaypoint.direction) || Waypoint.Direction.Left.Equals(nextWaypoint.direction))
        {
            SetIndicator(1, true);
            return;
        }
        else if (Waypoint.Direction.Right.Equals(activeWaypoint.direction) || Waypoint.Direction.Right.Equals(nextWaypoint.direction))
        {
            SetIndicator(2, true);
            return;
        }


        if (Waypoint.Direction.Straight.Equals(activeWaypoint.direction))
        {
            SetIndicator(1, false);
            SetIndicator(2, false);
        }
    }

    //0=Hazard; 1=Left; 2=Right
    protected void SetIndicator(int index, bool val)
    {
        switch (index)
        {
            case 0:
                if (val ? !vCon.input.hazardLights : vCon.input.hazardLights) {
                    vCon.input.hazardLights = val;
                }
                break;
            case 1:
                if (val ? !vCon.input.leftBlinker : vCon.input.leftBlinker) {
                    vCon.input.leftBlinker = val;
                }
                break;
            case 2:
                if (val ? !vCon.input.rightBlinker : vCon.input.rightBlinker) {
                    vCon.input.rightBlinker = val;
                }
                break;
        }
    }

    protected void setSteering()
    {
        //Funktioniert, aber steuert nur direkt auf die Waypoints zu
        /*
        Vector3 target = activeWaypoint.transform.position;
        //Vector3 heading = target - carGO.transform.position;
        Vector3 heading = target - getPositionGO().transform.position;
        //float dirNum = Angle(carGO.transform.forward, heading, carGO.transform.up);
        float dirNum = Angle(getPositionGO().transform.forward, heading, getPositionGO().transform.up);

        vCon.input.Horizontal = dirNum*0.2f;

        if (debugTargetPosTransform != null)
        {
            debugTargetPosTransform.position = target;
        }
        */
        //




        //Bezier
        Vector3 target;
        if (oldWaypoint == null)
        {
            target = BezierUtil.getBezierPosition(getPositionGO().transform, activeWaypoint.transform, 1f);
        }
        else
        {
            //Position des Fahrzeugs + Wert um vorneliegende Position zu finden (nicht die an der das Fahrzeug jetzt gerade ist)
            Vector3 posForBezier = getPositionGO().transform.position + ((activeWaypoint.transform.position - oldWaypoint.transform.position) * bezierForwardMulti);

            target = BezierUtil.getBezierPosition(oldWaypoint.transform, activeWaypoint.transform, BezierUtil.getApproximatelyNearestBezierT(oldWaypoint.transform, activeWaypoint.transform, posForBezier, bezierApprCycles));
        }

        Vector3 heading = target - getPositionGO().transform.position;
        //Debug.Log("heading: "+ heading);
        //float dirNum = Angle(getPositionGO().transform.forward, heading, getPositionGO().transform.up);
        Vector3 vec3forward = new Vector3(getPositionGO().transform.forward.x, 0, getPositionGO().transform.forward.z);
        Vector3 vec3Heading = new Vector3(heading.x, 0, heading.z);
        //TODO Deg2Rad/Rad2Deg weg
        float dirNum = Vector3.Angle(vec3forward, vec3Heading) * Mathf.Deg2Rad * AngleDir(getPositionGO().transform.forward, heading, getPositionGO().transform.up);
        dirNum *= Mathf.Rad2Deg;

        float steerStrength = dirNum / vCon.steering.lowSpeedAngle;
        //Debug.Log("dirNum: "+ dirNum+"\tsteerStrength: " + steerStrength);
        vCon.input.Horizontal = steerStrength;

        if (debugTargetPosTransform != null)
        {
            debugTargetPosTransform.position = target;
        }






        //Lenkende Raeder holen und mit threshold zum ausschalten der Detectoren vergleichen
        float completeAngle = 0;
        int i = 0;
        foreach (Axle axle in vCon.axles)
        {
            //Pruefung darauf ob Achse ueberhaupt lenken kann -> wenn nicht, nicht einberechnen
            if (axle.geometry.steerCoefficient == 0) {
                continue;
            }


            completeAngle += Math.Abs(axle.leftWheel.SteerAngle);
            completeAngle += Math.Abs(axle.rightWheel.SteerAngle);
            i += 2;
        }
        completeAngle /= i;

        if (completeAngle >= steerAngleDetectorThreshold)
        {
            dmf.setDetectorCollider(tDetForward,
                vCon.SpeedKPH <= speedCountAsSlow ? DetectionManagerForward.Speed.Slow : DetectionManagerForward.Speed.Normal,
                dirNum < 0 ? DetectionManagerForward.Direction.Left : DetectionManagerForward.Direction.Right);
        }
        else
        {
            dmf.setDetectorCollider(tDetForward,
                vCon.SpeedKPH <= speedCountAsSlow ? DetectionManagerForward.Speed.Slow : DetectionManagerForward.Speed.Normal,
                DetectionManagerForward.Direction.Straight);
        }


     }

    protected void setAccelBrake()
    {
        vCon.input.Handbrake = 0;
        vCon.input.Vertical = 0;


        if (hadCollision)
        {
            //Debug.Log("1 col");
            doSpeed(0);
            return;
        }



        if (hasRealObstacle(obstaclesForward))
        {
            //Debug.Log("2 obstFor");
            VehicleController slowVehicle = getVehicleObstacleSlowestIfNonOtherObstacle(obstaclesForward);
            
            //Ein Fahrzeug ist das Hindernis
            if (slowVehicle != null)
            {
                //75% Geschwindigkeit des langsamen Fahrzeugs annehmen
                //Aber nicht rueckwerts fahren
                doSpeed(Mathf.Max(0,slowVehicle.SpeedKPH*0.75f));
            }
            else if (vCon.SpeedKPH >= 5)
            {
                //Debug.Log("3 kmh >= 5");
                doSpeed(0);
            }
            else if (hasRealObstacle(obstaclesBackward))
            {
                //Debug.Log("4 obstBack");

                //Wenn hinten Hindernis -> stehen bleiben
                doSpeed(0);
            }
            else
            {
                //Wenn kein Hindernis hinter dem Fahrzeug -> zurueckfahren
                //doSpeed(-1);
                doSpeed(0);
            }

            return;
        }



        if (trafficSignIsOk() || trafficSignsStop.Count == 0) {

            float targetSpeed;
            //An TrafficLight/TrafficSign(zum stoppen) wird langsam heran gefahren, bis extra detector an trigger von TrafficLight/Sign kommt
            if (!trafficSignIsOk())
            {
                targetSpeed = speedCountAsSlow;
            }
            else
            {
                //Die Beschilderung/Ampeln geben an, dass gefahren werden darf

                targetSpeed = getSpeed(activeWaypoint.getRecommendedSpeedKMH());
                //Geschwindigkeit der naechsten zwei Waypoints nehmen und verrechnen (sollte zweiter sehr weit weg sein, Wert des 2. verwerfen)
                //Und nur dann, wenn der activeWaypoint eine hoehere Geschwindigkeit hat (damit ist die Geschwindigkeit nie zu hoch, sondern kann nur sinken)
                if (getSpeed(activeWaypoint.getRecommendedSpeedKMH()) > getSpeed(nextWaypoint.getRecommendedSpeedKMH()) && Vector3.Distance(nextWaypoint.transform.position, activeWaypoint.transform.position) <= 10)
                {
                    targetSpeed += getSpeed(nextWaypoint.getRecommendedSpeedKMH());
                    targetSpeed /= 2;
                }
            }

            doSpeed(targetSpeed);


        }
        else
        {
            //Ansonsten stehen bleiben
            doSpeed(0);

        }

    }

    /*
    private void doSpeed(float speedKMH)
    {

        //Debug.Log("doSpeed: "+speedKMH);

        if(speedKMH == 0)
        {
            if(vCon.SpeedKPH >= 5)
            {
                vCon.input.Vertical = -1f;
            }
            else
            {
                vCon.input.Vertical = 0f;
                vCon.input.Handbrake = 1f;
            }
        }else if (speedKMH > 0)
        {
            if(vCon.SpeedKPH > targetSpeed)
            {
                
                if(vCon.SpeedKPH > targetSpeed-1f && vCon.SpeedKPH < targetSpeed + 1f)
                {
                    vCon.input.Vertical = 0f;
                }
                else
                {

                    //vCon.input.Vertical = -0.75f;

                    //Abstufungen: gucken wie viel schneller das Fahrzeug ist und entsprechend stark bremsen
                    float percentToFast = vCon.SpeedKPH/targetSpeed-1;
                    vCon.input.Vertical = Mathf.Max(-1, -percentToFast);
                }

            }
            else if (vCon.SpeedKPH < wheelSlipAboveSpeed || noWheelSlip())
            {
                //nur beschleunigen, wenn Fahrzeug nicht am rutschen ist
                //Bei niedrigen Geschwindigkeiten einfach Gas geben (beim Anfahren sind Reifen immer am "durchdrehen")
                if (vCon.SpeedKPH > targetSpeed - 1f && vCon.SpeedKPH < targetSpeed + 1f)
                {
                    vCon.input.Vertical = 0f;
                }
                else
                {
                    //Abstufungen: gucken wie viel langsamer das Fahrzeug ist und entsprechend stark beschleunigen
                    float percentToSlow = targetSpeed / vCon.SpeedKPH - 1;
                    //25% werden immer mindestens Gas gegeben, da sonst die targetSpeed nur sehr langsam erreicht wird
                    vCon.input.Vertical = Mathf.Min(percentToSlow+0.25f, 1);
                }
                
            }else
            {
   
                if (!noWheelSlipSideway()) { 
                    //ist am rutschen(Seitwaerts) -> leicht bremsen
                    vCon.input.Vertical = -0.15f;
                }
                else
                {
                    //ist hat durchdrehende/blockierende Reifen -> weder Gas noch Bremse
                    vCon.input.Vertical = 0f;
                }
                
            }

        }else
        {
            //speed < 0
            if (vCon.SpeedKPH <= speedKMH*-1)
            {

                vCon.input.Vertical = -0.25f;
                vCon.input.Handbrake = 0f;
            }
            else
            {
                //TODO: vCon.SpeedKPH auch bei rueckwaerts positiv ->
                //hier kann es sein, dass das Fahrzeug mehr als speedKMH*-1 vorwaerts faehrt(!) oder eben zu schnell rueckwaerts

                vCon.input.Vertical = 0f;
                vCon.input.Handbrake = 1f;
            }


        }


    }
    */
    private void doSpeed(float speedKMH)
    {
        carTargetSpeed = speedKMH;

        //Debug.Log("doSpeed: "+speedKMH);

        if (speedKMH == 0)
        {
            if (vCon.SpeedKPH >= 5)
            {
                vCon.input.Vertical = -1f;
            }
            else
            {
                vCon.input.Vertical = 0f;
                vCon.input.Handbrake = 1f;
            }
        }
        else if (speedKMH > 0)
        {
            if (vCon.SpeedKPH > speedKMH)
            {

                if (vCon.SpeedKPH > speedKMH - 1f && vCon.SpeedKPH < speedKMH + 1f)
                {
                    vCon.input.Vertical = 0f;
                }
                else
                {

                    //vCon.input.Vertical = -0.75f;

                    //Abstufungen: gucken wie viel schneller das Fahrzeug ist und entsprechend stark bremsen
                    float percentToFast = vCon.SpeedKPH / speedKMH - 1;
                    vCon.input.Vertical = Mathf.Max(-1, -percentToFast);
                }

            }
            else if (vCon.SpeedKPH < wheelSlipAboveSpeed || noWheelSlip())
            {
                //nur beschleunigen, wenn Fahrzeug nicht am rutschen ist
                //Bei niedrigen Geschwindigkeiten einfach Gas geben (beim Anfahren sind Reifen immer am "durchdrehen")
                if (vCon.SpeedKPH > speedKMH - 1f && vCon.SpeedKPH < speedKMH + 1f)
                {
                    vCon.input.Vertical = 0f;
                }
                else
                {
                    //Abstufungen: gucken wie viel langsamer das Fahrzeug ist und entsprechend stark beschleunigen
                    float percentToSlow = speedKMH / vCon.SpeedKPH - 1;
                    //25% werden immer mindestens Gas gegeben, da sonst die speedKMH nur sehr langsam erreicht wird
                    vCon.input.Vertical = Mathf.Min(percentToSlow + 0.25f, 1);
                }

            }
            else
            {

                if (!noWheelSlipSideway())
                {
                    //ist am rutschen(Seitwaerts) -> leicht bremsen
                    vCon.input.Vertical = -0.15f;
                }
                else
                {
                    //ist hat durchdrehende/blockierende Reifen -> weder Gas noch Bremse
                    vCon.input.Vertical = 0f;
                }

            }

        }
        else
        {
            //speed < 0

            
            if (vCon.SpeedKPH <= speedKMH * -1)
            {

                vCon.input.Vertical = -0.25f;
                vCon.input.Handbrake = 0f;
            }
            else
            {
                //TODO: vCon.SpeedKPH auch bei rueckwaerts positiv ->
                //hier kann es sein, dass das Fahrzeug mehr als speedKMH*-1 vorwaerts faehrt(!) oder eben zu schnell rueckwaerts

                vCon.input.Vertical = 0f;
                vCon.input.Handbrake = 1f;
            }

        }


    }

    private bool trafficSignIsOk()
    {
        TrafficSign ts;
        foreach (Collider c in trafficSigns)
        {
            ts = c.GetComponent<TrafficSign>();
            //Wenn eine Beschilderung/Ampel angibt, es ist nicht ok zu fahren, dann return false
            if ( waypointIsAffected(ts) && !ts.getOkToDrive(vCon))
            {
                return false;
            }
        }

        return true;
    }

    //Ob der aktive/naechste Waypoint von dem TrafficSign beeinflusst wird
    private bool waypointIsAffected(TrafficSign ts)
    {
        foreach (Waypoint wp in ts.affectedWaypoints) {
            if(wp.Equals(activeWaypoint) || wp.Equals(nextWaypoint))
            {
                return true;
            }
        }

        return false;
    }

    private bool noWheelSlip()
    {
        float slip = 0;
        int i = 0;

        foreach(Axle axle in vCon.axles)
        {
            slip += axle.leftWheel.ForwardSlip + axle.leftWheel.SideSlip;
            slip += axle.rightWheel.ForwardSlip + axle.rightWheel.SideSlip;

            i++;
        }

        slip /= i;

        bool res = slip <= wheelSlipThreshold;

        //Debug.Log("noWheelSlip: "+slip +" -> "+res);

        return res;
    }

    private bool noWheelSlipSideway()
    {
        float slip = 0;
        int i = 0;

        foreach (Axle axle in vCon.axles)
        {
            slip += axle.leftWheel.SideSlip;
            slip += axle.rightWheel.SideSlip;

            i++;
        }

        slip /= i;

        bool res = slip <= wheelSlipThreshold/10;

        //Debug.Log("noWheelSlip: "+slip +" -> "+res);

        return res;
    }

    private float getSpeed(float recommendedSpeedKMH)
    {
        if (vCon.trailer.attached || drivesSlow)
        {
            //Mit Anhaenger dran wird langsamer gefahren

            return recommendedSpeedKMH * (2f / 3f);
        }

        return recommendedSpeedKMH;
    }

    //Prueft ob in Liste echte Hindernisse sind (zb. Strassen/Waypoints werden erfasst, aber stellen kein Hinterniss dar)
    bool hasRealObstacle(List<Collider> colls)
    {
        foreach (Collider c in colls)
        {
            if(isRealObstacle(c))
                return true;
        }

        return false;
    }

    //Prueft Collider echtes Hindernis ist (zb. Strassen/Waypoints werden erfasst, aber stellen kein Hinterniss dar)
    bool isRealObstacle(Collider col)
    {
        if (col.isTrigger)
            return false;

        if ("Road".Equals(col.tag))
            return false;

        if ("Terrain".Equals(col.tag))
            return false;

        if ("TrafficSign".Equals(col.tag))
            return false;
        
        return true;
    }

    List<VehicleController> getVehicleObstaclesIfNonOtherObstacle(List<Collider> colls)
    {
        List<VehicleController> vehicles = new List<VehicleController>();

        foreach (Collider c in colls)
        {
            //Nur wenn ein echtes Hinderniss gefunden
            if (isRealObstacle(c)) { 
                //Wenn Hindernis ein Fahrzeug -> Liste hinzufuegen
                if (c.attachedRigidbody != null && c.attachedRigidbody.tag.Equals("Vehicle"))
                {
                    vehicles.Add(c.attachedRigidbody.GetComponent<VehicleController>());
                }
                else
                {
                    //Wenn ein anderes Hindernis gefunden, leere Liste zurueckgeben
                    vehicles.Clear();
                    return vehicles;
                }
            }
        }

        return vehicles;
    }

    VehicleController getVehicleObstacleSlowestIfNonOtherObstacle(List<Collider> colls)
    {
        List<VehicleController> vehicles = getVehicleObstaclesIfNonOtherObstacle(colls);
        //Entweder gar kein Hindernis gefunden oder ein anderes Hinderniss, als ein Fahrzeug ist in der Liste vorhanden
        //In beiden Faellen return null;
        if (vehicles.Count == 0) {
            return null;
        }

        VehicleController slowest = null;

        foreach (VehicleController vCon in vehicles)
        {
            if (slowest == null || vCon.Speed < slowest.Speed)
            {
                slowest = vCon;
            }


        }

        return slowest;
    }

    //Prueft ob Collider Beschilderung/Ampel ist
    bool isTrafficSign(Collider col)
    {
        if ("TrafficSign".Equals(col.tag))
            return true;

        return false;
    }
    

    protected void setWaypoint()
    {
        if(activeWaypoint == null)
        {
            oldWaypoint = null;
            //activeWaypoint = wpm.getNearestWaypoint(carGO.transform.position).GetComponent<Waypoint>();
            activeWaypoint = wpm.getNearestWaypoint(getPositionGO().transform.position).GetComponent<Waypoint>();
            nextWaypoint = getNextWaypoint();
            return;
        }

        //if(Vector3.Distance(carGO.transform.position, activeWaypoint.transform.position) <= waypointNearEnough)
        if (Vector3.Distance(getPositionGO().transform.position, activeWaypoint.transform.position) <= waypointNearEnough)
        {
            oldWaypoint = activeWaypoint;
            activeWaypoint = nextWaypoint;
            nextWaypoint = getNextWaypoint();
        }
    }

    protected Waypoint getNextWaypoint()
    {
        Waypoint tmpWP = activeWaypoint.getRandomNextWaypoint();



        //Wenn der zufaellige naechste Waypoint so nah am aktiven Waypoint liegt, dass er automatisch als naechster Waypoint erkannt wird -> ueberspringen, da es sich um die Konnektoren handelt
        if (Vector3.Distance(tmpWP.transform.position, activeWaypoint.transform.position) <= activeWaypoint.getCountAsNextPreviousWaypointDistance())
        {
            tmpWP = tmpWP.getRandomNextWaypoint();
        }

        return tmpWP;
    }

    void AICarCollition(Collision collision)
    {
        
        // Debug-draw all contact points and normals
        foreach (ContactPoint contact in collision.contacts)
        {
            Debug.DrawRay(contact.point, contact.normal, Color.white);
        }


        //nur bei groesseren Kollisionen stehenbleiben (und wenn Objekt keine Strasse ist (Busse zb. koennen bei Steilen auf/abfahrten mit Strasse kollidieren))
        Debug.Log(carGO.name + " had collision with " + collision.gameObject.name+"\trelMagnitute: "+ collision.relativeVelocity.magnitude);
        if (collision.relativeVelocity.magnitude > collisionThreshold && !"Road".Equals(collision.gameObject.tag)) {
            hadCollision = true;
            //Debug.Log("collision strong enough");

            //Kollisionsstatus nach einiger Zeit wieder entfernen
            StartCoroutine(ResetHadCollision(10f));
        }

    }

    IEnumerator ResetHadCollision(float time)
    {
        yield return new WaitForSeconds(time);

        hadCollision = false;
    }

    void obstacleForwardEnter(Collider other)
    {
        if (isRealObstacle(other)){
            //Debug.Log("obstacleForwardEnter: " + other.name);
            obstaclesForward.Add(other);
        }

        
    }

    void obstacleForwardExit(Collider other)
    {
        if (obstaclesForward.Contains(other)) {
            //Debug.Log("obstacleForwardExit: " + other.name);
            obstaclesForward.Remove(other);
        }
    }

    void obstacleBackwardEnter(Collider other)
    {
        if (isRealObstacle(other))
        {
            //Debug.Log("obstacleBackwardEnter: " + other.name);
            obstaclesBackward.Add(other);
        }


    }

    void obstacleBackwardExit(Collider other)
    {
        if (obstaclesForward.Contains(other))
        {
            //Debug.Log("obstacleBackwardExit: " + other.name);
            obstaclesBackward.Remove(other);
        }
    }

    void trafficSignEnter(Collider other)
    {
        if (isTrafficSign(other))
        {
            trafficSigns.Add(other);
        }


    }

    void trafficSignExit(Collider other)
    {
        if (trafficSigns.Contains(other))
        {
            trafficSigns.Remove(other);
        }
    }

    void trafficSignStopEnter(Collider other)
    {
        if (isTrafficSign(other))
        {
            trafficSignsStop.Add(other);
        }


    }

    void trafficSignStopExit(Collider other)
    {
        if (trafficSignsStop.Contains(other))
        {
            trafficSignsStop.Remove(other);
        }
    }

    private void connectTrailer()
    {
        if(!vCon.trailer.attached)
            vCon.input.trailerAttachDetach = true;

        //TODO: wenn Anhaenger angekuppelt, sollte der backwardDetector ausgeschaltet werden
    }




    float AngleTest(Transform carGO, Vector3 target)
    {
        //float dir = Vector3.Dot(fwd, targetDir);

        float dir = (Vector3.Dot(carGO.position, target)) / (carGO.position.magnitude * target.magnitude);
        dir = Mathf.Acos(dir) * 360 * AngleDir(carGO.forward, target - getPositionGO().transform.position, carGO.up);
        
        //Debug.Log("dir: " + dir);
        return dir;
    }

    float Angle(Vector3 fwd, Vector3 targetDir, Vector3 up)
    {
        Vector3 perp = Vector3.Cross(fwd, targetDir);
        float dir = Vector3.Dot(perp, up);

        //Debug.Log("dir: " + dir);
        return dir;
    }

    float AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up)
    {
        float dir = Angle(fwd, targetDir, up);

        if (dir > 0f)
        {
            return 1f;
        }
        else if (dir < 0f)
        {
            return -1f;
        }
        else
        {
            return 0f;
        }
    }

    


#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        Handles.Label(transform.position + new Vector3(0, 2f, 0), "carTargetSpeed: " + carTargetSpeed + "(curr: "+(vCon != null ? (int)vCon.SpeedKPH : 0) +")");
    }
#endif
}
