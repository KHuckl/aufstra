﻿using UnityEngine;

public class ResetSeatedPosition : MonoBehaviour
{
    [Tooltip("Desired head position of player when seated")]
    public Transform desiredHeadPosition;
    public Transform steamCamera;
    public Transform cameraRig;

    private bool resetNextFrame = false;

    private void Start()
    {
        resetNextFrame = true;
    }

    private void OnEnable()
    {
        resetNextFrame = true;
    }

    // Update is called once per frame
    void Update()
    {

        //if (Input.GetKeyDown(KeyCode.Space))
        if(resetNextFrame)
        {
            resetNextFrame = false;

            if (desiredHeadPosition != null)
            {
                ResetSeatedPos(desiredHeadPosition);
            }
            else
            {
                Debug.Log("Target Transform required. Assign in inspector.");
            }

        }
    }

    private void ResetSeatedPos(Transform desiredHeadPos)
    {
        if ((steamCamera != null) && (cameraRig != null))
        {
            //Temporaer merken, da unterer Code nur funktioniert, wenn diese Transform eine genullte rotation hat
            Quaternion tmpRot = transform.rotation;
            transform.rotation = Quaternion.Euler(0,0,0);


            //ROTATION
            // Get current head heading in scene (y-only, to avoid tilting the floor)
            float offsetAngle = steamCamera.rotation.eulerAngles.y;
            // Now rotate CameraRig in opposite direction to compensate
            cameraRig.Rotate(0f, -offsetAngle, 0f);

            //POSITION
            // Calculate postional offset between CameraRig and Camera
            Vector3 offsetPos = steamCamera.position - cameraRig.position;
            // Reposition CameraRig to desired position minus offset
            cameraRig.position = (desiredHeadPos.position - offsetPos);

            //zuruck auf eigentliche rotation
            transform.rotation = tmpRot;


            Debug.Log("Seat recentered!");
        }
        else
        {
            Debug.Log("Error: SteamVR objects not found!");
        }
    }
}