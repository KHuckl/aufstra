﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ScenarioMenu : MonoBehaviour {

    public GameObject pauseMenu;

    bool isPaused = false;
    float tsBeforePause = -1f;

    private void Start()
    {
        hidePauseMenu();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            switchPause();
        }
    }

    private void switchPause()
    {
        if (isPaused)
        {
            hidePauseMenu();
            Time.timeScale = tsBeforePause;
        }
        else
        {
            showPauseMenu();
            tsBeforePause = Time.timeScale;
            Time.timeScale = 0f;
        }

        isPaused = !isPaused;
    }

    private void showPauseMenu()
    {
        pauseMenu.SetActive(true);
    }

    private void hidePauseMenu()
    {
        pauseMenu.SetActive(false);
    }

    private void LoadScene(string sceneName)
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(sceneName);
    }

    public void ReturnToMainMenu()
    {
        Debug.Log("ReturnToMainMenu");

        LoadScene("MainMenu");
    }

    public void ReloadScenario()
    {
        Debug.Log("ReloadScenario");

        LoadScene(SceneManager.GetActiveScene().name);
    }

}
