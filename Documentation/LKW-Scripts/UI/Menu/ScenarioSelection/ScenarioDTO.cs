﻿using UnityEngine;

[System.Serializable]
public class ScenarioDTO {

    public string sceneFileName;
    public string sceneViewName;
    public Texture sceneImage;
    public string sceneDescription;

}
