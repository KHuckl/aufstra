﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScenarioListEntry : MonoBehaviour {

    public Text scenarioName;
    public RawImage scenarioImage;
    public Text scenarioDescription;

    public void Initialize(string scenarioName, Texture scenarioImage, string scenarioDescription)
    {
        this.scenarioName.text = scenarioName;
        this.scenarioImage.texture = scenarioImage;
        this.scenarioDescription.text = scenarioDescription;
    }

}
