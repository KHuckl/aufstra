﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BtnSwitchMenu : MonoBehaviour {

    public MenuManager.Menu ToMenu;
    private MenuManager mMan;

    private void Start()
    {
        mMan = GameObject.FindGameObjectWithTag("Menus").GetComponent<MenuManager>();
    }

    public void doSwitchMenu()
    {
        mMan.setActiveMenu(ToMenu);
    }

}
