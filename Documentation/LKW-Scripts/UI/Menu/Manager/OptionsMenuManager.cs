﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class OptionsMenuManager : MonoBehaviour
{
    public Dropdown ui_inputType;
    public Slider ui_steeringWheelSensitivity;
    public Toggle ui_moveInSteps;
    public Slider ui_soundVol;
    public Toggle ui_VRMode;

    void Start()
    {
        ui_inputType.AddOptions(new List<string> {"Debug Keyboard", "Steering Wheel" });

        int it;
        switch (SelectedOptions.getInstance().inputType)
        {
            case InputHandler.InputType.Keyboard:
                it = 0;
                break;
            case InputHandler.InputType.SteeringWheel:
                it = 1;
                break;
            default:
                throw new NotSupportedException("InputType not supported: "+ SelectedOptions.getInstance().inputType);
                break;
        }

        //UI mit gespeicherten Werten befuellen
        setInputType(it, true);
        setSteeringWheelSensitivity(SelectedOptions.getInstance().steeringWheelSensitivity, true);
        setMoveInSteps(SelectedOptions.getInstance().moveInSteps, true);
        setSoundVol(SelectedOptions.getInstance().soundVol, true);
        setVRMode(SelectedOptions.getInstance().vrMode, true);
    }

    //updateUI setzt nur die Werte in der UI
    //!updateUI speichert die Werte
    private void setInputType(int v, bool updateUI)
    {
        if(!updateUI)
            SelectedOptions.getInstance().setInputType(v);

        if (updateUI)
            ui_inputType.value = v;

    }

    //updateUI setzt nur die Werte in der UI
    //!updateUI speichert die Werte
    private void setSteeringWheelSensitivity(float v, bool updateUI)
    {
        if (!updateUI)
            SelectedOptions.getInstance().setSteeringWheelSensitivity(v);

        if (updateUI) {
            ui_steeringWheelSensitivity.value = v;
        }
        //Immer
        ui_steeringWheelSensitivity.transform.parent.Find("Text").GetComponent<Text>().text = v.ToString();

    }

    //updateUI setzt nur die Werte in der UI
    //!updateUI speichert die Werte
    private void setMoveInSteps(bool v, bool updateUI)
    {
        if (!updateUI)
            SelectedOptions.getInstance().setMoveInSteps(v);

        if (updateUI)
            ui_moveInSteps.isOn = v;
    }

    //updateUI setzt nur die Werte in der UI
    //!updateUI speichert die Werte
    private void setSoundVol(float v, bool updateUI)
    {
        if (!updateUI)
            SelectedOptions.getInstance().setSoundVolume(v);

        if (updateUI)
        {
            ui_soundVol.value = v;
        }
        //Immer
        ui_soundVol.transform.parent.Find("Text").GetComponent<Text>().text = v.ToString();

    }

    //updateUI setzt nur die Werte in der UI
    //!updateUI speichert die Werte
    private void setVRMode(bool v, bool updateUI)
    {
        if (!updateUI)
            SelectedOptions.getInstance().setVRMode(v);

        if (updateUI)
            ui_VRMode.isOn = v;
    }

    public void InputTypeChanged()
    {
        setInputType(ui_inputType.value, false);
    }

    public void SteeringWheelSensitivityChanged()
    {
        setSteeringWheelSensitivity(ui_steeringWheelSensitivity.value, false);
    }

    public void MoveInStepsChanged()
    {
        setMoveInSteps(ui_moveInSteps.isOn, false);
    }

    public void SoundVolumeChanged()
    {
        setSoundVol(ui_soundVol.value, false);
    }

    public void VRModeChanged()
    {
        setVRMode(ui_VRMode.isOn, false);
    }
}
