﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScenarioManager : MonoBehaviour {

    //Liste von verfuegbaren Szenarien
    public List<ScenarioDTO> availableScenarios;
    //Vorlage des Listeneintrags
    public GameObject listEntryPrefab;
    //"Content"
    public GameObject listEntryParent;
    
    //Alle erstellten Listeneintraege
    protected List<ScenarioListEntry> scenarioListEntries = new List<ScenarioListEntry>();
    protected int selectedEntry = 0;

    //Hier muss immer der Height Wert des listEntryParent eingetragen werden
    public float entryHeight;
    public float spaceBetweenEntries = 15f;

    protected bool resetScrollBar;

    // Use this for initialization
    void Start () {
        generateListEntries();
	}

    private void generateListEntries()
    {
        int i=0;
        ScenarioListEntry tmpSLE;
        foreach (ScenarioDTO scenario in availableScenarios)
        {
            //Instanz des Listeneintrags erstellen
            tmpSLE = Instantiate(listEntryPrefab).GetComponent<ScenarioListEntry>();
            //Werte setzen
            tmpSLE.Initialize(scenario.sceneViewName, scenario.sceneImage, scenario.sceneDescription);
            //"Content" hinzufuegen und Position setzen
            tmpSLE.transform.SetParent(listEntryParent.transform);
            tmpSLE.transform.localScale = new Vector3(1,1,1);
            ((RectTransform)tmpSLE.transform).anchoredPosition = new Vector3(0, (entryHeight * i + spaceBetweenEntries * (i + 1)) * -1 -entryHeight/2, 0);
            //"Content" Groesse anpassen
            ((RectTransform)tmpSLE.transform.parent).sizeDelta = new Vector2(0, entryHeight * (i+1) + spaceBetweenEntries * (i+2));
            
            //Variable Capturing -> wenn i direkt uebergeben wuerde, waeren fuer alle Buttons der Parameter die der max. i Wert
            int param = i;
            //Klick auf Element registrieren
            tmpSLE.GetComponent<Button>().onClick.AddListener(() => OnButtonClicked(param));

            scenarioListEntries.Add(tmpSLE);

            i++;
        }

        //Scrollbalken wuerde in den naechsten Frames nach unten rutschen, daher im naechsten Frame zuruecksetzten
        resetScrollBar=true;
    }

    void Update()
    {
        if (resetScrollBar)
        {
            //Scrollbalken nach ganz oben schieben
            listEntryParent.transform.parent.parent.GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
            resetScrollBar = false;
        }
    }

    public void OnButtonClicked(int i)
    {
        selectedEntry = i;
    }

    public void StartScenario()
    {
        SceneManager.LoadScene(availableScenarios[selectedEntry].sceneFileName);
    }
}
