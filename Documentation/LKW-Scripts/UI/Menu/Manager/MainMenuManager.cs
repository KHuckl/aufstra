﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    private void Awake()
    {
        //einmal einfach aufrufen, damit Settings geladen werden (insbesondere wichtig bei Start der Anwendung wegen VR)
        SelectedOptions.getInstance();
    }

    public void Exit()
    {
        Application.Quit();
    }
}
