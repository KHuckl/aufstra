﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour {

    public enum Menu
    {
        MainMenu,
        ScenarioSelection,
        Options
    }

    public List<MenuGOMenuName> menus;
    private Menu activeMenu;

	// Use this for initialization
	void Start () {
        setActiveMenu(Menu.MainMenu);
	}
	
	public void setActiveMenu(Menu newActive)
    {
        foreach(MenuGOMenuName m in menus)
        {
            if (newActive.Equals(m.menu))
            {
                m.menuGameObject.SetActive(true);
            }
            else
            {
                m.menuGameObject.SetActive(false);
            }
        }
    }
}
