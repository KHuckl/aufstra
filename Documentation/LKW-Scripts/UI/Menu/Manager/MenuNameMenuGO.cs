﻿using UnityEngine;

[System.Serializable]
public class MenuGOMenuName {
    public MenuManager.Menu menu;
    public GameObject menuGameObject;
}
