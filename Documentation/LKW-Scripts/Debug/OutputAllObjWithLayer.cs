﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutputAllObjWithLayer : MonoBehaviour
{
    public int layer;

    // Start is called before the first frame update
    void Start()
    {
        List<GameObject> gos = find(layer);

        foreach(GameObject go in gos)
        {
            //Debug.Log("GO with layer "+layer+": "+go.name);
        }

    }

    List<GameObject> find(int layer)
    {
        GameObject[] goArray = FindObjectsOfType<GameObject>();
        List<GameObject> goList = new List<GameObject>();
        for (int i = 0; i < goArray.Length; i++)
        {
            if (goArray[i].layer == layer)
            {
                Debug.Log(goArray[i]+" "+ goArray[i].layer);
                goList.Add(goArray[i]);
            }
        }
        
        return goList;
    }

}
