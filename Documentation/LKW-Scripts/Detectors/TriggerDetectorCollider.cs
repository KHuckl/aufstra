﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDetectorCollider : MonoBehaviour
{
    List<Collider> colliderInside = new List<Collider>();

    public delegate void OnTriggerEnterDel(Collider other);
    public event OnTriggerEnterDel EventOnTriggerEnter;

    public delegate void OnTriggerExitDel(Collider other);
    public event OnTriggerExitDel EventOnTriggerExit;

    public delegate void OnTriggerStayDel(Collider other);
    public event OnTriggerStayDel EventOnTriggerStay;

    protected void ThrowOnTriggerEnter(Collider other)
    {
        if (EventOnTriggerEnter != null)
            EventOnTriggerEnter(other);
    }

    protected void ThrowOnTriggerExit(Collider other)
    {
        if (EventOnTriggerExit != null)
            EventOnTriggerExit(other);
    }

    protected void ThrowOnTriggerStay(Collider other)
    {
        if (EventOnTriggerStay != null)
            EventOnTriggerStay(other);
    }

    private void OnTriggerEnter(Collider other)
    {
        colliderInside.Add(other);
        ThrowOnTriggerEnter(other);
    }

    private void OnTriggerStay(Collider other)
    {
        ThrowOnTriggerStay(other);
    }

    private void OnTriggerExit(Collider other)
    {
        colliderInside.Remove(other);
        ThrowOnTriggerExit(other);
    }

    public bool isColliderInside(Collider other)
    {
        return colliderInside.Contains(other);
    }

    void OnDisable()
    {
        for(int i = colliderInside.Count-1; i>=0; i--)
        {
            OnTriggerExit(colliderInside[i]);
        }
    }
}
