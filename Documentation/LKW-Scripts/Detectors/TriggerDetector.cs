﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Events werden so geworfen, als waeren die Child-Collider ein grosser.
//Dh. Enter/Exit einmal geworfen (bzw. nicht, wenn Collider noch in anderem Child-Collider)
//Nur Stay wird fuer jeden Child-Collider einmal geworfen
public class TriggerDetector : MonoBehaviour
{
    List<TriggerDetectorCollider> childTDCollider = new List<TriggerDetectorCollider>();

    //Alle Collider, die im Moment mit irgendeinem Child-Collider kollidieren
    List<Collider> colliderInside = new List<Collider>();
    
    public delegate void OnTriggerEnterDel(Collider other);
    public event OnTriggerEnterDel EventOnTriggerEnter;

    public delegate void OnTriggerExitDel(Collider other);
    public event OnTriggerExitDel EventOnTriggerExit;

    public delegate void OnTriggerStayDel(Collider other);
    public event OnTriggerStayDel EventOnTriggerStay;

    private void Awake()
    {
        childTDCollider.AddRange(transform.GetComponentsInChildren<TriggerDetectorCollider>());

        foreach(TriggerDetectorCollider td in childTDCollider)
        {
            td.EventOnTriggerEnter += OnTriggerEnter;
            td.EventOnTriggerStay += OnTriggerStay;
            td.EventOnTriggerExit += OnTriggerExit;
        }
    }

    protected void ThrowOnTriggerEnter(Collider other)
    {
        if (EventOnTriggerEnter != null)
            EventOnTriggerEnter(other);
    }

    protected void ThrowOnTriggerExit(Collider other)
    {
        if (EventOnTriggerExit != null)
            EventOnTriggerExit(other);
    }

    protected void ThrowOnTriggerStay(Collider other)
    {
        if (EventOnTriggerStay != null)
            EventOnTriggerStay(other);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (!colliderInside.Contains(other))
        {
            colliderInside.Add(other);
            ThrowOnTriggerEnter(other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (colliderInside.Contains(other) && isNotInOtherChildCollider(other))
        {
            colliderInside.Remove(other);
            ThrowOnTriggerExit(other);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        ThrowOnTriggerStay(other);
    }

    private bool isNotInOtherChildCollider(Collider other)
    {
        foreach(TriggerDetectorCollider td in childTDCollider)
        {
            if (td.gameObject.activeSelf && td.isColliderInside(other))
            {
                return false;
            }
        }

        return true;
    }

    public void SetSubColliderActive(TriggerDetectorCollider subCollider, bool active)
    {
        if (!childTDCollider.Contains(subCollider))
            return;

        //evtl. vorhandene Objekte im Subdetector werden mit Aufruf dieser Methode aus gesamt-liste entfernt
        //subCollider.DeactivateCollider();
        subCollider.gameObject.SetActive(active);
    }
}
