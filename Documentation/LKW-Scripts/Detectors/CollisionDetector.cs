﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetector : MonoBehaviour
{
    
    public delegate void OnCollisionEnterDel(Collision collision);
    public event OnCollisionEnterDel EventOnCollisionEnter;

    private void OnCollisionEnter(Collision collision)
    {
        ThrowOnCollisionEnter(collision);
    }

    protected void ThrowOnCollisionEnter(Collision collision)
    {
        if(EventOnCollisionEnter != null)
            EventOnCollisionEnter(collision);

    }
}
