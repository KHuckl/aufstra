﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TmpCollisionDetection : MonoBehaviour
{
    public List<Collider> insideColls = new List<Collider>();

    void OnCollisionEnter(Collision collision)
    {
        insideColls.Add(collision.collider);
    }

    void OnCollisionExit(Collision collision)
    {
        if(insideColls.Contains(collision.collider))
            insideColls.Remove(collision.collider);
    }

    public List<Collider> getInsideColls()
    {
        return insideColls;
    }
}
