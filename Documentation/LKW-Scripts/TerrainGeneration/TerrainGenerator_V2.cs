﻿//#define RIVERSASCOROUTINE
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;

public class TerrainGenerator_V2 : MonoBehaviour
{
    [Header("Seed")]
    public int seed;

    [Header("Height Settings")]
    //"Zoom" der Karte
    [Range(50f, 1000f)]
    public float scale = 750f;
    [Range(0.0001f, 2)]
    public float damp=1f;
    [Range(1, 10)]
    public int amountDetails = 10;
    [Range(1f, 10f)]
    public float frequency = 2.5f;
    [Range(0f, 1f)]
    public float altitude = 0.25f;
    [Range(0f, 1f)]
    public float raiseTerrainBy = 0f;

    
    [Header("River Settings")]
    public int amountRiversFlow=1;
    public int amountRiversThrough = 1;
    [Range(0.001f, 0.5f)]
    public float riverStartDepth = 0.025f;

    [Range(0.0001f, 0.3f)]
    public float riverSlopeMin = 0.005f;
    [Range(0.0001f, 0.3f)]
    public float riverSlopeMax = 0.015f;
    [Range(0, 100)]
    public int riverDirectionChangeChance = 15;

    [Header("Rough up Settings")]
    [Range(0, 0.05f)]
    public float roughMin = 0.000f;
    [Range(0, 0.05f)]
    public float roughMax = 0.001f;

    [Header("Smooth Settings")]
    [Range(0, 10)]
    public int smoothIterations = 1;

    [Header("Lift/Sink Settings")]
    [Range(-1f, 1f)]
    public float changeHeightBy = 0f;

    [Header("Texture Settings")]
    public List<SplatHeightTextures> heightTextures;
    [Range(0f, 100f)]
    public float countAsCliffHeightThreshold = 1.35f;
    [Range(0f, 100f)]
    public float cliffHeightRandomness=0f;
    public SplatHeightTextures textureCliff;

    [Header("Vegetation Settings")]
    public List<SpawnableObject> vegetationObjects;
    [Range(1, 100000)]
    public int maxVegetationCollisionCheckIterations = 10;
    [Range(1, 100000)]
    public int maxLegalVegetationPositionIterations = 100;
    int startedStartCoroutine;

    protected TerrainData td;
    protected float[,] newHeightData;

    protected System.Random SeedRND;


    //TODO: debug
    public Transform debugPosMarker;
    
    public void SetNewRandomBasedOnSeed()
    {
        SeedRND = new System.Random(seed);
    }

    protected void init()
    {
        td = transform.GetComponent<Terrain>().terrainData;

        SetNewRandomBasedOnSeed();
    }

    protected void initHeight()
    {
        init();
        newHeightData = td.GetHeights(0, 0, td.heightmapWidth, td.heightmapHeight);
    }

    protected void setNewHeightData()
    {
        td.SetHeights(0, 0, newHeightData);
    }



    //------------------------------------Height-------------------------------------
    public void generateHeight()
    {
        initHeight();

        genHeight_perlinNoise(newHeightData, seed, amountDetails, altitude, frequency);

        setNewHeightData();
    }

    //https://www.youtube.com/watch?v=wbpMiKiSKm8
    //https://www.youtube.com/watch?v=WP-Bm65Q-1Y
    //https://www.youtube.com/watch?v=MRNFcywkUSA
    private void genHeight_perlinNoise(float[,] newHeightData, int baseSeed, int octaves, float baseAltitude, float baseFrequency)
    {
        
        Vector2 realOffset = new Vector2();
        //realOffset.x = positionOffset.x + rnd.Next(-1000, 1000);
        //realOffset.y = positionOffset.y + rnd.Next(-1000, 1000);
        realOffset.x = SeedRND.Next(-1000, 1000);
        realOffset.y = SeedRND.Next(-1000, 1000);

        float minHeight = float.MaxValue;



        for (int y = 0; y < newHeightData.GetLength(0); y++)
        //for (int y = 0; y < 10; y++)
        {
            for (int x = 0; x < newHeightData.GetLength(1); x++)
            //for (int x = 0; x < 10; x++)
            {

                float altitude = 1;
                float frequency = 1;
                float height = 0;


                for (int oct = 0; oct < octaves; oct++)
                {
                    float posX = (x - newHeightData.GetLength(1) / 2) / scale * frequency + realOffset.x;
                    float posY = (y - newHeightData.GetLength(0) / 2) / scale * frequency + realOffset.y;

                    height += Mathf.PerlinNoise(posX, posY) * altitude;

                    altitude *= baseAltitude;
                    frequency *= baseFrequency;
                }

                height *= damp;

                if (height < minHeight)
                    minHeight = height;

                newHeightData[x, y] = height;



            }
        }

        //Die Hoehendaten nach (ganz) unten setzen, damit Terrain nicht am obersten Ende der heightMap Werte ist
        //Aber um raiseTerrainBy wieder erhoehen, damit bei manchen Terrains die Flusse noch tief genug erstellt werden koennen.
        //(Problematisch bei TerrainPreset Low -> Terrain ist sehr flach und Platz nach oben wird nicht benoetigt, also etwas anheben)
        for (int y = 0; y < newHeightData.GetLength(0); y++)
        {
            for (int x = 0; x < newHeightData.GetLength(1); x++)
            {
                newHeightData[x, y] = newHeightData[x, y] - minHeight + raiseTerrainBy;
            }
        }
        
    }
    //------------------------------------/ Height-------------------------------------
    //------------------------------------Holes/Lakes-------------------------------------
    private void createHole(int x, int y, float height, float holeSlopeMin, float holeSlopeMax, float maxDepth)
    {
        if (x < 0 || x >= newHeightData.GetLength(1))
        {
            return;
        }
        if (y < 0 || y >= newHeightData.GetLength(0))
        {
            return;
        }
        if (height < maxDepth)
        {
            return;
        }
        if (newHeightData[x, y] <= height)
        {
            return;
        }

        newHeightData[x, y] = height;

        createHole(x - 1, y, height + UnityEngine.Random.Range(holeSlopeMin, holeSlopeMax), holeSlopeMin, holeSlopeMax, maxDepth);
        createHole(x + 1, y, height + UnityEngine.Random.Range(holeSlopeMin, holeSlopeMax), holeSlopeMin, holeSlopeMax, maxDepth);
        createHole(x, y - 1, height + UnityEngine.Random.Range(holeSlopeMin, holeSlopeMax), holeSlopeMin, holeSlopeMax, maxDepth);
        createHole(x, y + 1, height + UnityEngine.Random.Range(holeSlopeMin, holeSlopeMax), holeSlopeMin, holeSlopeMax, maxDepth);
    }

    //------------------------------------/ Holes/Lakes-------------------------------------
    //------------------------------------Rivers-------------------------------------
    public void generateRivers()
    {
#if (RIVERSASCOROUTINE)
        StartCoroutine(generateRiversIE_V2());
#else
        generateRiversIE_V2();
#endif

    }

    public void generateRiversWholeTerrain()
    {
#if (RIVERSASCOROUTINE)
        StartCoroutine(generateRiversIE_V3());
#else
        generateRiversIE_V3();
#endif

    }

    /// <summary>
    /// Generiert einen Fluss, der versucht immer in die Richtung des Gefaelles zu fliessen.
    /// Sollte kein echtes Gefaelle vorhanden sein, wird der niedrigste Nachbar genommen. Dessen (neue) Hoehe wird die letzte Flusstiefe nicht ueberschreiten.
    /// Dies passiert solange, bis der Fluss das Ende des Terrain erreicht oder bis er sich in einer Sackgasse(bereits durch alle Nachbarn geflossen) befindet.
    /// Dadurch koennen auch Seen entstehen.
    /// </summary>
    /// <returns></returns>
#if (RIVERSASCOROUTINE)
    protected IEnumerator generateRiversIE_V2()
#else
    protected void generateRiversIE_V2()
#endif
    {
        initHeight();


        float[,] newHeightDataCopy = (float[,])newHeightData.Clone();

        //Gilt fuer alle Fluesse (damit zu generierende Fluesse in bereits generierte fliessen koennen)
        Hashtable visitedXY = new Hashtable();

        for (int i = 0; i < amountRiversFlow; i++)
        {
            int x = UnityEngine.Random.Range(0, newHeightData.GetLength(1));
            int y = UnityEngine.Random.Range(0, newHeightData.GetLength(0));
            

            int[] directionRes = null;
            float riverHeight = newHeightDataCopy[x, y] - riverStartDepth;
            float riverHeight_old = riverHeight;


            while (y > 0 && y < newHeightData.GetLength(0) - 1 && x > 0 && x < newHeightData.GetLength(1) - 1)
            {

                if (newHeightDataCopy[x, y] > riverHeight)
                {
                    //terrain ist hoeher als fluss
                    riverHeight = newHeightDataCopy[x, y] - riverStartDepth;
                    //fluss darf nicht aufwaerts fliessen
                    if (riverHeight > riverHeight_old)
                    {
                        riverHeight = riverHeight_old;
                    }

                }

                if (riverHeight < 0)
                {
                    riverHeight = 0;
                }

                createHole(x, y, riverHeight, riverSlopeMin, riverSlopeMax, 0f);
                riverHeight_old = riverHeight;
                visitedXY.Add("x" + x + "y" + y, new int[] { x, y });

                int[] directionRes_old = new int[] { 1, 0 };
                if (directionRes != null)
                {
                    directionRes_old[0] = directionRes[0];
                    directionRes_old[1] = directionRes[1];
                }
                
                List<int[]> lowestNeightboursSorted = getNeighboursSorted(x, y, newHeightDataCopy);
                bool noNeightboursLeft = false;
                do
                {
                    //alle Nachbarn wurden geprueft und alle wurden bereits besucht -> Abbrechen
                    if (lowestNeightboursSorted.Count == 0)
                    {
                        noNeightboursLeft = true;
                        break;
                    }

                    directionRes = lowestNeightboursSorted[0];
                    lowestNeightboursSorted.RemoveAt(0);

                } while (alreadyVisited(visitedXY, x + directionRes[0], y + directionRes[1]));

                if (noNeightboursLeft)
                {
                    //diesen Fluss beenden
                    break;
                }
                else
                {
                    x += directionRes[0];
                    y += directionRes[1];
                }


#if (RIVERSASCOROUTINE)
                //TODO:debug
                td.SetHeights(0, 0, newHeightData);

                float posX = ((y - directionRes[1]) / (float)td.alphamapWidth) * td.size.x + transform.position.x; // = worldPos.x
                float posZ = ((x - directionRes[0]) / (float)td.alphamapHeight) * td.size.z + transform.position.z; // = worldPos.x
                //Debug.Log("x - directionRes[0]: "+x+ "\ttd.alphamapWidth: "+ td.alphamapWidth+ "\ttd.size.x: "+ td.size.x+ "\ttransform.position.x: "+ transform.position.x);
                
                Vector3 pos = new Vector3(posX, 0, posZ);
                //Debug.Log("posX: "+ posX+ "\tposZ: "+ posZ+ "\tpos: "+ pos);

                debugPosMarker.position = pos;

                yield return new WaitForSeconds(0.0001f);
                //yield return new WaitForSeconds(0.05f);
#endif
            }

        }

        setNewHeightData();
    }

    /// <summary>
    /// Der Fluss startet am Terrainrand mit einer angegebenen Tiefe.
    /// Er laeuft durch das gesamte Terrain, egal wie es verlaeuft. Dh. er laeuft durch Berge und teilt diese so.
    /// Sollte das Terrain zu niedrig werden, senkt sich der Fluss entsprechend und bleibt auf dieser hoehe.
    /// </summary>
#if (RIVERSASCOROUTINE)
    protected IEnumerator generateRiversIE_V3()
#else
    protected void generateRiversIE_V3()
#endif
    {
        initHeight();


        float[,] newHeightDataCopy = (float[,])newHeightData.Clone();

        //Gilt fuer alle Fluesse (damit zu generierende Fluesse in bereits generierte fliessen koennen)
        Hashtable visitedXY = new Hashtable();

        for (int i = 0; i < amountRiversThrough; i++)
        {
            int x = UnityEngine.Random.Range(0, 2) > 0 ? newHeightData.GetLength(1) - 2 : 0 + 1;
            int y = UnityEngine.Random.Range(0, 2) > 0 ? newHeightData.GetLength(0) - 2 : 0 + 1;

            //Entweder x != 0 bzw. Xmax oder y != 0 bzw. Ymax
            //Damit Fluss immer an einem Rand started
            if (UnityEngine.Random.Range(0, 2) > 0)
            {
                x = UnityEngine.Random.Range(0, newHeightData.GetLength(1));
            }
            else
            {
                y = UnityEngine.Random.Range(0, newHeightData.GetLength(0));
            }
            


            List<int[]> excepDirList = new List<int[]> { new int[] { 0, 0 } };
            //Fluss startet an Rand des Terrain - Verhindern, dass er direkt aus dem Terrain raus fliesst
            if (x == 1)
            {
                excepDirList.Add(new int[] { -1, 1 });
                excepDirList.Add(new int[] { -1, 0 });
                excepDirList.Add(new int[] { -1, -1 });

                excepDirList.Add(new int[] { 0, 1 });
                excepDirList.Add(new int[] { 0, -1 });
            }
            else if (x == newHeightData.GetLength(1) - 2)
            {
                excepDirList.Add(new int[] { 1, 1 });
                excepDirList.Add(new int[] { 1, 0 });
                excepDirList.Add(new int[] { 1, -1 });

                excepDirList.Add(new int[] { 0, 1 });
                excepDirList.Add(new int[] { 0, -1 });
            }
            else if (y == 1)
            {
                excepDirList.Add(new int[] { 1, -1 });
                excepDirList.Add(new int[] { 0, -1 });
                excepDirList.Add(new int[] { -1, -1 });

                excepDirList.Add(new int[] { 1, 0 });
                excepDirList.Add(new int[] { -1, 0 });
            }
            else
            {
                excepDirList.Add(new int[] { 1, 1 });
                excepDirList.Add(new int[] { 0, 1 });
                excepDirList.Add(new int[] { -1, 1 });

                excepDirList.Add(new int[] { 1, 0 });
                excepDirList.Add(new int[] { -1, 0 });
            }


            int[] directionRes = randomDirection(excepDirList);
            int[] firstDir = new int[] { directionRes[0], directionRes[1] };


            float riverHeight = newHeightDataCopy[x, y] - riverStartDepth;
            float riverHeight_old = riverHeight;
            
            while (y > 0 && y < newHeightData.GetLength(0) - 1 && x > 0 && x < newHeightData.GetLength(1) - 1)
            {
                
                if (newHeightDataCopy[x, y] > riverHeight)
                {
                    //Terrain ist hoeher als Fluss
                    riverHeight = newHeightDataCopy[x, y] - riverStartDepth;
                    //Fluss darf nicht aufwaerts Fliessen
                    if (riverHeight > riverHeight_old)
                    {
                        riverHeight = riverHeight_old;
                    }

                }

                if (riverHeight < 0)
                {
                    riverHeight = 0;
                }


                //Andere Flussstelle gefunden -> Fluss fertig
                if (alreadyVisited(visitedXY, x, y))
                {
                    break;
                }

                createHole(x, y, riverHeight, riverSlopeMin, riverSlopeMax, 0f);
                riverHeight_old = riverHeight;
                visitedXY.Add("x" + x + "y" + y, new int[] { x, y });


                bool randomDirectionChange = UnityEngine.Random.Range(0, 100) < riverDirectionChangeChance;
                if (randomDirectionChange)
                {

                    List<int[]> except = new List<int[]> {
                         new int[] { directionRes[0], directionRes[1] }
                    };
                    except.AddRange(excepDirList);

                    List<int[]> possibleDir = new List<int[]>() {
                        new int[] {1, -1},
                        new int[] {1, 0},
                        new int[] {1, 1},

                        new int[] {0, -1},
                        new int[] {0,1},

                        new int[] {-1,-1},
                        new int[] {-1,0},
                        new int[] {-1,1}
                    };

                    removeBackwardsDirection(firstDir, possibleDir);

                    try
                    {
                        directionRes = randomDirectionFrom(possibleDir, except);
                    }
                    catch (ArgumentException ae)
                    {
                        Debug.Log(ae.ToString());
                        break;
                    }


                }

                x += directionRes[0];
                y += directionRes[1];



#if (RIVERSASCOROUTINE)
                //TODO:debug
                td.SetHeights(0, 0, newHeightData);

                float posX = ((y - directionRes[1]) / (float)td.alphamapWidth) * td.size.x + transform.position.x; // = worldPos.x
                float posZ = ((x - directionRes[0]) / (float)td.alphamapHeight) * td.size.z + transform.position.z; // = worldPos.x
                //Debug.Log("x - directionRes[0]: "+x+ "\ttd.alphamapWidth: "+ td.alphamapWidth+ "\ttd.size.x: "+ td.size.x+ "\ttransform.position.x: "+ transform.position.x);
                
                Vector3 pos = new Vector3(posX, 0, posZ);
                //Debug.Log("posX: "+ posX+ "\tposZ: "+ posZ+ "\tpos: "+ pos);

                debugPosMarker.position = pos;

                yield return new WaitForSeconds(0.0001f);
                //yield return new WaitForSeconds(0.05f);
#endif
            }

        }

        setNewHeightData();
    }

    private int[] randomDirectionFrom(List<int[]> possibleDir, List<int[]> except)
    {
        List<int[]> possibleDirClone = new List<int[]>(possibleDir);
        int rndPos;

        int[] res = new int[2];

        do
        {
            if (possibleDirClone.Count == 0)
            {
                throw new ArgumentException("No matching direction in List");
            }

            rndPos = UnityEngine.Random.Range(0, possibleDirClone.Count);
            res = possibleDirClone[rndPos];
            possibleDirClone.RemoveAt(rndPos);
        } while (dirIsInExcept(except, res));


        return res;
    }

    private int[] randomDirection(List<int[]> except)
    {
        List<int[]> possibleDir = new List<int[]>() {
            new int[] {1, -1},
            new int[] {1, 0},
            new int[] {1, 1},

            new int[] {0, -1},
            new int[] {0, 0},
            new int[] {0,1},

            new int[] {-1,-1},
            new int[] {-1,0},
            new int[] {-1,1}
        };

        return randomDirectionFrom(possibleDir, except);
    }

    private bool dirIsInExcept(List<int[]> except, int[] res)
    {
        foreach (int[] exc in except)
        {
            if (exc[0] == res[0] && exc[1] == res[1])
                return true;
        }

        return false;
    }

    private void removeBackwardsDirection(int[] firstDir, List<int[]> lowestNeightbours)
    {
        if (firstDir[0] == 0 && firstDir[1] == 0)
            throw new ArgumentException("firstDir is x:0 y:0. This is no direction");


        /*
        Vorzustellen wie:
        x   y   |   x   y   |   x   y
        _____________________________
        1   -1  |   1   0  |   1    1
        ------------------------------
        0   -1  |   0   0  |   0    1
        ------------------------------
        -1  -1  |   -1  0  |   -1   1

        Gegenueberliegende Richtungen werden aus lowestNeightbours entfernt, da der Fluss sonst nicht weiterlaeuft
         */

        List<int[]> toRemove = new List<int[]>();

        if (firstDir[0] == 0)
        {
            toRemove.Add(new int[] { -1, firstDir[1] * -1 });
            toRemove.Add(new int[] { 0, firstDir[1] * -1 });
            toRemove.Add(new int[] { 1, firstDir[1] * -1 });
        }
        else if (firstDir[1] == 0)
        {
            toRemove.Add(new int[] { firstDir[0] * -1, -1 });
            toRemove.Add(new int[] { firstDir[0] * -1, 0 });
            toRemove.Add(new int[] { firstDir[0] * -1, 1 });
        }
        else if (firstDir[0] == 1)
        {
            toRemove.Add(new int[] { -1, 0 });
            toRemove.Add(new int[] { -1, firstDir[1] * -1 });
            toRemove.Add(new int[] { 0, firstDir[1] * -1 });
        }
        else if (firstDir[0] == -1)
        {
            toRemove.Add(new int[] { 1, 0 });
            toRemove.Add(new int[] { 1, firstDir[1] * -1 });
            toRemove.Add(new int[] { 0, firstDir[1] * -1 });
        }


        foreach (int[] remove in toRemove)
        {
            int[] e;
            for (int i = lowestNeightbours.Count - 1; i >= 0; i--)
            {
                e = lowestNeightbours[i];

                if (e[0] == remove[0] && e[1] == remove[1])
                    lowestNeightbours.RemoveAt(i);

            }
        }

    }

    private bool containsDirection(List<int[]> lowestNeightbours, int[] directionRes)
    {
        foreach (int[] e in lowestNeightbours)
        {
            if (e[0] == directionRes[0] && e[1] == directionRes[1])
                return true;
        }

        return false;
    }

    private int[] getRandomElementAndRemove(List<int[]> lowestNeightbours)
    {
        int i = UnityEngine.Random.Range(0, lowestNeightbours.Count);
        int[] e = lowestNeightbours[i];
        lowestNeightbours.RemoveAt(i);

        return e;
    }

    private bool alreadyVisited(Hashtable visitedXY, int x, int y)
    {
        return visitedXY.ContainsKey("x" + x + "y" + y);
    }

    /// <summary>
    /// Gibt den niedrigsten Nachbarn zurueck. Wenn mehrere Nachbarn den selben Wert haben, sind diese auch im Array enthalten
    /// </summary>
    /// <param name="x">x Position der Heightmap</param>
    /// <param name="y">y Position der Heightmap</param>
    /// <param name="heightData">Heightmap</param>
    /// <returns></returns>
    private List<int[]> getLowestNeighbours(int x, int y, float[,] heightData)
    {
        List<int[]> res = new List<int[]>();
        float lowestHeight = 1.1f;

        for (int i = -1; i < 2; i++)
        {
            for (int j = -1; j < 2; j++)
            {

                try
                {
                    if (i != 0 || j != 0)
                    {

                        if (heightData[x + i, y + j] < lowestHeight)
                        {
                            res.Clear();
                            res.Add(new int[] { i, j });
                            lowestHeight = heightData[x + i, y + j];
                        }
                        else if (heightData[x + i, y + j] == lowestHeight)
                        {
                            res.Add(new int[] { i, j });
                        }

                    }
                }
                catch (IndexOutOfRangeException oor)
                {
                    //Kann passieren, wenn x,y bereits der Rand ist
                    Debug.Log("getLowestNeighbours IndexOutOfRangeException - x + i: " + (x + i) + "\ty + j: " + (y + j));
                }

            }

        }

        return res;
    }

    /// <summary>
    /// Gibt alle Nachbarn der Hoehe nach sortiert zurueck. Niedrigste zuerst
    /// </summary>
    /// <param name="x">x Position der Heightmap</param>
    /// <param name="y">y Position der Heightmap</param>
    /// <param name="heightData">Heightmap</param>
    /// <returns></returns>
    private List<int[]> getNeighboursSorted(int x, int y, float[,] heightData)
    {
        List<float[]> tmp = new List<float[]>();

        for (int i = -1; i < 2; i++)
        {
            for (int j = -1; j < 2; j++)
            {

                try
                {
                    if (i != 0 || j != 0)
                    {
                        tmp.Add(new float[] { i, j, heightData[x + i, y + j] });
                    }
                }
                catch (IndexOutOfRangeException oor)
                {
                    //Kann passieren, wenn x,y bereits der Rand ist
                    Debug.Log("getLowestNeighbours IndexOutOfRangeException - x + i: " + (x + i) + "\ty + j: " + (y + j));
                }

            }

        }

        tmp.Sort((a, b) => a[2].CompareTo(b[2]));
        

        List<int[]> res = new List<int[]>();
        foreach (float[] t in tmp)
        {
            res.Add(new int[] { (int)t[0], (int)t[1] });
        }


        return res;
    }




    //------------------------------------/ Rivers-------------------------------------

    //------------------------------------Smooth Height-------------------------------------
    public void roughUpHeight()
    {
        initHeight();

        float amount;
        bool add;
        for (int i = 0; i < newHeightData.GetLength(0); i++)
        {
            for (int j = 0; j < newHeightData.GetLength(1); j++)
            {
                add = UnityEngine.Random.Range(0, 2) > 0;
                amount = UnityEngine.Random.Range(roughMin, roughMax);

                if (add)
                    newHeightData[i, j] += amount;
                else
                    newHeightData[i, j] -= amount;

            }
        }

        setNewHeightData();
    }

    //------------------------------------/Smooth Height-------------------------------------

    //------------------------------------Smooth Height-------------------------------------
    public void smoothHeight()
    {
        initHeight();


        for (int ite = 0; ite < smoothIterations; ite++)
        {
            float smoothedHeight;
            // = 1; < height/width -> Raender auslassen, weil eine Reihe/Zeile fehlt
            for (int i = 1; i < newHeightData.GetLength(0) - 1; i++)
            {
                for (int j = 1; j < newHeightData.GetLength(1) - 1; j++)
                {
                    smoothedHeight = (newHeightData[i + 1, j - 1] +
                        newHeightData[i + 1, j] +
                        newHeightData[i + 1, j + 1] +
                        newHeightData[i, j - 1] +
                        newHeightData[i, j] +
                        newHeightData[i, j + 1] +
                        newHeightData[i - 1, j - 1] +
                        newHeightData[i - 1, j] +
                        newHeightData[i - 1, j + 1]);
                    smoothedHeight /= 9f;

                    newHeightData[i, j] = smoothedHeight;
                }
            }
        }


        setNewHeightData();
    }

    //------------------------------------/Smooth Height-------------------------------------

    //------------------------------------Change Height-------------------------------------

    public void changeHeight()
    {
        initHeight();

        for (int i = 0; i < newHeightData.GetLength(0); i++)
        {
            for (int j = 0; j < newHeightData.GetLength(1); j++)
            {

                newHeightData[i, j] += changeHeightBy;
            }
        }

        setNewHeightData();
    }

    //------------------------------------/ Change Height-------------------------------------
    //------------------------------------Textures-------------------------------------

    [System.Serializable]
    public class SplatHeightTextures
    {
        public int textureIndex;
        public int startHeight;
        public float heightRandomness;

        public SplatHeightTextures()
        {

        }

        public SplatHeightTextures(int texIndex, int startHeight, float heightRandomness)
        {
            this.textureIndex = texIndex;
            this.startHeight = startHeight;
            this.heightRandomness = heightRandomness;
        }
    }


    public void sortHeightTextures()
    {
        List<SplatHeightTextures> shtCopy = new List<SplatHeightTextures>();
        shtCopy.AddRange(heightTextures);
        heightTextures.Clear();

        SplatHeightTextures sht;
        while (shtCopy.Count > 0)
        {
            sht = getLowestTextureMinVal(shtCopy, 0);
            heightTextures.Add(sht);
            shtCopy.Remove(sht);
        }

    }

    private SplatHeightTextures getLowestTextureMinVal(List<SplatHeightTextures> shtList, float minVal)
    {
        SplatHeightTextures lowestSht = shtList[shtList.Count - 1];
        foreach (SplatHeightTextures sht in shtList)
        {
            if (sht.startHeight < lowestSht.startHeight && sht.startHeight >= minVal)
            {
                lowestSht = sht;
            }
        }

        return lowestSht;
    }

    private SplatHeightTextures getTextureByHeight(List<SplatHeightTextures> shtList, float height)
    {
        return getTextureByHeight(shtList, height, false);
    }

    private SplatHeightTextures getTextureByHeight(List<SplatHeightTextures> shtList, float height, bool calcRandomness)
    {
        if (calcRandomness) {
            //Eigentliche Textur finden und deren heightRandomness herausfinden
            float rnd = getTextureByHeight(shtList, height, 0f).heightRandomness;
            //Textur mit entsprechendem random Wert finden
            return getTextureByHeight(shtList, height, rnd);
        }else
        {
            return getTextureByHeight(shtList, height, 0f);
        }
    }

    private SplatHeightTextures getTextureByHeight(List<SplatHeightTextures> shtList, float height, float heightRandomness)
    {
        if (heightRandomness != 0)
        {
            float rndVal = (float)SeedRND.NextDouble() * heightRandomness;
            rndVal = SeedRND.Next(0, 2) > 0 ? rndVal : rndVal * -1;
            height += rndVal;

            height = Mathf.Clamp(height, 0, 100);
        }

        SplatHeightTextures resSht = shtList[0];
        foreach (SplatHeightTextures sht in shtList)
        {

            if (sht.startHeight <= height && sht.startHeight > resSht.startHeight)
            {
                resSht = sht;
            }
        }

        return resSht;
    }



    public void generateTextures()
    {
        init();

        /*
        Je nachdem wie steil der Abhang ist bzw wie hoch die Stelle ist, eine passende Textur setzen
        
        Setzen der Textur basierend auf Hoehe
        https://www.youtube.com/watch?v=aUcWm1k0xDc
        https://www.youtube.com/watch?v=yv23i2PERR8
        */


        float[,,] smd = new float[td.alphamapWidth, td.alphamapHeight, td.alphamapLayers];

        for (int i = 0; i < td.alphamapWidth; i++)
        //for (int i = 0; i < (td.alphamapWidth * 0.025f); i++)
        {
            for (int j = 0; j < td.alphamapHeight; j++)
            //for (int j = 0; j < (td.alphamapHeight*0.025f); j++)
            {



                SplatHeightTextures sht = getSplatHeightTexture(i, j);

                float[] splat = new float[td.terrainLayers.Length];

                splat[sht.textureIndex] = 1;


                for (int k = 0; k < splat.Length; k++)
                {
                    smd[i, j, k] = splat[k];
                }


            }


        }

        td.SetAlphamaps(0, 0, smd);

    }

    private int[] getAlphaMapToHeightMapCoordinates(int x, int y)
    {
        return new int[] {
            (int)((y * td.heightmapHeight) / (float)td.alphamapHeight),
            (int)((x * td.heightmapWidth) / (float)td.alphamapWidth) };
    }

    private int[] getHeightMapToAlphaMapCoordinates(int x, int y)
    {
        return new int[] {
            (int)((y * td.alphamapHeight) / (float)td.heightmapHeight),
            (int)((x * td.alphamapWidth) / (float)td.heightmapWidth) };
    }


    private SplatHeightTextures getSplatHeightTexture(int x, int y)
    {
        SplatHeightTextures res = heightTextures[0];
        int[] heightMapCoor = getAlphaMapToHeightMapCoordinates(x, y);
        float height = td.GetHeight(heightMapCoor[0], heightMapCoor[1]);

        if (isCliff(heightMapCoor[0], heightMapCoor[1], countAsCliffHeightThreshold, cliffHeightRandomness))
            return textureCliff;

        if (heightTextures.Count == 0)
            throw new IndexOutOfRangeException("No textures set: heightTextures.Count == 0");


        res = getTextureByHeight(heightTextures, height, true);

        return res;
    }

    private bool isCliff(int x, int y, float countAsCliffHeightThreshold)
    {
        return isCliff(x, y, countAsCliffHeightThreshold, 0f);
    }
    //x,y sind heightMap coordinates
    private bool isCliff(int x, int y, float countAsCliffHeightThreshold, float cliffHeightRandomness)
    {
        float height = td.GetHeight(x, y);


        for (int i = -1; i < 2; i++)
        {
            for (int j = -1; j < 2; j++)
            {
                try
                {
                    //Wenn irgendein Nachbar der jetzigen Position um den countAsCliffHeightThreshold-Wert hoeher/tiefer liegt,
                    //dann ist die jetzige Position eine Klippe

                    
                    float rndVal = (float)SeedRND.NextDouble() * cliffHeightRandomness;
                    rndVal = SeedRND.Next(0, 2) > 0 ? rndVal : rndVal * -1;

                    if ((i != 0 || j != 0) && (Mathf.Abs(height - td.GetHeight(x + i, y + j)) > (countAsCliffHeightThreshold + rndVal)))
                    {
                        return true;
                    }
                }
                catch (IndexOutOfRangeException oor)
                {
                    //Kann passieren, wenn x,y bereits der Rand ist
                }

            }

        }

        return false;
    }

    //------------------------------------/Textures-------------------------------------
    //------------------------------------Generate vegetation-------------------------------------

    [System.Serializable]
    public class SpawnableObject
    {
        public List<GameObject> prefabs;
        public int amount;
        [Range(0f, 100f)]
        [Tooltip("Up to which threshold can the object be placed?")]
        public float cliffThreshold = 0.1f;
        [Tooltip("On which texture layers can the object be placed?")]
        public int[] legalTextureLayerIndexes;
        public bool rotateX = false;
        public bool rotateY = false;
        public bool rotateZ = false;

    }

    public class TextureLayerStrength
    {
        public int layerIndex;
        public float layerStrength;

        public TextureLayerStrength(int index, float strength)
        {
            this.layerIndex = index;
            this.layerStrength = strength;
        }
    }


    //OnCollisionExit triggert nicht immer,
    //dadurch koennen Objekte, bis maxVegetationCollisionCheckIterations erreicht ist,
    //weiter versucht werden zu positionieren
    public void generateVegetation()
    {
        init();
        initHeight();

        Debug.Log("generateVegetation start");
        startedStartCoroutine = 0;
        spawnObjectCalled = 0;

        Vector3 start = transform.position;
        Vector3 end = start + td.size;

        Transform vegetationParent = transform.Find("Vegetation");
        if (vegetationParent == null)
        {
            vegetationParent = new GameObject().transform;
            vegetationParent.name = "Vegetation";
            vegetationParent.parent = transform;
        }

        //vll. koennte man start/end in Segmente unterteilen und damit eine gleichmaessigere Verteilung erreichen (wenn man das will)
        foreach (SpawnableObject so in vegetationObjects)
        {
            startedStartCoroutine++;
            StartCoroutine(spawnObjects(start, end, so, vegetationParent));
        }


    }

    private IEnumerator spawnObjects(Vector3 areaStart, Vector3 areaEnd, SpawnableObject so, Transform parent)
    {
        for (int tc = 0; tc < so.amount; tc++)
        {
            startedStartCoroutine++;
            StartCoroutine(spawnObject(areaStart, areaEnd, so, parent));
        }

        startedStartCoroutine--;
        if (startedStartCoroutine == 0)
        {
            Debug.Log("generateVegetation done spawnObjectCalled: " + spawnObjectCalled);
        }

        yield return new WaitForEndOfFrame();
    }

    int spawnObjectCalled;
    private IEnumerator spawnObject(Vector3 areaStart, Vector3 areaEnd, SpawnableObject so, Transform parent)
    {
#if UNITY_EDITOR
        spawnObjectCalled++;

        int collisionCheckIterations = 0;

        GameObject spawned = PrefabUtility.InstantiatePrefab(so.prefabs[UnityEngine.Random.Range(0, so.prefabs.Count)]) as GameObject;


        Vector3? spawnPos = null;
        Quaternion rotation = spawned.transform.rotation;
        rotation.eulerAngles = rotateObjectRandom(so, spawned);

        spawned.transform.parent = parent;
        spawned.transform.rotation = rotation;

        List<Collider> goColls = getColliderFromGameObject(spawned);
        List<TmpCollisionDetection> tmoColDet = addTmpCollisionDetection(spawned, goColls);

        bool keepChecking = true;
        do
        {
            spawnPos = getLegalVegetationPosition(so, areaStart, areaEnd);
            if (spawnPos == null)
            {
                Debug.LogWarning("No legal spawn position found for object to spawn");
                break;
            }


            spawned.transform.position = spawnPos ?? new Vector3();

            yield return new WaitForEndOfFrame();

            bool hasCol = hasCollision(tmoColDet, goColls);
            keepChecking = hasCol;



            //damit keine endlos Schleife entstehen kann
            collisionCheckIterations++;
            // /
        } while (keepChecking && collisionCheckIterations < maxVegetationCollisionCheckIterations);
        if (keepChecking)
        {
            Debug.LogWarning("No legal and non colliding spawn position found for object to spawn: " + spawned.name);
            Destroy(spawned);
        }
        else
        {
            removeTmpAddedComponents(tmoColDet, spawned);
        }

        startedStartCoroutine--;
        if (startedStartCoroutine == 0)
        {
            Debug.Log("generateVegetation done spawnObjectCalled: " + spawnObjectCalled);
        }
#else
        yield return new WaitForEndOfFrame();
#endif
    }

    private Vector3 rotateObjectRandom(SpawnableObject so, GameObject spawned)
    {
        float x = 0;
        float y = 0;
        float z = 0;

        if (so.rotateX)
        {
            x = UnityEngine.Random.Range(0f, 360f);
        }
        if (so.rotateY)
        {
            y = UnityEngine.Random.Range(0f, 360f);
        }
        if (so.rotateZ)
        {
            z = UnityEngine.Random.Range(0f, 360f);
        }


        return new Vector3(x, y, z);
    }

    private void removeTmpAddedComponents(List<TmpCollisionDetection> tmpColDet, GameObject spawned)
    {
        foreach (TmpCollisionDetection tcd in tmpColDet)
        {
            Destroy(tcd.transform.GetComponent<Rigidbody>());
            Destroy(tcd);
        }

    }

    //Prefab darf keinen Rigidbody haben! Der wird im spaeteren Verlauf immer entfernt
    private List<TmpCollisionDetection> addTmpCollisionDetection(GameObject spawned, List<Collider> goColls)
    {
        List<TmpCollisionDetection> tmpColDet = new List<TmpCollisionDetection>();

        foreach (Collider col in goColls)
        {
            Rigidbody rb = col.gameObject.AddComponent<Rigidbody>();
            rb.constraints = RigidbodyConstraints.FreezeRotationX |
                RigidbodyConstraints.FreezeRotationY |
                RigidbodyConstraints.FreezeRotationZ |
                RigidbodyConstraints.FreezePositionX |
                RigidbodyConstraints.FreezePositionY |
                RigidbodyConstraints.FreezePositionZ;

            tmpColDet.Add(col.gameObject.AddComponent<TmpCollisionDetection>());

        }

        return tmpColDet;
    }

    private List<Collider> getColliderFromGameObject(GameObject spawned)
    {
        List<Collider> colls;
        Collider[] collsArr = spawned.transform.GetComponentsInChildren<Collider>(false);
        colls = new List<Collider>(collsArr);
        Collider GOCol = spawned.GetComponent<Collider>();
        if (GOCol != null)
            colls.Add(GOCol);

        return colls;
    }

    private bool hasCollision(List<TmpCollisionDetection> tmpDolDets, List<Collider> colls)
    {
        //Jeden Collider des Objektes durchgehen
        foreach (TmpCollisionDetection tdd in tmpDolDets)
        {
            //Jeden Collider, der mit irgendeinem Collider des Objectes kollidiert durchgehen
            foreach (Collider c in tdd.getInsideColls())
            {
                //Wenn irgendein kollidierender Collider kein eigender Collider && nicht das Terrain ist
                //-> Object kollidiert mit irgendetwas
                //Manchmal kann es vorkommen, dass ein Collider geloescht wird (Objekt platziert), aber in der Liste vorhanden bleibt -> als Kollision werten
                if (c == null || (!colls.Contains(c) && !"Terrain".Equals(c.transform.tag)))
                {
                    return true;
                }
            }
        }

        return false;
    }

    private Vector3? getLegalVegetationPosition(SpawnableObject so, Vector3 start, Vector3 end)
    {
        float legalThreshold = 0.75f;

        float rndX;
        float rndZ;


        int iterations = 0;

        int[] heightMapCoordinates;
        int[] alphaMapCoordinates;
        bool keepSearching = true;
        do
        {
            rndX = UnityEngine.Random.Range(start.x, end.x);
            rndZ = UnityEngine.Random.Range(start.z, end.z);

            heightMapCoordinates = getWorldToHeightMapCoordinates(rndX, rndZ);
            alphaMapCoordinates = getHeightMapToAlphaMapCoordinates(heightMapCoordinates[0], heightMapCoordinates[1]);
            
            if (!isCliff(heightMapCoordinates[0], heightMapCoordinates[1], so.cliffThreshold) && isOnLegalTextureLayer(alphaMapCoordinates[1], alphaMapCoordinates[0], so.legalTextureLayerIndexes, legalThreshold))
            {
                return (new Vector3(rndX, td.GetHeight(heightMapCoordinates[0], heightMapCoordinates[1]), rndZ));
            }

            //damit keine endlos Schleife entstehen kann
            iterations++;
            if (iterations >= maxLegalVegetationPositionIterations)
                keepSearching = false;
            // /

        } while (keepSearching);

        return null;
    }

    private int[] getWorldToHeightMapCoordinates(float x, float z)
    {
        if (newHeightData == null)
            Debug.LogError("getWorldToHeightMapCoordinates newHeightData need to be set");

        int mapX = (int)(((x - transform.position.x) / td.size.x) * newHeightData.GetLength(1));
        int mapZ = (int)(((z - transform.position.z) / td.size.z) * newHeightData.GetLength(0));

        return new int[] { mapX, mapZ };
    }

    //x,y sind Alphamap coordinates
    private bool isOnLegalTextureLayer(int x, int z, int[] legalTextureLayers, float legalThreshold)
    {
        TextureLayerStrength tls;
        foreach (int i in legalTextureLayers)
        {
            tls = getMainTextureLayerIndex(x, z);
            if (i == tls.layerIndex && tls.layerStrength >= legalThreshold)
                return true;
        }

        return false;
    }

    private TextureLayerStrength getMainTextureLayerIndex(int x, int z)
    {
        float[] mix = getTextureLayerMix(x, z);

        float mainStrength = 0;
        int mainIndex = 0;

        for (int i = 0; i < mix.Length; i++)
        {
            if (mix[i] > mainStrength)
            {
                mainIndex = i;
                mainStrength = mix[i];
            }
        }

        return new TextureLayerStrength(mainIndex, mainStrength);

    }

    private float[] getTextureLayerMix(int x, int z)
    {
        float[,,] splatmapData = td.GetAlphamaps(x, z, 1, 1);

        float[] texMix = new float[splatmapData.GetUpperBound(2) + 1];

        for (int n = 0; n < texMix.Length; n++)
        {
            texMix[n] = splatmapData[0, 0, n];
        }

        return texMix;
    }
    //------------------------------------/Generate vegetation-------------------------------------



}
