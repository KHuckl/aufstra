﻿//#define RIVERSASCOROUTINE

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


//TODO: undo Funktion
public class TerrainGenerator : MonoBehaviour
{
    [Header("Height Settings")]
    [Range(0.000f,0.01f)]
    public float heightBumpiness = 0.005f;
    [Range(0f, 1f)]
    public float heightDamp = 0.125f;

    [Header("Mountain Settings")]
    public int amountMountains;
    public int mountainsOffsetFromBorder;
    [Range(0.001f, 0.3f)]
    public float mountainHeightMin = 0.01f;
    [Range(0.001f, 0.3f)]
    public float mountainHeightMax = 0.025f;
    [Range(0.0001f, 0.3f)]
    public float mountainSlopeMin = 0.0001f;
    [Range(0.0001f, 0.3f)]
    public float mountainSlopeMax = 0.001f;

    [Header("Hole/Lake Settings")]
    public int amountHoles;
    public int holesOffsetFromBorder;
    [Range(0f, 1f)]
    public float maxHoleDepth;
    [Range(0.001f, 0.5f)]
    public float holeDepthMin = 0.015f;
    [Range(0.001f, 0.5f)]
    public float holeDepthMax = 0.03f;
    [Range(0.0001f, 0.3f)]
    public float holeSlopeMin = 0.003f;
    [Range(0.0001f, 0.3f)]
    public float holeSlopeMax = 0.015f;

    [Header("River Settings")]
    public float DEBUG_x;
    public float DEBUG_y;

    public int amountRivers;
    [Range(0.001f, 0.5f)]
    public float riverDepth = 0.025f;
    [Range(0.001f, 0.5f)]
    public float riverStartDepth = 0.025f;
    [Range(0.0001f, 1f)]
    public float riverMaxHeightChangeBeforeStop = 0.005f;

    [Range(0.0001f, 0.3f)]
    public float riverSlopeMin = 0.005f;
    [Range(0.0001f, 0.3f)]
    public float riverSlopeMax = 0.015f;
    [Range(0, 100)]
    public int riverDirectionChangeChance = 2;
    [Range(0f, 1f)]
    public float slopeChange = 0.25f;
    //Wenn Fluss bergauffliesst, fliesst er erstmal eine  Zeit lang nur in diese Richtung (dreht sonst direkt wieder um und fliesst in sich selber)
    public int directionLockedForIterations = 5;
    protected int directionLockedForIterationsDone = 0;

    [Header("Rough up Settings")]
    [Range(0, 0.05f)]
    public float roughMin = 0.000f;
    [Range(0.0001f, 0.05f)]
    public float roughMax = 0.001f;

    [Header("Smooth Settings")]
    [Range(0, 10)]
    public int smoothIterations = 1;

    [Header("Lift/Sink Settings")]
    [Range(-1f,1f)]
    public float changeHeightBy = 0f;

    [Header("Texture Settings")]
    public List<SplatHeightTextures> heightTextures;
    [Range(0f, 100f)]
    public float countAsCliffHeightThreshold;
    [Range(0f, 100f)]
    public float cliffHeightRandomness;
    public SplatHeightTextures textureCliff;

    [Header("Vegetation Settings")]
    public List<SpawnableObject> vegetationObjects;
    [Range(1, 100000)]
    public int maxVegetationCollisionCheckIterations = 1000;
    [Range(1, 100000)]
    public int maxLegalVegetationPositionIterations = 1000;
    int startedStartCoroutine;


    protected TerrainData td;
    protected float[,] newHeightData;



    //TODO: debug
    public Transform debugPosMarker;


    protected void init()
    {
        //Debug.Log("init");
        td = transform.GetComponent<Terrain>().terrainData;
    }

    protected void initHeight()
    {
        init();
        newHeightData = td.GetHeights(0, 0, td.heightmapWidth, td.heightmapHeight);
    }

    protected void setNewHeightData()
    {
        td.SetHeights(0, 0, newHeightData);
    }

    //------------------------------------Height-------------------------------------
    public void generateHeight()
    {
        initHeight();

        //float[,] newHeightData = new float[td.alphamapWidth, td.alphamapHeight];


        genHeight_perlinNoise(newHeightData);



        //td.SetHeights(0, 0, newHeightData);
        setNewHeightData();
    }

    //TODO: (x+xSeed) * heightBumpiness, (y+ySeed) * heightBumpiness
    private void genHeight_perlinNoise(float[,] newHeightData)
    {
        for(int y = 0; y < newHeightData.GetLength(0); y++)
        {
            for(int x=0; x< newHeightData.GetLength(1); x++)
            {
                newHeightData[x, y] = Mathf.PerlinNoise(x * heightBumpiness, y * heightBumpiness) * heightDamp;
            }
        }
    }
    //------------------------------------/ Height-------------------------------------

    //------------------------------------Mountains-------------------------------------
    public void generateMountains()
    {
        initHeight();

        if (mountainsOffsetFromBorder > newHeightData.GetLength(1) / 2 || mountainsOffsetFromBorder > newHeightData.GetLength(0) / 2)
        {
            throw new System.ArgumentException("mountainsOffsetFromBorder is to high");
        }
        if (mountainHeightMin > mountainHeightMax)
        {
            throw new System.ArgumentException("mountainHeightMin is higher than mountainHeightMax");
        }
        if(mountainSlopeMin <= 0)
        {
            throw new System.ArgumentException("mountainSlopeMin needs to be greater than 0");
        }
        if (mountainSlopeMin > mountainSlopeMax)
        {
            throw new System.ArgumentException("mountainSlopeMin is higher than mountainSlopeMax");
        }


        float mountainHeight;
        for (int i=0; i<amountMountains; i++)
        {
            mountainHeight = UnityEngine.Random.Range(mountainHeightMin, mountainHeightMax);

            int x = UnityEngine.Random.Range(mountainsOffsetFromBorder, newHeightData.GetLength(1) - mountainsOffsetFromBorder);
            int y = UnityEngine.Random.Range(mountainsOffsetFromBorder, newHeightData.GetLength(1) - mountainsOffsetFromBorder);

            float newHeight = newHeightData[x, y] + mountainHeight;
            
            createMountain(x, y, newHeight, mountainSlopeMin, mountainSlopeMax);
        }


        //td.SetHeights(0, 0, newHeightData);
        setNewHeightData();
    }

    private void createMountain(int x, int y, float height, float mountainSlopeMin, float mountainSlopeMax)
    {
        if (x < 0 || x >= newHeightData.GetLength(1))
            return;
        if (y < 0 || y >= newHeightData.GetLength(0))
            return;
        if (height <= 0)
            return;
        if (newHeightData[x, y] >= height)
            return;
        newHeightData[x, y] = height;

        createMountain(x - 1, y, height - UnityEngine.Random.Range(mountainSlopeMin, mountainSlopeMax), mountainSlopeMin, mountainSlopeMax);
        createMountain(x + 1, y, height - UnityEngine.Random.Range(mountainSlopeMin, mountainSlopeMax), mountainSlopeMin, mountainSlopeMax);
        createMountain(x, y - 1, height - UnityEngine.Random.Range(mountainSlopeMin, mountainSlopeMax), mountainSlopeMin, mountainSlopeMax);
        createMountain(x, y + 1, height - UnityEngine.Random.Range(mountainSlopeMin, mountainSlopeMax), mountainSlopeMin, mountainSlopeMax);
    }

    //------------------------------------/ Mountains-------------------------------------

    //------------------------------------Holes/Lakes-------------------------------------

    public void generateHoles()
    {
        initHeight();

        if (holesOffsetFromBorder > newHeightData.GetLength(1) / 2 || holesOffsetFromBorder > newHeightData.GetLength(0) / 2)
        {
            throw new System.ArgumentException("holesOffsetFromBorder is to high");
        }
        if (holeDepthMin > holeDepthMax)
        {
            throw new System.ArgumentException("holeDephtMin is higher than holeDephtMax");
        }
        if (holeSlopeMin <= 0)
        {
            throw new System.ArgumentException("holeSlopeMin needs to be greater than 0");
        }
        if (holeSlopeMin > holeSlopeMax)
        {
            throw new System.ArgumentException("holeSlopeMin is higher than holeSlopeMax");
        }


        float holeDepth;
        for (int i = 0; i < amountHoles; i++)
        {
            holeDepth = UnityEngine.Random.Range(holeDepthMin, holeDepthMax);

            int x = UnityEngine.Random.Range(holesOffsetFromBorder, newHeightData.GetLength(1) - holesOffsetFromBorder);
            int y = UnityEngine.Random.Range(holesOffsetFromBorder, newHeightData.GetLength(0) - holesOffsetFromBorder);

            float newHeight = newHeightData[x, y] - holeDepth;

            createHole(x, y, newHeight, holeSlopeMin, holeSlopeMax, maxHoleDepth);
        }


        //td.SetHeights(0, 0, newHeightData);
        setNewHeightData();
    }

    private void createHole(int x, int y, float height, float holeSlopeMin, float holeSlopeMax, float maxDepth)
    {
        if (x < 0 || x >= newHeightData.GetLength(1))
        {
            //Debug.Log("createHole RETURN x < 0 || x > td.alphamapWidth");
            return;
        }
        if (y < 0 || y >= newHeightData.GetLength(0))
        {
            //Debug.Log("createHole RETURN y < 0 || y > td.alphamapHeight");
            return;
        }
        if (height < maxDepth)
        {
            //Debug.Log("createHole RETURN height < maxDepth");
            return;
        }
        if (newHeightData[x, y] <= height)
        {
            //Debug.Log("createHole RETURN newHeightData[x, y] <= height");
            return;
        }

        newHeightData[x, y] = height;

        createHole(x - 1, y, height + UnityEngine.Random.Range(holeSlopeMin, holeSlopeMax), holeSlopeMin, holeSlopeMax, maxDepth);
        createHole(x + 1, y, height + UnityEngine.Random.Range(holeSlopeMin, holeSlopeMax), holeSlopeMin, holeSlopeMax, maxDepth);
        createHole(x, y - 1, height + UnityEngine.Random.Range(holeSlopeMin, holeSlopeMax), holeSlopeMin, holeSlopeMax, maxDepth);
        createHole(x, y + 1, height + UnityEngine.Random.Range(holeSlopeMin, holeSlopeMax), holeSlopeMin, holeSlopeMax, maxDepth);
    }

    //------------------------------------/ Holes/Lakes-------------------------------------

//------------------------------------Rivers-------------------------------------



    public void generateRivers()
    {
        /*
#if (RIVERSASCOROUTINE)
        StartCoroutine(generateRiversIE());
# else
        generateRiversIE();
#endif
*/

#if (RIVERSASCOROUTINE)
        StartCoroutine(generateRiversIE_V2());
#else
        generateRiversIE_V2();
#endif

    }

    public void generateRiversWholeTerrain()
    {
#if (RIVERSASCOROUTINE)
        StartCoroutine(generateRiversIE_V3());
#else
        generateRiversIE_V3();
#endif

    }


#if (RIVERSASCOROUTINE)
        protected IEnumerator generateRiversIE()
#else
    protected void generateRiversIE()
#endif
    {
        initHeight();

        Debug.Log("generateRivers started");

        //TODO:
        //https://www.youtube.com/watch?v=TnZWEUY78bU


        float[,] newHeightDataCopy = (float[,]) newHeightData.Clone();

        Hashtable visitedXY = new Hashtable();

        for (int i=0; i< amountRivers; i++)
        {
            //Debug.Log("generateRivers in for");

            int x = UnityEngine.Random.Range(0, newHeightData.GetLength(1));
            int y = UnityEngine.Random.Range(0, newHeightData.GetLength(0));

            //TODO: debug
            //x = (int)(td.alphamapWidth * DEBUG_x); //152
            //y = (int)(td.alphamapHeight * DEBUG_y); //142

            int xdir = UnityEngine.Random.Range(-1, 2);
            int ydir = UnityEngine.Random.Range(-1, 2);

            int[] firstDir=null;
            int[] directionRes = null;
            float lastHeight=1;

            bool directionLocked = false;
            while (y > 0 && y < newHeightData.GetLength(0)-1 && x > 0 && x < newHeightData.GetLength(1)-1)
            {
                /*
                //if(newHeightDataCopy[x, y] - riverDepth > lastHeight + riverMaxHeightChangeBeforeStop)
                if (newHeightDataCopy[x, y] - riverDepth > lastHeight)
                {
                    //Hoehenunterschied ausgehend vom letzten Schritt zu gross -> Fluss kann nicht weiter (bergauf) fliessen
                    Debug.Log("riverMaxHeightChangeBeforeStop break;");
                    break;
                }
                */



                lastHeight = newHeightDataCopy[x, y] - riverDepth;
                if (lastHeight < 0){
                    lastHeight = 0;
                }

                //Debug.Log("generateRivers in while - 1 x: " + x + "\ty: " + y+ "\tlastHeight: "+ lastHeight+"("+ (lastHeight * td.size.y) +")");

                //Debug.Log("createHole: "+x+", "+y+ "\tlastHeight: "+ lastHeight+ "\triverSlopeMin: "+ riverSlopeMin+ "\triverSlopeMax: "+ riverSlopeMax);
                createHole(x, y, lastHeight, riverSlopeMin, riverSlopeMax, 0f);

                /*
                if(Random.Range(0,100) <= riverDirectionChangeChance)
                {
                    xdir = Random.Range(-1, 2);
                }
                if (Random.Range(0, 100) <= riverDirectionChangeChance)
                {
                    ydir = Random.Range(-1, 2);
                }

                x += xdir;
                y += ydir;
                */




                

                if(!directionLocked)
                {
                    int[] directionRes_old = new int[] { 1, 0 };
                    if (directionRes != null)
                    {
                        if(firstDir == null)
                        {
                            firstDir = new int[2];
                            firstDir[0] = directionRes[0];
                            firstDir[1] = directionRes[1];
                        }

                        directionRes_old[0] = directionRes[0];
                        directionRes_old[1] = directionRes[1];
                    }

                    List<int[]> lowestNeightbours = getLowestNeighbours(x, y, newHeightDataCopy);

                    //Durch das Entfernen der "Rueckwaerts"-Richtungen, wird verhindert, dass der Fluss in einem Tal bleibt und aufhoert zu fliessen
                    //Dadruch werden oft sehr kurze Fluesse erstellt.
                    //Allerdings fliessen damit Fluesse auch (unrealistischerweise, trotzdem gewollt (wie gesagt: kurze Fluesse)) Bergauf
                    //und koennen keine Wendungen haben
                    if(firstDir != null)
                        removeBackwardsDirection(firstDir, lowestNeightbours);

                    bool randomDirectionChange = UnityEngine.Random.Range(0, 100) < riverDirectionChangeChance;
                    bool noLowestNeightbourFound = false;
                    //Debug.Log("-----do------");
                    do
                    {

                        if (directionRes == null || randomDirectionChange || !containsDirection(lowestNeightbours, directionRes))
                        {
                            //Debug.Log("randomDirectionChange: " + randomDirectionChange);
                            //Nur aufrufen, wenn auch Elemente vorhanden sind. Wenn nicht, wird alte Richtung beibehalten(damit Fluss weiterfliesst)
                            if(lowestNeightbours.Count > 0)
                                directionRes = getRandomElementAndRemove(lowestNeightbours);
                        }
                        else
                        {
                            //Debug.Log("!ln.c(dRes): "+(!lowestNeightbours.Contains(directionRes)));
                        }




                        //Debug.Log("res[0]: " + directionRes[0] + "\tres[1]" + directionRes[1]);
                        /*if (0 == res[0] && 0==res[1])
                        {
                            //passiert nicht
                            break;
                        }
                        else */
                        if (newHeightDataCopy[x + directionRes[0], y + directionRes[1]] > newHeightDataCopy[x, y] || alreadyVisited(visitedXY, x + directionRes[0], y + directionRes[1]))
                        {

                            //Debug.Log("alreadyVisited?: " + alreadyVisited(visitedXY, x + directionRes[0], y + directionRes[1]) + "\tnewHeightDataCopy[" + (x + directionRes[0]) + ", " + (y + directionRes[1]) + "]: " + newHeightDataCopy[x + directionRes[0], y + directionRes[1]] + "\tnewHeightDataCopy[" + x + ", " + y + "]: " + newHeightDataCopy[x, y]);
                            /*
                            //Alle anderen Seiten sind hoeher
                            Debug.Log("noLowestNeightbour break;");
                            break;
                            */
                            //Debug.Log("forced randomDirectionChange");
                            randomDirectionChange = true;

                            noLowestNeightbourFound = true;
                        }
                        else
                        {
                            noLowestNeightbourFound = false;
                            break;
                        }


                    } while (lowestNeightbours.Count > 0);

                    if (noLowestNeightbourFound)
                    {
                        /*
                        Debug.Log("noLowestNeightbourFound: dirRes[0]: " + directionRes[0] + "\tdirRes[1]: " + directionRes[1] + "\tdirResOLD[0]: " + directionRes_old[0] + "\tdirResOLD[1]: " + directionRes_old[1]);
                        Debug.Log("newHeightDataCopy[x + directionRes_old[0], y + directionRes_old[1]]: " + newHeightDataCopy[x + directionRes_old[0], y + directionRes_old[1]]);
                        Debug.Log("riverMaxHeightChangeBeforeStop: " + riverMaxHeightChangeBeforeStop);
                        Debug.Log("newHeightDataCopy[x, y]: " + newHeightDataCopy[x, y]);
                        Debug.Log("alreadyVisited(visitedXY, x + directionRes_old[0], y + directionRes_old[1]): " + alreadyVisited(visitedXY, x + directionRes_old[0], y + directionRes_old[1]));
                        */

                        //Wenn kein passender Nachbar gefunden, gibt es nur noch die Chance, dass der Fluss weiter fliest,
                        //wenn der Nachbar in der alten Richtung nicht hoeher als ein Schwellwert ist ( riverMaxHeightChangeBeforeStop)
                        if (newHeightDataCopy[x + directionRes_old[0], y + directionRes_old[1]] - riverMaxHeightChangeBeforeStop > newHeightDataCopy[x, y] || alreadyVisited(visitedXY, x + directionRes_old[0], y + directionRes_old[1]))
                        {
                            //Debug.Log("noLowestNeightbourFound - break");
                            break;
                        }
                        else
                        {
                            //Debug.Log("noLowestNeightbourFound - directionRes = directionRes_old");

                            //Fluss weiterlaufen lassen 
                            directionRes = directionRes_old;

                            directionLocked = true;
                        }
                    }
                }
                else
                {
                    directionLockedForIterationsDone++;

                    if (directionLockedForIterationsDone >= directionLockedForIterations)
                    {
                        directionLockedForIterationsDone = 0;
                        directionLocked = false;
                    }
                }

                



                //Debug.Log("visitedXY.Add(x + "+x+" + y + "+y+")");
                visitedXY.Add("x" + x + "y" + y, new int[] { x, y });

                x += directionRes[0];
                y += directionRes[1];
                //Debug.Log("2 y: " + y + "\tx: " + x);


                /*
                if (x == td.alphamapWidth || y == td.alphamapHeight) { 
                    //TODO: falls fluesse mal am Rand starten sollen, muss das hier modifiziert werden

                    //rand erreicht -> aufhoeren
                    break;
                }
                */


#if (RIVERSASCOROUTINE)
                //TODO:debug
                td.SetHeights(0, 0, newHeightData);

                float posX = ((y - directionRes[1]) / (float)td.alphamapWidth) * td.size.x + transform.position.x; // = worldPos.x
                float posZ = ((x - directionRes[0]) / (float)td.alphamapHeight) * td.size.z + transform.position.z; // = worldPos.x
                //Debug.Log("x - directionRes[0]: "+x+ "\ttd.alphamapWidth: "+ td.alphamapWidth+ "\ttd.size.x: "+ td.size.x+ "\ttransform.position.x: "+ transform.position.x);
                
                Vector3 pos = new Vector3(posX, 0, posZ);
                //Debug.Log("posX: "+ posX+ "\tposZ: "+ posZ+ "\tpos: "+ pos);
                /*
                int mapX = (int)(((worldPos.x - transform.position.x) / td.size.x) * td.alphamapWidth);
                int mapZ = (int)(((worldPos.z - transform.position.z) / td.size.z) * td.alphamapHeight);
                // Get the splat data for this cell as a 1x1xN 3d array (where N = number of textures)
                splatmapData = terrainData.GetAlphamaps(mapX, mapZ, 1, 1);
                */

                //debugPosMarker.position = new Vector3(((float)(x - directionRes[0]) / td.heightmapWidth) + transform.position.x, transform.position.y + (lastHeight * td.size.y), ((float)(y - directionRes[1]) / td.heightmapHeight) + transform.position.z);
                debugPosMarker.position = pos;

                yield return new WaitForSeconds(0.0001f);
#endif
            }

        }

        Debug.Log("generateRivers done");
        //td.SetHeights(0, 0, newHeightData);
        setNewHeightData();
    }


    /// <summary>
    /// Generiert einen Fluss, der versucht immer in die Richtung des Gefaelles zu fliessen.
    /// Sollte kein echtes Gefaelle vorhanden sein, wird der niedrigste Nachbar genommen. Dessen (neue) Hoehe wird die letzte Flusstiefe nicht ueberschreiten.
    /// Dies passiert solange, bis der Fluss das Ende des Terrain erreicht oder bis er sich in einer Sackgasse(bereits durch alle Nachbarn geflossen) befindet.
    /// Dadurch koennen auch Seen entstehen.
    /// </summary>
    /// <returns></returns>
#if (RIVERSASCOROUTINE)
    protected IEnumerator generateRiversIE_V2()
#else
    protected void generateRiversIE_V2()
#endif
    {
        initHeight();

        Debug.Log("generateRivers V2 started");

        float[,] newHeightDataCopy = (float[,])newHeightData.Clone();

        //Gilt fuer alle Fluesse (damit zu generierende Fluesse in bereits generierte fliessen koennen)
        Hashtable visitedXY = new Hashtable();

        for (int i = 0; i < amountRivers; i++)
        {
            int x = UnityEngine.Random.Range(0, newHeightData.GetLength(1));
            int y = UnityEngine.Random.Range(0, newHeightData.GetLength(0));

            //TODO: debug
            //x = (int)(td.alphamapWidth * DEBUG_x); //152
            //y = (int)(td.alphamapHeight * DEBUG_y); //142
            
            int[] directionRes = null;
            float riverHeight = newHeightDataCopy[x, y] - riverStartDepth;
            float riverHeight_old = riverHeight;


            while (y > 0 && y < newHeightData.GetLength(0) - 1 && x > 0 && x < newHeightData.GetLength(1) - 1)
            {
                //terrain ist tiefer als fluss
                /*if(newHeightDataCopy[x, y] < riverHeight)
                {
                    //fluss tiefe lassen wie sie ist
                }else */if(newHeightDataCopy[x, y] > riverHeight)
                {
                    //terrain ist hoeher als fluss
                    riverHeight = newHeightDataCopy[x, y] - riverStartDepth;
                    //fluss darf nicht aufwaerts fliessen
                    if(riverHeight > riverHeight_old)
                    {
                        riverHeight = riverHeight_old;
                    }

                }
                
                if (riverHeight < 0)
                {
                    riverHeight = 0;
                }
                
                createHole(x, y, riverHeight, riverSlopeMin, riverSlopeMax, 0f);
                riverHeight_old = riverHeight;
                //Debug.Log("visitedXY.Add(x + "+x+" + y + "+y+")");
                visitedXY.Add("x" + x + "y" + y, new int[] { x, y });

                int[] directionRes_old = new int[] { 1, 0 };
                if (directionRes != null)
                {
                    directionRes_old[0] = directionRes[0];
                    directionRes_old[1] = directionRes[1];
                }

                //directionRes = lowestNeightbours[UnityEngine.Random.Range(0, lowestNeightbours.Count)];
                List<int[]> lowestNeightboursSorted = getNeighboursSorted(x, y, newHeightDataCopy);
                bool noNeightboursLeft = false;
                do
                {
                    //alle Nachbarn wurden geprueft und alle wurden bereits besucht -> Abbrechen
                    if (lowestNeightboursSorted.Count == 0)
                    {
                        noNeightboursLeft = true;
                        break;
                    }

                    directionRes = lowestNeightboursSorted[0];
                    lowestNeightboursSorted.RemoveAt(0);

                } while (alreadyVisited(visitedXY, x + directionRes[0], y + directionRes[1]));

                if (noNeightboursLeft)
                {
                    //diesen Fluss beenden
                    break;
                }
                else { 
                    //Debug.Log("visitedXY.Add(x + "+x+" + y + "+y+")");
                    //visitedXY.Add("x" + x + "y" + y, new int[] { x, y });

                    x += directionRes[0];
                    y += directionRes[1];
                    //Debug.Log("2 y: " + y + "\tx: " + x);
                }


#if (RIVERSASCOROUTINE)
                //TODO:debug
                td.SetHeights(0, 0, newHeightData);

                float posX = ((y - directionRes[1]) / (float)td.alphamapWidth) * td.size.x + transform.position.x; // = worldPos.x
                float posZ = ((x - directionRes[0]) / (float)td.alphamapHeight) * td.size.z + transform.position.z; // = worldPos.x
                //Debug.Log("x - directionRes[0]: "+x+ "\ttd.alphamapWidth: "+ td.alphamapWidth+ "\ttd.size.x: "+ td.size.x+ "\ttransform.position.x: "+ transform.position.x);
                
                Vector3 pos = new Vector3(posX, 0, posZ);
                //Debug.Log("posX: "+ posX+ "\tposZ: "+ posZ+ "\tpos: "+ pos);

                debugPosMarker.position = pos;

                yield return new WaitForSeconds(0.0001f);
                //yield return new WaitForSeconds(0.05f);
#endif
            }

        }

        Debug.Log("generateRivers V2 done");
        setNewHeightData();
    }

    /// <summary>
    /// 
    /// </summary>
#if (RIVERSASCOROUTINE)
    protected IEnumerator generateRiversIE_V3()
#else
    protected void generateRiversIE_V3()
#endif
    {
        initHeight();

        Debug.Log("generateRivers V3 started");

        float[,] newHeightDataCopy = (float[,])newHeightData.Clone();

        //Gilt fuer alle Fluesse (damit zu generierende Fluesse in bereits generierte fliessen koennen)
        Hashtable visitedXY = new Hashtable();

        for (int i = 0; i < amountRivers; i++)
        {
            //Debug.Log("newHeightData.GetLength(1): "+ newHeightData.GetLength(1));
            int x = UnityEngine.Random.Range(0, 2) > 0 ? newHeightData.GetLength(1)-2 : 0+1;
            int y = UnityEngine.Random.Range(0, 2) > 0 ? newHeightData.GetLength(0)-2 : 0+1;

            //Entweder x != 0 bzw. Xmax oder y != 0 bzw. Ymax
            //Damit Fluss immer an einem Rand started
            if (UnityEngine.Random.Range(0,2) > 0)
            {
                x = UnityEngine.Random.Range(0, newHeightData.GetLength(1));
            }
            else
            {
                y = UnityEngine.Random.Range(0, newHeightData.GetLength(0));
            }



            //TODO: debug
            //x = (int)(td.alphamapWidth * DEBUG_x); //152
            //y = (int)(td.alphamapHeight * DEBUG_y); //142

            
            List<int[]> excepDirList = new List<int[]> { new int[] { 0, 0 } };
            //Fluss startet an Rand des Terrain - Verhindern, dass er direkt aus dem Terrain raus fliesst
            if (x == 1)
            {
                excepDirList.Add(new int[] { -1, 1 });
                excepDirList.Add(new int[] { -1, 0 });
                excepDirList.Add(new int[] { -1, -1 });

                excepDirList.Add(new int[] { 0, 1 });
                excepDirList.Add(new int[] { 0, -1 });
            }
            else if(x == newHeightData.GetLength(1) - 2)
            {
                excepDirList.Add(new int[] { 1, 1 });
                excepDirList.Add(new int[] { 1, 0 });
                excepDirList.Add(new int[] { 1, -1 });

                excepDirList.Add(new int[] { 0, 1 });
                excepDirList.Add(new int[] { 0, -1 });
            }else if (y == 1)
            {
                excepDirList.Add(new int[] { 1, -1 });
                excepDirList.Add(new int[] { 0, -1 });
                excepDirList.Add(new int[] { -1, -1 });

                excepDirList.Add(new int[] { 1, 0 });
                excepDirList.Add(new int[] { -1, 0 });
            }
            else
            {
                excepDirList.Add(new int[] { 1, 1 });
                excepDirList.Add(new int[] { 0, 1 });
                excepDirList.Add(new int[] { -1, 1 });

                excepDirList.Add(new int[] { 1, 0 });
                excepDirList.Add(new int[] { -1, 0 });
            }


            int[] directionRes = randomDirection(excepDirList);
            int[] firstDir = new int[] { directionRes[0], directionRes[1]};
            //int[] directionRes = allowedDirList[UnityEngine.Random.Range(0, allowedDirList.Count)];
            //int[] firstDir = new int[] { allowedDirList[1][0], allowedDirList[1][1]};


            float riverHeight = newHeightDataCopy[x, y] - riverStartDepth;
            float riverHeight_old = riverHeight;

            //Debug.Log("directionRes: "+ directionRes[0]+", "+ directionRes[1]);
            //Debug.Log("x: " + x + ", y: " + y);


            while (y > 0 && y < newHeightData.GetLength(0) - 1 && x > 0 && x < newHeightData.GetLength(1) - 1)
            {
                //Debug.Log("in while");

                //Terrain ist tiefer als Fluss
                /*if(newHeightDataCopy[x, y] < riverHeight)
                {
                    //Fluss tiefe lassen wie sie ist
                }else */
                if (newHeightDataCopy[x, y] > riverHeight)
                {
                    //Terrain ist hoeher als Fluss
                    riverHeight = newHeightDataCopy[x, y] - riverStartDepth;
                    //Fluss darf nicht aufwaerts Fliessen
                    if (riverHeight > riverHeight_old)
                    {
                        riverHeight = riverHeight_old;
                    }

                }

                if (riverHeight < 0)
                {
                    riverHeight = 0;
                }


                //Andere Flussstelle gefunden -> Fluss fertig
                if (alreadyVisited(visitedXY, x, y))
                {
                    Debug.Log("alreadyVisited");
                    break;
                }

                createHole(x, y, riverHeight, riverSlopeMin, riverSlopeMax, 0f);
                riverHeight_old = riverHeight;
                //Debug.Log("visitedXY.Add(x + "+x+" + y + "+y+")");
                visitedXY.Add("x" + x + "y" + y, new int[] { x, y });


                bool randomDirectionChange = UnityEngine.Random.Range(0, 100) < riverDirectionChangeChance;
                if (randomDirectionChange)
                {
                    
                    List<int[]> except = new List<int[]> {
                         new int[] { directionRes[0], directionRes[1] }
                    };
                    except.AddRange(excepDirList);

                    List<int[]> possibleDir = new List<int[]>() {
                        new int[] {1, -1},
                        new int[] {1, 0},
                        new int[] {1, 1},

                        new int[] {0, -1},
                        new int[] {0,1},

                        new int[] {-1,-1},
                        new int[] {-1,0},
                        new int[] {-1,1}
                    };

                    /*foreach (int[] dir in possibleDir)
                    {
                        Debug.Log("pre ir x: "+dir[0]+", y: "+dir[1]);
                    }*/
                    removeBackwardsDirection(firstDir, possibleDir);
                    /*foreach (int[] dir in possibleDir)
                    {
                        Debug.Log("post dir x: " + dir[0] + ", y: " + dir[1]);
                    }*/

                    try
                    {
                        /*
                        foreach (int[] dir in except)
                        {
                            Debug.Log("except x: " + dir[0] + ", y: " + dir[1]);
                        }
                        */
                        directionRes = randomDirectionFrom(possibleDir, except);
                    }catch(ArgumentException ae)
                    {
                        Debug.Log(ae.ToString());
                        break;
                    }
                    


                    //directionRes = allowedDirList[UnityEngine.Random.Range(0, allowedDirList.Count)];
                }

                //Debug.Log("1 y: " + y + "\tx: " + x);
                x += directionRes[0];
                y += directionRes[1];
                //Debug.Log("2 y: " + y + "\tx: " + x);

                



#if (RIVERSASCOROUTINE)
                //TODO:debug
                td.SetHeights(0, 0, newHeightData);

                float posX = ((y - directionRes[1]) / (float)td.alphamapWidth) * td.size.x + transform.position.x; // = worldPos.x
                float posZ = ((x - directionRes[0]) / (float)td.alphamapHeight) * td.size.z + transform.position.z; // = worldPos.x
                //Debug.Log("x - directionRes[0]: "+x+ "\ttd.alphamapWidth: "+ td.alphamapWidth+ "\ttd.size.x: "+ td.size.x+ "\ttransform.position.x: "+ transform.position.x);
                
                Vector3 pos = new Vector3(posX, 0, posZ);
                //Debug.Log("posX: "+ posX+ "\tposZ: "+ posZ+ "\tpos: "+ pos);

                debugPosMarker.position = pos;

                yield return new WaitForSeconds(0.0001f);
                //yield return new WaitForSeconds(0.05f);
#endif
            }

        }

        Debug.Log("generateRivers V3 done");
        setNewHeightData();
    }

    private int[] randomDirectionFrom(List<int[]> possibleDir, List<int[]> except)
    {
        List<int[]> possibleDirClone = new List<int[]>(possibleDir);
        int rndPos;

        int[] res = new int[2];

        do
        {
            if(possibleDirClone.Count == 0)
            {
                throw new ArgumentException("No matching direction in List");
            }

            rndPos = UnityEngine.Random.Range(0, possibleDirClone.Count);
            res = possibleDirClone[rndPos];
            possibleDirClone.RemoveAt(rndPos);
        } while (dirIsInExcept(except, res));


        return res;
    }

    private int[] randomDirection(List<int[]> except)
    {
        List<int[]> possibleDir = new List<int[]>() {
            new int[] {1, -1},
            new int[] {1, 0},
            new int[] {1, 1},

            new int[] {0, -1},
            new int[] {0, 0},
            new int[] {0,1},

            new int[] {-1,-1},
            new int[] {-1,0},
            new int[] {-1,1}
        };

        return randomDirectionFrom(possibleDir, except);
    }

    private bool dirIsInExcept(List<int[]> except, int[] res)
    {
        foreach(int[] exc in except)
        {
            if(exc[0] == res[0] && exc[1] == res[1])
                return true;
        }

        return false;
    }

    private void removeBackwardsDirection(int[] firstDir, List<int[]> lowestNeightbours)
    {
        if (firstDir[0] == 0 && firstDir[1] == 0)
            throw new ArgumentException("firstDir is x:0 y:0. This is no direction");


        /*
        Vorzustellen wie:
        x   y   |   x   y   |   x   y
        _____________________________
        1   -1  |   1   0  |   1    1
        ------------------------------
        0   -1  |   0   0  |   0    1
        ------------------------------
        -1  -1  |   -1  0  |   -1   1

        Gegenueberliegende Richtungen werden aus lowestNeightbours entfernt, da der Fluss sonst nicht weiterlaeuft
         */

        List<int[]> toRemove = new List<int[]>();

        if(firstDir[0] == 0)
        {
            toRemove.Add(new int[] { -1, firstDir[1] * -1 });
            toRemove.Add(new int[] { 0, firstDir[1] * -1 });
            toRemove.Add(new int[] { 1, firstDir[1] * -1 });
        }else if (firstDir[1] == 0)
        {
            toRemove.Add(new int[] { firstDir[0] * -1 , - 1});
            toRemove.Add(new int[] { firstDir[0] * -1 , 0});
            toRemove.Add(new int[] { firstDir[0] * -1 , 1});
        }else if (firstDir[0] == 1)
        {
            toRemove.Add(new int[] { -1, 0});
            toRemove.Add(new int[] { -1, firstDir[1] * -1 });
            toRemove.Add(new int[] { 0, firstDir[1] * -1 });
        }
        else if (firstDir[0] == -1)
        {
            toRemove.Add(new int[] { 1, 0 });
            toRemove.Add(new int[] { 1, firstDir[1] * -1 });
            toRemove.Add(new int[] { 0, firstDir[1] * -1 });
        }


        foreach(int[] remove in toRemove)
        {
            int[] e;
            for (int i= lowestNeightbours.Count-1; i>=0; i--)
            {
                e = lowestNeightbours[i];

                if (e[0] == remove[0] && e[1] == remove[1])
                    lowestNeightbours.RemoveAt(i);

            }
        }

    }

    private bool containsDirection(List<int[]> lowestNeightbours, int[] directionRes)
    {
        foreach(int[] e in lowestNeightbours)
        {
            if (e[0] == directionRes[0] && e[1] == directionRes[1])
                return true;
            
        }

        return false;
    }

    private int[] getRandomElementAndRemove(List<int[]> lowestNeightbours)
    {
        int i =UnityEngine.Random.Range(0, lowestNeightbours.Count);
        int[] e = lowestNeightbours[i];
        lowestNeightbours.RemoveAt(i);

        return e;
    }

    private bool alreadyVisited(Hashtable visitedXY, int x, int y)
    {
        return visitedXY.ContainsKey("x" + x + "y" + y);
    }

    /// <summary>
    /// Gibt den niedrigsten Nachbarn zurueck. Wenn mehrere Nachbarn den selben Wert haben, sind diese auch im Array enthalten
    /// </summary>
    /// <param name="x">x Position der Heightmap</param>
    /// <param name="y">y Position der Heightmap</param>
    /// <param name="heightData">Heightmap</param>
    /// <returns></returns>
    private List<int[]> getLowestNeighbours(int x, int y, float[,] heightData)
    {
        List<int[]> res = new List<int[]>();
        float lowestHeight = 1.1f;

        for(int i=-1; i<2; i++)
        {
            for(int j =-1; j<2; j++)
            {
                //Debug.Log("heightData["+x+i+", "+y+j+"]: " + heightData[x + i, y + j] + "\tlowestHeight: " + lowestHeight);
                try { 
                    if (i != 0 || j != 0)
                    {
                        //Debug.Log("SET heightData[" + x + i + ", " + y + j + "]: " + heightData[x + i, y + j] + "\tlowestHeight: " + lowestHeight);
                        if(heightData[x + i, y + j] < lowestHeight)
                        {
                            res.Clear();
                            res.Add(new int[] { i, j });
                            lowestHeight = heightData[x + i, y + j];
                        }
                        else if(heightData[x + i, y + j] == lowestHeight)
                        {
                            res.Add(new int[] { i, j });
                        }

                    }
                }
                catch (IndexOutOfRangeException oor)
                {
                    //Kann passieren, wenn x,y bereits der Rand ist
                    Debug.Log("getLowestNeighbours IndexOutOfRangeException - x + i: "+ (x + i)+ "\ty + j: "+(y + j));
                }

            }

        }

        //Debug.Log("lowestHeight: " + lowestHeight + "res[0]: " + res[0] + "\tres[1]: " + res[1]);
        return res;
    }

    /// <summary>
    /// Gibt alle Nachbarn der Hoehe nach sortiert zurueck. Niedrigste zuerst
    /// </summary>
    /// <param name="x">x Position der Heightmap</param>
    /// <param name="y">y Position der Heightmap</param>
    /// <param name="heightData">Heightmap</param>
    /// <returns></returns>
    private List<int[]> getNeighboursSorted(int x, int y, float[,] heightData)
    {
        List<float[]> tmp = new List<float[]>();
        
        for (int i = -1; i < 2; i++)
        {
            for (int j = -1; j < 2; j++)
            {
                //Debug.Log("heightData["+x+i+", "+y+j+"]: " + heightData[x + i, y + j] + "\tlowestHeight: " + lowestHeight);
                try
                {
                    if (i != 0 || j != 0)
                    {
                        tmp.Add(new float[] { i, j, heightData[x + i, y + j] });
                    }
                }
                catch (IndexOutOfRangeException oor)
                {
                    //Kann passieren, wenn x,y bereits der Rand ist
                    Debug.Log("getLowestNeighbours IndexOutOfRangeException - x + i: " + (x + i) + "\ty + j: " + (y + j));
                }

            }

        }

        tmp.Sort((a, b) => a[2].CompareTo(b[2]));

        //Debug.Log("lowestHeight: " + lowestHeight + "res[0]: " + res[0] + "\tres[1]: " + res[1]);



        //Debug.Log("getNeighboursSorted");
        List<int[]> res = new List<int[]>();
        foreach (float[] t in tmp)
        {
            //Debug.Log("res new neighbour - x: "+(int)t[0] +", y: "+(int)t[1]+" height: "+t[2]);
            res.Add(new int[] { (int)t[0], (int)t[1] });
        }


        //throw new System.Exception("debug exception");
        
        return res;
    }




    //------------------------------------/ Rivers-------------------------------------

    //------------------------------------Smooth Height-------------------------------------
    public void roughUpHeight()
    {
        initHeight();

        float amount;
        bool add;
        for(int i = 0; i < newHeightData.GetLength(0); i++)
        {
            for(int j=0; j < newHeightData.GetLength(1); j++)
            {
                add = UnityEngine.Random.Range(0, 2) > 0;
                amount = UnityEngine.Random.Range(roughMin, roughMax);

                if (add)
                    newHeightData[i, j] += amount;
                else
                    newHeightData[i, j] -= amount;

            }
        }

        //td.SetHeights(0, 0, newHeightData);
        setNewHeightData();
    }

    //------------------------------------/Smooth Height-------------------------------------

    //------------------------------------Smooth Height-------------------------------------
    public void smoothHeight()
    {
        initHeight();


        for (int ite = 0; ite < smoothIterations; ite++) { 
            float smoothedHeight;
            // = 1; < height/width -> Raender auslassen, weil eine Reihe/Zeile fehlt
            for (int i = 1; i < newHeightData.GetLength(0)-1; i++)
            {
                for (int j = 1; j < newHeightData.GetLength(1) - 1; j++)
                {
                    smoothedHeight = (newHeightData[i+1,j-1]+
                        newHeightData[i+1, j] +
                        newHeightData[i+1, j+1] +
                        newHeightData[i, j-1] +
                        newHeightData[i, j] +
                        newHeightData[i, j+1] +
                        newHeightData[i-1, j-1] +
                        newHeightData[i-1, j] +
                        newHeightData[i-1, j+1]);
                    smoothedHeight /= 9f;

                    newHeightData[i, j] = smoothedHeight;
                }
            }
        }



        //td.SetHeights(0, 0, newHeightData);
        setNewHeightData();
    }

    //------------------------------------/Smooth Height-------------------------------------

    //------------------------------------Change Height-------------------------------------

    public void changeHeight()
    {
        initHeight();

        // = 1; < height/width -> Raender auslassen, weil eine Reihe/Zeile fehlt
        for (int i = 0; i < newHeightData.GetLength(0); i++)
        {
            for (int j = 0; j < newHeightData.GetLength(1); j++)
            {

                newHeightData[i, j] += changeHeightBy;
            }
        }

        setNewHeightData();
    }

    //------------------------------------/ Change Height-------------------------------------

    //------------------------------------Textures-------------------------------------

    [System.Serializable]
    public class SplatHeightTextures
    {
        public int textureIndex;
        public int startHeight;
    }


    public void sortHeightTextures()
    {
        //SplatHeightTextures[] shtCopy = new SplatHeightTextures[heightTextures.Count];
        //heightTextures.CopyTo(shtCopy);
        //heightTextures.Clear();

        List<SplatHeightTextures> shtCopy = new List<SplatHeightTextures>();
        shtCopy.AddRange(heightTextures);
        heightTextures.Clear();

        SplatHeightTextures sht;
        while (shtCopy.Count > 0)
        {
            sht = getLowestTextureMinVal(shtCopy, 0);
            heightTextures.Add(sht);
            shtCopy.Remove(sht);
        }

    }

    private SplatHeightTextures getLowestTextureMinVal(List<SplatHeightTextures> shtList, float minVal)
    {
        //Debug.Log("------------------------getLowestTexture");
        SplatHeightTextures lowestSht = shtList[shtList.Count-1];
        foreach (SplatHeightTextures sht in shtList)
        {
            //Debug.Log("sht.startHeight: " + sht.startHeight + "\tlowestSht.startHeight: " + lowestSht.startHeight + "\tminVal: " + minVal);
            if (sht.startHeight < lowestSht.startHeight && sht.startHeight >= minVal)
            {
                //Debug.Log("tlowestSht set");
                lowestSht = sht;
            }
        }

        return lowestSht;
    }
    private SplatHeightTextures getTextureByHeight(List<SplatHeightTextures> shtList, float height)
    {
        //Debug.Log("------------------------getTextureByHeight");
        SplatHeightTextures resSht = shtList[0];
        foreach (SplatHeightTextures sht in shtList)
        {
            //Debug.Log("sht.startHeight: " + sht.startHeight + "\tresSht.startHeight: " + resSht.startHeight + "\theight: " + height);
            if (sht.startHeight <= height && sht.startHeight > resSht.startHeight)
            {
                //Debug.Log("tresSht set");
                resSht = sht;
            }
        }

        return resSht;
    }



    public void generateTextures()
    {
        init();

        /*
        Je nachdem wie steil der Abhang ist bzw wie hoch die Stelle ist, eine passende Textur setzen
        
        Setzen der Textur basierend auf Hoehe
        https://www.youtube.com/watch?v=aUcWm1k0xDc
        https://www.youtube.com/watch?v=yv23i2PERR8
        */

        
        float[,,] smd = new float[td.alphamapWidth, td.alphamapHeight, td.alphamapLayers];

        for(int i = 0; i < td.alphamapWidth; i++)
        //for (int i = 0; i < (td.alphamapWidth * 0.025f); i++)
        {
            for(int j = 0; j < td.alphamapHeight; j++)
            //for (int j = 0; j < (td.alphamapHeight*0.025f); j++)
            {


                
                SplatHeightTextures sht = getSplatHeightTexture(i,j);
                //Debug.Log("x: " + i + "\ty: " + j+"\tres: "+sht.textureIndex);

                float[] splat = new float[td.terrainLayers.Length];

                //Debug.Log("----generateTextures - "+ splat.Length + "\ttex: "+ sht.textureIndex);
                splat[sht.textureIndex] = 1;
                //splat[3] = 1;


                for (int k=0; k < splat.Length; k++)
                {
                    smd[i, j, k] = splat[k];
                }


            }


        }

        td.SetAlphamaps(0, 0, smd);

    }

    /*
    private float getHeightAtAlphamapPosition(int x, int y)
    {
        return td.GetHeight((int)((y * td.heightmapHeight) / (float)td.alphamapHeight), (int)((x * td.heightmapWidth) / (float)td.alphamapWidth)); ;
    }
    */

    private int[] getAlphaMapToHeightMapCoordinates(int x, int y)
    {
        return new int[] {
            (int)((y * td.heightmapHeight) / (float)td.alphamapHeight),
            (int)((x * td.heightmapWidth) / (float)td.alphamapWidth) };
    }

    private int[] getHeightMapToAlphaMapCoordinates(int x, int y)
    {
        return new int[] {
            (int)((y * td.alphamapHeight) / (float)td.heightmapHeight),
            (int)((x * td.alphamapWidth) / (float)td.heightmapWidth) };
    }
    

    private SplatHeightTextures getSplatHeightTexture(int x, int y)
    {
        //Debug.Log("getSplatHeightTexture");

        SplatHeightTextures res = heightTextures[0];
        //float height = td.GetHeight(x, y);
        //float height = td.GetHeight(y, x);
        int[] heightMapCoor = getAlphaMapToHeightMapCoordinates(x,y);
        float height = td.GetHeight(heightMapCoor[0], heightMapCoor[1]);
        //Debug.Log("x: "+x+" - y: "+y+"\theight: " + height);
        if (isCliff(heightMapCoor[0], heightMapCoor[1], countAsCliffHeightThreshold, cliffHeightRandomness))
            return textureCliff;

        if (heightTextures.Count == 0)
            throw new IndexOutOfRangeException("No textures set: heightTextures.Count == 0");


        res = getTextureByHeight(heightTextures, height);
        //if(res.textureIndex == 4)
            //Debug.Log("getSplatHeightTexture x"+x+"\ty: "+y+"\theight: "+height+"\tres: "+res.textureIndex);




        return res;
    }

    private bool isCliff(int x, int y, float countAsCliffHeightThreshold)
    {
        return isCliff(x, y, countAsCliffHeightThreshold, 0f);
    }
    //x,y sind heightMap coordinates
    private bool isCliff(int x, int y, float countAsCliffHeightThreshold, float cliffHeightRandomness)
    {
        //float height = td.GetHeight(y, x);
        float height = td.GetHeight(x, y);


        for (int i = -1; i < 2; i++)
        {
            for (int j = -1; j < 2; j++)
            {
                try
                {
                    //Wenn irgendein Nachbar der jetzigen Position um den countAsCliffHeightThreshold-Wert hoeher/tiefer liegt,
                    //dann ist die jetzige Position eine Klippe
                    float rnd = UnityEngine.Random.Range(0,cliffHeightRandomness);
                    rnd = UnityEngine.Random.Range(0, 2) > 0 ? rnd : rnd*-1;

                    //if ((i != 0 || j != 0) && (Mathf.Abs(height - td.GetHeight(y + j, x + i)) > (countAsCliffHeightThreshold + rnd)))
                    if ((i != 0 || j != 0) && (Mathf.Abs(height - td.GetHeight(x + i, y + j)) > (countAsCliffHeightThreshold + rnd)))
                    {
                        //Debug.Log("countAsCliffHeightThreshold check - height: "+ height+ "\ttd.GetHeight(j, i): "+ td.GetHeight(y + j, x + i) +"\t-abs: "+ Mathf.Abs(height - td.GetHeight(j, i)));
                        return true;
                    }
                }
                catch (IndexOutOfRangeException oor)
                {
                    //Kann passieren, wenn x,y bereits der Rand ist
                }

            }

        }

        return false;
    }

    //------------------------------------/Textures-------------------------------------

    //------------------------------------Generate vegetation-------------------------------------

    [System.Serializable]
    public class SpawnableObject
    {
        public List<GameObject> prefabs;
        public int amount;
        [Range(0f, 100f)]
        public float cliffThreshold = 0.1f;
        //auf welchen Texturen darf das Objekt platziert werden?
        public int[] legalTextureLayerIndexes;
        public bool rotateX = false;
        public bool rotateY = false;
        public bool rotateZ = false;

    }

    public class TextureLayerStrength
    {
        public int layerIndex;
        public float layerStrength;

        public TextureLayerStrength(int index, float strength)
        {
            this.layerIndex = index;
            this.layerStrength = strength;
        }
    }


    //OnCollisionExit triggert nicht immer,
    //dadurch koennen Objekte, bis maxVegetationCollisionCheckIterations erreicht ist,
    //weiter versucht werden zu positionieren
    public void generateVegetation()
    {
        init();
        initHeight();

        Debug.Log("generateVegetation start");
        startedStartCoroutine = 0;
        spawnObjectCalled = 0;

        Vector3 start = transform.position;
        Vector3 end = start + td.size;

        Transform vegetationParent = transform.Find("Vegetation");
        if (vegetationParent == null) {
            vegetationParent = new GameObject().transform;
            vegetationParent.name = "Vegetation";
            vegetationParent.parent = transform;
        }

        //TODO: vll. koennte man start/end in Segmente unterteilen und damit eine gleichmaessigere Verteilung erreichen (wenn man das will)

        foreach (SpawnableObject so in vegetationObjects)
        {
            startedStartCoroutine++;
            StartCoroutine(spawnObjects(start, end, so, vegetationParent));
        }

        
    }

    private IEnumerator spawnObjects(Vector3 areaStart, Vector3 areaEnd, SpawnableObject so, Transform parent)
    {
        //Debug.Log("spawnObjects");

        for (int tc = 0; tc < so.amount; tc++)
        {
            startedStartCoroutine++;
            StartCoroutine(spawnObject(areaStart, areaEnd, so, parent));
        }

        startedStartCoroutine--;
        if (startedStartCoroutine == 0) {
            Debug.Log("generateVegetation done spawnObjectCalled: "+ spawnObjectCalled);
        }

        yield return new WaitForEndOfFrame();
    }

    int spawnObjectCalled;
    private IEnumerator spawnObject(Vector3 areaStart, Vector3 areaEnd, SpawnableObject so, Transform parent)
    {
#if UNITY_EDITOR
        spawnObjectCalled++;

        int collisionCheckIterations = 0;

        GameObject spawned = PrefabUtility.InstantiatePrefab(so.prefabs[UnityEngine.Random.Range(0, so.prefabs.Count)]) as GameObject;


        Vector3? spawnPos = null;
        Quaternion rotation = spawned.transform.rotation;
        rotation.eulerAngles = rotateObjectRandom(so, spawned);
        //Debug.Log("rotation.x: " + rotation.x + "\trotation.y: " + rotation.y + "\trotation.z: " + rotation.z);

        spawned.transform.parent = parent;
        spawned.transform.rotation = rotation;

        List<Collider> goColls = getColliderFromGameObject(spawned);
        List<TmpCollisionDetection> tmoColDet = addTmpCollisionDetection(spawned, goColls);

        bool keepChecking = true;
        do
        {
            spawnPos = getLegalVegetationPosition(so, areaStart, areaEnd);
            if (spawnPos == null)
            {
                Debug.LogWarning("No legal spawn position found for object to spawn");
                break;
            }


            spawned.transform.position = spawnPos ?? new Vector3();

            yield return new WaitForEndOfFrame();
            //Debug.Log("post WaitForEndOfFrame");

            bool hasCol = hasCollision(tmoColDet, goColls);
            //Debug.Log("spawned.name: "+ spawned.name + "\thasCol: " + hasCol);
            keepChecking = hasCol;

            

            //damit keine endlos Schleife entstehen kann
            collisionCheckIterations++;
            // /
        } while (keepChecking && collisionCheckIterations < maxVegetationCollisionCheckIterations);
        if (keepChecking)
        {
            Debug.LogWarning("No legal and non colliding spawn position found for object to spawn: " + spawned.name);
            Destroy(spawned);
        }
        else
        {
            removeTmpAddedComponents(tmoColDet, spawned);
        }

        startedStartCoroutine--;
        if (startedStartCoroutine == 0)
        {
            Debug.Log("generateVegetation done spawnObjectCalled: " + spawnObjectCalled);
        }
#else
        yield return new WaitForEndOfFrame();
#endif
    }

    private Vector3 rotateObjectRandom(SpawnableObject so, GameObject spawned)
    {
        float x = 0;
        float y = 0;
        float z = 0;

        if (so.rotateX)
        {
            x = UnityEngine.Random.Range(0f,360f);
        }
        if (so.rotateY)
        {
            y = UnityEngine.Random.Range(0f, 360f);
        }
        if (so.rotateZ)
        {
            z = UnityEngine.Random.Range(0f, 360f);
        }

        //Debug.Log("x: "+x+ "\ty: " + y + "\tz: " + z);

        return new Vector3(x,y,z);
    }

    private void removeTmpAddedComponents(List<TmpCollisionDetection> tmpColDet, GameObject spawned)
    {
        foreach(TmpCollisionDetection tcd in tmpColDet)
        {
            Destroy(tcd.transform.GetComponent<Rigidbody>());
            Destroy(tcd);
        }

    }

    //Prefab darf keinen Rigidbody haben! Der wird im spaeteren Verlauf immer entfernt
    private List<TmpCollisionDetection> addTmpCollisionDetection(GameObject spawned, List<Collider> goColls)
    {
        List<TmpCollisionDetection> tmpColDet = new List<TmpCollisionDetection>();

        foreach (Collider col in goColls)
        {
            Rigidbody rb = col.gameObject.AddComponent<Rigidbody>();
            rb.constraints = RigidbodyConstraints.FreezeRotationX |
                RigidbodyConstraints.FreezeRotationY |
                RigidbodyConstraints.FreezeRotationZ |
                RigidbodyConstraints.FreezePositionX |
                RigidbodyConstraints.FreezePositionY | 
                RigidbodyConstraints.FreezePositionZ;

            tmpColDet.Add(col.gameObject.AddComponent<TmpCollisionDetection>());

        }

        return tmpColDet;
    }

    private List<Collider> getColliderFromGameObject(GameObject spawned)
    {
        List<Collider> colls;
        Collider[] collsArr = spawned.transform.GetComponentsInChildren<Collider>(false);
        colls = new List<Collider>(collsArr);
        Collider GOCol = spawned.GetComponent<Collider>();
        if(GOCol != null)
            colls.Add(GOCol);

        return colls;
    }

    private bool hasCollision(List<TmpCollisionDetection> tmpDolDets, List<Collider> colls)
    {
        //Jeden Collider des Objektes durchgehen
        foreach (TmpCollisionDetection tdd in tmpDolDets)
        {
            //Jeden Collider, der mit irgendeinem Collider des Objectes kollidiert durchgehen
            foreach(Collider c in tdd.getInsideColls())
            {
                //Wenn irgendein kollidierender Collider kein eigender Collider && nicht das Terrain ist
                //-> Object kollidiert mit irgendetwas
                if (!colls.Contains(c) && !"Terrain".Equals(c.transform.tag))
                {
                    //Debug.Log("collision with: "+c.name);
                    return true;
                }
            }
        }

        return false;
    }

    private Vector3? getLegalVegetationPosition(SpawnableObject so, Vector3 start, Vector3 end)
    {
        float legalThreshold = 0.75f;

        float rndX;
        float rndZ;

        
        int iterations = 0;

        int[] heightMapCoordinates;
        int[] alphaMapCoordinates;
        bool keepSearching = true;
        do
        {
            rndX = UnityEngine.Random.Range(start.x, end.x);
            rndZ = UnityEngine.Random.Range(start.z, end.z);

            heightMapCoordinates = getWorldToHeightMapCoordinates(rndX, rndZ);
            alphaMapCoordinates = getHeightMapToAlphaMapCoordinates(heightMapCoordinates[0], heightMapCoordinates[1]);

            //debugPosMarker.transform.position = new Vector3(rndX, 0, rndZ);

            if (!isCliff(heightMapCoordinates[0], heightMapCoordinates[1], so.cliffThreshold) && isOnLegalTextureLayer(alphaMapCoordinates[1], alphaMapCoordinates[0], so.legalTextureLayerIndexes, legalThreshold))
            {
                return (new Vector3(rndX, td.GetHeight(heightMapCoordinates[0], heightMapCoordinates[1]), rndZ));
            }

            //damit keine endlos Schleife entstehen kann
            iterations++;
            if (iterations >= maxLegalVegetationPositionIterations)
                keepSearching = false;
            // /

        } while (keepSearching);

        return null;
    }

    

    private int[] getWorldToHeightMapCoordinates(float x, float z)
    {
        if (newHeightData == null)
            Debug.LogError("getWorldToHeightMapCoordinates newHeightData need to be set");

        int mapX = (int)(((x - transform.position.x) / td.size.x) * newHeightData.GetLength(1));
        int mapZ = (int)(((z - transform.position.z) / td.size.z) * newHeightData.GetLength(0));
        
        return new int[] { mapX , mapZ };
    }

    //x,y sind Alphamap coordinates
    private bool isOnLegalTextureLayer(int x, int z, int[] legalTextureLayers, float legalThreshold)
    {
        TextureLayerStrength tls;
        foreach(int i in legalTextureLayers) {
            tls = getMainTextureLayerIndex(x, z);
            if (i == tls.layerIndex && tls.layerStrength >= legalThreshold)
                return true;
        }

        return false;
    }

    private TextureLayerStrength getMainTextureLayerIndex(int x, int z)
    {
        float[] mix = getTextureLayerMix(x,z);

        float mainStrength = 0;
        int mainIndex = 0;

        // loop through each mix value and find the maximum
        for (int n = 0; n < mix.Length; n++ )
     {
            if (mix[n] > mainStrength)
            {
                mainIndex = n;
                mainStrength = mix[n];
            }
        }

        return new TextureLayerStrength(mainIndex, mainStrength);

    }
    private float[] getTextureLayerMix(int x, int z) { 
        // get the splat data for this cell as a 1x1xN 3d array (where N = number of textures)
        float[,,] splatmapData = td.GetAlphamaps(x, z, 1, 1);

        // extract the 3D array data to a 1D array:
        float[] cellMix = new float[splatmapData.GetUpperBound(2) + 1];

        for (int n = 0; n<cellMix.Length; n++ )
        {
            cellMix[n] = splatmapData[0, 0, n];
        }

        return cellMix;
    }
    //------------------------------------/Generate vegetation-------------------------------------



}

