﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NWH.VehiclePhysics;
using System;
using MotionSystems;

//TODO: wenn pausiert forceseatmi state aendern
public class MotionPlattformManager : MonoBehaviour
{
    public bool movePlattform = true;

    protected bool isPaused=false;

    public VehicleChanger vehicleChanger;
    public VehicleController vehicleController;

    private ForceSeatMI_UnityVehicle m_Api;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("m_Api.Begin();");
        m_Api = new ForceSeatMI_UnityVehicle();
        //ForceSeatMI_BeginMotionControl
        m_Api.Begin();
    }

    void OnDestroy()
    {
        Debug.Log("m_Api.End();");
        //ForceSeatMI_EndMotionControl
        m_Api.End();
    }

    // Update is called once per frame
    void Update()
    {
        if (vehicleChanger != null)
        {
            vehicleController = vehicleChanger.ActiveVehicleController;

        }

        if (vehicleController != null)
        {
            doMotion();
        }
    }

    private void doMotion()
    {
        if (!movePlattform)
            return;

        //motionplattform bewegen
        m_Api.Tick(vehicleController.vehicleRigidbody, Time.deltaTime, isPaused, vehicleController.engine.RPM, vehicleController.engine.maxRPM, vehicleController.transmission.Gear);
    }
}
